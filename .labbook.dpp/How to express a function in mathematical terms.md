

Let $\vec x = f(a, b, c)$

where $f(a, b, c) == $

$\vec x = \begin{bmatrix} a \\ b \\ c \end{bmatrix}, a \lt b\lt c$



How to express a programming function in mathematical notation.