$computational\ method := \{Q, I, \Omega, f\}$

$Q := \{I, \Omega, ?\}$

$f : Q \to Q$

$f(q) = q, \forall q \in \Omega$



$Q :=$ states of the computation

$I :=$ the input

$\Omega :=$ the output

$f := $ the computational rule.



$x \in I$

$x :=$? 



$computational\ sequence := (x_0, x_1, x_2, ...)$ where $x_0 = x$ and  $x_{k+1} = f(x_k)$ where $k \ge 0$ 

i.e.

$computational\ sequence := (x, x_1 = f(x) , x_2 = f(x_1), x_3 = f(x_2), ... , x_{k+1} = f(x_k))$

i.e.

$computational\ sequence := (x, f(x) , f(x_1), f(x_2), ... , f(x_k))$

if $k$ is the smallest integer integer for which $x_k \in \Omega$ and therefore $f(x_{k-1}) \in \Omega$

$computational\ sequence := (x, f(x) , f(x_1), f(x_2), ... , f(x_{k-1}))$

So $f(x_{k-1})$ is an [the first] answer.









