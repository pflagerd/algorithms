package net.pflager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Random;

import org.junit.jupiter.api.Test;

public class TestBinarySearch extends BinarySearch {
    @Test
    public void testSomeRandomArrayOfInt() {
        Random random = new Random();

        int[] array = new int[random.nextInt(10)];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt();
        }
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

        int correctIndex = random.nextInt(array.length);
        int value = array[correctIndex];
        assertEquals(correctIndex, search1(array, value));
    }

    @Test
    public void testSomeSpecificArrayOfInt() {
        String s = "-1731689481 -1325619091 -1186790236 -937302375 -610273715 69506695 1134126390 1321197116 1493694519";
        String[] sa = s.split("\\s+");
        int[] array = new int[sa.length];
        for (int i = 0; i < sa.length; i++) {
            array[i] = Integer.parseInt(sa[i]);
        }
        Arrays.sort(array);

        int correctIndex = 8;
        int value = array[correctIndex];
        assertEquals(correctIndex, search1(array, value));

    }
}
