package net.pflager;

import org.junit.jupiter.api.Test;

public class TestFibonacci extends Fibonacci {
	@Test
	void test01_to_9() {
		for (int i = 0; i < 10; i++) {
			System.out.println(fibonacci(i));
		}
	}
	
	private static String spaces(int n) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < n; i++) {
			stringBuffer.append(' ');
		}
		return stringBuffer.toString();
	}
	
	@Test
	void test01_to_49() {
		for (int i = 0; i < 94; i++) {
			System.out.println(i + spaces(3 - String.valueOf(i).length()) + Long.toUnsignedString(fibonacci(i)));
		}
		System.out.println('a' + 'b');
	}

}
