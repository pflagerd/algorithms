package net.pflager;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/*
 *                     10                       
 *             8                12               
 *        6         9      11        14        
 * 
 */
class Test_isBST extends IsBST {
	
	@BeforeEach
	public void before() {
		lastValue = Integer.MIN_VALUE;
	}


	@Test
	void testIsNull() {
		assertEquals(true, isBST(null));
	}

	/*
	 *                     10                       
	 */
	@Test
	void testIs10NoChildren() {
		Node node = new Node();
		node.value = 10;
		assertEquals(true, isBST(node));
	}

	/*
	 *                     10                       
	 *              8
	 * 
	 */
	@Test
	void testIs10LeftChild8NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node8 = new Node();
		node8.value = 8;
		node.left = node8;
		assertEquals(true, isBST(node));
	}

	/*
	 *                     10                       
	 *              11
	 * 
	 */
	@Test
	void testIs10LeftChild11NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node11 = new Node();
		node11.value = 11;
		node.left = node11;
		assertEquals(false, isBST(node));
	}

	/*
	 *                     10                       
	 *              10
	 */
	@Test
	void testIs10LeftChild10NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node10 = new Node();
		node10.value = 10;
		node.left = node10;
		assertEquals(false, isBST(node));
	}

	/*
	 *                     10                       
	 *                            12               
	 */
	@Test
	void testIsRightChild12NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node12 = new Node();
		node12.value = 12;
		node.right = node12;
		assertEquals(true, isBST(node));
	}

	/*
	 *                     10                       
	 *                            9               
	 */
	@Test
	void testIsRightChild9NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node9 = new Node();
		node9.value = 9;
		node.right = node9;
		assertEquals(false, isBST(node));
	}

	/*
	 *                     10                       
	 *                            10
	 */
	@Test
	void testIsRightChild10NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node10 = new Node();
		node10.value = 10;
		node.right = node10;
		assertEquals(false, isBST(node));
	}

	/*
	 *                      10                       
	 *              8				12               
	 * 
	 */
	@Test
	void testIsLeftChild8NoChildrenRightChild10NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node8 = new Node();
		node8.value = 8;
		node.left = node8;
		Node node12 = new Node();
		node12.value = 12;
		node.right = node12;
		assertEquals(true, isBST(node));
	}

	/*
	 *                      10                       
	 *              11				12               
	 * 
	 */
	 /*
	@Test
	void testIsLeftChild8NoChildrenRightChild10NoChildren() {
		Node node = new Node();
		node.value = 10;
		Node node8 = new Node();
		node8.value = 8;
		node.left = node8;
		Node node12 = new Node();
		node12.value = 12;
		node.right = node12;
		assertEquals(true, isBST(node));
	}
	*/
	/*
	 *                      10
	 *              8				9               
	 * 
	 */

	/*
	 *                      10
	 *              9				8               
	 * 
	 */

	/*
	 *                      10
	 *              11				9               
	 * 
	 */

	/*
	 *                      10
	 *              11				13               
	 * 
	 */

	/*
	 *                      10
	 *              13				11               
	 * 
	 */
	
	/*
	 *                     10                       
	 *              8				12               
     *        6         9       11        14        
	 * 
	 */
	@Test
	void testIsLeftChild8TwoChildrenLeftChild8RightChild9RightChild12TwoChildrenLeftChild11RightChild14() {
		Node node = new Node();
		node.value = 10;
		Node node8 = new Node();
		node8.value = 8;
		node.left = node8;
		Node node12 = new Node();
		node12.value = 12;
		node.right = node12;
		Node node6 = new Node();
		node6.value = 6;
		node8.left = node6;
		Node node9 = new Node();
		node9.value = 9;
		node8.right = node9;
		Node node11 = new Node();
		node11.value = 11;
		node12.left = node11;
		Node node14 = new Node();
		node14.value = 14;
		node12.right = node14;
		assertEquals(true, isBST(node));
	}

	/*
	 *                     10                       
	 *              8				12               
     *        6         9       13        14        
	 * 
	 */
	@Test
	void testIsLeftChild8TwoChildrenLeftChild8RightChild9RightChild12TwoChildrenLeftChild13RightChild14() {
		Node node = new Node();
		node.value = 10;
		Node node8 = new Node();
		node8.value = 8;
		node.left = node8;
		Node node12 = new Node();
		node12.value = 12;
		node.right = node12;
		Node node6 = new Node();
		node6.value = 6;
		node8.left = node6;
		Node node9 = new Node();
		node9.value = 9;
		node8.right = node9;
		Node node13 = new Node();
		node13.value = 13;
		node12.left = node13;
		Node node14 = new Node();
		node14.value = 14;
		node12.right = node14;
		assertEquals(false, isBST(node));
	}

	/*
	 *                      11                       
	 *              8				12               
     *        6         9       10       14        
	 * 
	 */
	@Test
	void testIsLeftChild8TwoChildrenLeftChild11RightChild9RightChild12TwoChildrenLeftChild13RightChild14() {
		Node node11 = new Node();
		node11.value = 11;
		Node node8 = new Node();
		node8.value = 8;
		node11.left = node8;
		Node node12 = new Node();
		node12.value = 12;
		node11.right = node12;
		Node node6 = new Node();
		node6.value = 6;
		node8.left = node6;
		Node node9 = new Node();
		node9.value = 9;
		node8.right = node9;
		Node node10 = new Node();
		node10.value = 10;
		node12.left = node10;
		Node node14 = new Node();
		node14.value = 14;
		node12.right = node14;
		assertEquals(false, isBST(node11));
	}
}
