package net.pflager;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class TestBinarySearchTree extends BinarySearchTree<Integer> {
	TestBinarySearchTree() {
		super(0);
	}
	
	Random random = new Random();
	
	final static int upperBound = 100;
	
	int[] testData = {6, 29, 87, 91, 29, 44, 80, 25, 25, 46, 80, 8, 98, 69, 38, 74, 27, 4, 63, 60, 11, 86, 49, 48, 72, 32, 29, 99, 92, 65, 57, 5};
	
	// @Test
	void generateTestData() {
		StringBuilder stringBuilder = new StringBuilder("int[] data = {");
		for (int i = 0; i < 32; i++) {
			if (i != 0) {
				stringBuilder.append(", ");				
			}
			stringBuilder.append(random.nextInt(upperBound));
		}
		stringBuilder.append("};");
		System.out.println(stringBuilder.toString());
	}
	
	@Test
	void add() {
		BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<Integer>(0);
		
		for (int i : testData) {
			binarySearchTree.add(i);
		}
	}

}
