package net.pflager;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TestPrime extends Prime {

	@Test
	void test_1() {
		assertEquals(false, isPrime(1));
	}

	@Test
	void test_2() {
		assertEquals(true, isPrime(2));
	}

	@Test
	void test_3() {
		assertEquals(true, isPrime(3));
	}

	@Test
	void test_4() {
		assertEquals(false, isPrime(4));
	}

	@Test
	void test_5() {
		assertEquals(true, isPrime(5));
	}

}
