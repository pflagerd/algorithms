package net.pflager;

public class Sets {

	public static int arrangements(int n, int k) {
		int arrangements = k;
		for (int i = k + 1; i <= n; i++) {
			arrangements *= i;
		}
		return arrangements;
	}
	
	public static int combinations(int n, int k) {
		int combinations = arrangements(n, k);
		
		return combinations / factorial(k);
	}
	
	public static int factorial(int n) {
		int factorial = 1;
		for (int i = 2; i <= n; i++) {
			factorial *= i;
		}
		return factorial;
	}
	
	public static void main(String[] args) {
		// System.out.println(subsets(new String[] {"1", "2", "3"}));
		for (int i = 0; i < 13; i++) {
			System.out.print(factorial(i) + " ");
		}
	}

}
