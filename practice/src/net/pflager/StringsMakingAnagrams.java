package net.pflager;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class StringsMakingAnagrams {

    static String toString(char[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            stringBuilder.append(array[i]);
        }
        return stringBuilder.toString();
    }

    static int minimumDeletionCountToMakeAnagramsOfEachOther(String a, String b) {
        char[] aCharArray = a.toCharArray();
        Arrays.sort(aCharArray);
        System.out.println(toString(aCharArray));

        char[] bCharArray = b.toCharArray();
        Arrays.sort(bCharArray);
        System.out.println(toString(bCharArray));

        int aIndex = 0; //
        int bIndex = 0; //

        int deletionCount = 0; //  
        while (true) {
            System.out.println("34: aIndex == " + aIndex + ", bIndex = " + bIndex + ", deletionCount == " + deletionCount);
            if (aCharArray[aIndex] < bCharArray[bIndex]) {
                deletionCount++;
                aIndex++;
            } else if (aCharArray[aIndex] == bCharArray[bIndex]) {
                aIndex++;
                bIndex++;
            } else { // aCharArray[aIndex] > bCharArray[bIndex]
                deletionCount++;
                bIndex++;
            }
            System.out.println("45: aIndex == " + aIndex + ", bIndex = " + bIndex + ", deletionCount == " + deletionCount);

            if (aIndex >= aCharArray.length) {
            	deletionCount += bCharArray.length - bIndex;
            	break;
            }

            if (bIndex >= bCharArray.length) {
            	deletionCount += aCharArray.length - aIndex;
            	break;
            }
        }

        return deletionCount;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String a = scanner.nextLine();

        String b = scanner.nextLine();

        int res = minimumDeletionCountToMakeAnagramsOfEachOther(a, b);

        bufferedWriter.write(String.valueOf(res));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
