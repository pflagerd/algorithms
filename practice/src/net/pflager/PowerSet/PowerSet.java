package net.pflager.PowerSet;

public class PowerSet {
	public static void main(String[] args) {
		naive2();
	}

	private static void naive1() {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < 2; k++) {
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append("{ ");
					if (i == 1) {
						stringBuilder.append('c');
					}
					if (j == 1) {
						if (stringBuilder.length() != 2) {
							stringBuilder.append(", ");
						}
						stringBuilder.append('b');
					}
					if (k == 1) {
						if (stringBuilder.length() != 2) {
							stringBuilder.append(", ");
						}
						stringBuilder.append('a');
					}
					stringBuilder.append(" }");
					System.err.println(stringBuilder);
				}
			}
		}
	}
	
	private static void naive2() {
		char[] thing = {'a', 'b', 'c'};
		for (int length = 1; length <= 3; length++) {
			StringBuilder stringBuilder = new StringBuilder("{ ");
			for (int i = 0; i < thing.length - length + 1; i++) {
				for (int j = 0; j < length; j++) {
					if (stringBuilder.length() > 2)
						stringBuilder.append(", ");
					stringBuilder.append(thing[i + j]);
				}
			}
			stringBuilder.append(" }");
			System.err.println(stringBuilder);
		}
	}
}
