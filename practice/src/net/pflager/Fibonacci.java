package net.pflager;

public class Fibonacci {
	static long fibonacci(int n) {
		long f = 0;
		long g = 1;
		for (int i = 0; i < n; i++) {
			f = f + g;
			g = f - g;
		}
		return f;
	}
}
