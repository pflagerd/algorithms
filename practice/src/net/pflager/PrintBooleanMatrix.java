package net.pflager;

import java.util.Random;

public class PrintBooleanMatrix {
	
	private String padWithSpaces(String in, int fieldWidth) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = in.length(); i < fieldWidth; i++) {
			stringBuilder.append(' ');			
		}
		stringBuilder.append(in);
		return stringBuilder.toString();
	}

	public void thing() {
		boolean[][] array = new boolean[4][6];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = new Random().nextInt(2) == 0;  
			}
		}
		
		String s = "" + (array.length - 1);
		int rowlabelminwidth = s.length() + 1;
		
		System.out.print(padWithSpaces("", rowlabelminwidth));
		s = "" + (array[0].length - 1);
		int colminwidth = s.length() + 1;
		for (int i = 0; i < array[0].length; i++) {
			System.out.print(padWithSpaces("" + i, colminwidth));
		}
		System.out.println();
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(padWithSpaces("" + i, rowlabelminwidth));
			for (int j = 0; j < array[i].length; j++) {				
				System.out.print(padWithSpaces(array[i][j] ? "*" : " ", colminwidth));  
			}
			System.out.println();
		}
		
	}

	public static void main(String[] args) {
		new PrintBooleanMatrix().thing();
	}

}
