package net.pflager.leetcode.minimum_window_substring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

class Solution {
    boolean isMatch(int[] sCharCounts, int[] tCharCounts) {
        for (int i = 0; i < sCharCounts.length; i++) {
            if (tCharCounts[i] != 0 && sCharCounts[i] < tCharCounts[i])
                return false;
        }
        return true;
    }
            
    public String minWindow(String s, String t) {
        int[] tCharCounts = new int[128];
        for (int i = 0, tLength = t.length(); i < tLength; i++) {
            tCharCounts[t.charAt(i)]++;
        }

        int foundStart = 0;
        int foundLength = 0;
        int[] sCharCounts = new int[128];
        for (int head = 0, tail = 0, sLength = s.length(); head < sLength;) {
            sCharCounts[s.charAt(head++)]++;
            
            while (isMatch(sCharCounts, tCharCounts) && tail <= head) {
            	int length = head - tail;
                if (foundLength == 0 || length < foundLength) {
                    foundStart = tail;
                    foundLength = length;
                }
                sCharCounts[s.charAt(tail++)]--;
            }
        }
        return s.substring(foundStart, foundStart + foundLength);
    }
    
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;

//		exec = () -> assertEquals("BANC", minWindow("ADOBECODEBANC", "ABC"));
//		testName = "assertEquals(\"BANC\", minWindow(\"ADOBECODEBANC\", \"ABC\"))";
//		dTest = DynamicTest.dynamicTest(testName, exec);
//		dynamicTests.add(dTest);
//
//		exec = () -> assertEquals("", minWindow("a", "aa"));
//		testName = "assertEquals(\"\", minWindow(\"a\", \"aa\"))";
//		dTest = DynamicTest.dynamicTest(testName, exec);
//		dynamicTests.add(dTest);
//
//		exec = () -> assertEquals("baa", minWindow("bbaa", "aba"));
//		testName = "assertEquals(\"baa\", minWindow(\"bbaa\", \"aba\"))";
//		dTest = DynamicTest.dynamicTest(testName, exec);
//		dynamicTests.add(dTest);

		exec = () -> assertEquals("cwae", minWindow("cabwefgewcwaefgcf", "cae"));
		testName = "assertEquals(\"cwae\", minWindow(\"cabwefgewcwaefgcf\", \"cae\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}

}


