package net.pflager;

public class ArrayDistance
{
    public static int maxDistance(int arr[], int n)
    {
        // System.err.println(Arrays.toString(arr));
        for (int k = n - 1; k > 0; k--) {      // k holds the current distance being tested
//            if (tested[k])                      // starting with arr.length() - 1 (the greatest possible distance)
            // System.err.println("k == " + k);
            for (int i = 0; i < n - k; i++) {   // Q: How many positions have length k? A: n - k.
                // System.err.println("arr[" + i + "] == " + arr[i] + ", arr[" + (i + n - k) + "] == " + arr[i + n - k]);
                if (arr[i] == arr[i + k]) {
                    // System.err.println("return " + k);
                    return k;
                }
            }
        }

        // System.err.println("return 0");
        return 0;
    }
}
