package net.pflager;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestArrayDistance extends ArrayDistance {

        //3 2 1 2 1 4 5 8 6 7 4 2
        // 10
    @Test
    public void test_6x() {
        int[] a = {1, 1, 2, 2, 2, 1};
        assertEquals(maxDistance(a, a.length), 5);
    }

    @Test
    public void test_13x() {
        int[] a = {3, 2, 1, 2, 1, 4, 5, 8, 6, 7, 4, 2};
        assertEquals(maxDistance(a, a.length), 10);
    }

    @Test
    public void test_1() {
        int[] a = {1};
        assertEquals(maxDistance(a, a.length), 0);
    }


    @Test
    public void test_2a() {
        int[] a = {1, 1};
        assertEquals(maxDistance(a, a.length), 1);
    }

    @Test
    public void test_2b() {
        int[] a = {1, 2};
        assertEquals(maxDistance(a, a.length), 0);
    }

    @Test
    public void test_3a() {
        int[] a = {1, 1, 1};
        assertEquals(maxDistance(a, a.length), 2);
    }

    @Test
    public void test_3b() {
        int[] a = {2, 1, 1};
        assertEquals(maxDistance(a, a.length), 1);
    }

    @Test
    public void test_3c() {
        int[] a = {1, 2, 1};
        assertEquals(maxDistance(a, a.length), 2);
    }

    @Test
    public void test_3d() {
        int[] a = {1, 1, 2};
        assertEquals(maxDistance(a, a.length), 1);
    }

    @Test
    public void test_3e() {
        int[] a = {1, 2, 3};
        assertEquals(maxDistance(a, a.length), 0);
    }

    @Test
    public void test_4a() {
        int[] a = {1, 1, 1, 1};
        assertEquals(maxDistance(a, a.length), 3);
    }

    @Test
    public void test_4b() {
        int[] a = {2, 1, 1, 1};
        assertEquals(maxDistance(a, a.length), 2);
    }

    @Test
    public void test_4c() {
        int[] a = {1, 2, 1, 1};
        assertEquals(maxDistance(a, a.length), 3);
    }

    @Test
    public void test_4d() {
        int[] a = {1, 1, 2, 1};
        assertEquals(maxDistance(a, a.length), 3);
    }

    @Test
    public void test_4e() {
        int[] a = {1, 1, 1, 2};
        assertEquals(maxDistance(a, a.length), 2);
    }

    @Test
    public void test_4f() {
        int[] a = {2, 2, 1, 1};
        assertEquals(maxDistance(a, a.length), 1);
    }

    @Test
    public void test_4g() {
        int[] a = {2, 1, 2, 1};
        assertEquals(maxDistance(a, a.length), 2);
    }

    @Test
    public void test_4h() {
        int[] a = {2, 1, 1, 2};
        assertEquals(maxDistance(a, a.length), 3);
    }
}
