package net.pflager;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TestStringsMakingAnagrams extends StringsMakingAnagrams {

	@Test
	void test_a_a() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("a", "a"), 0);
	}

	@Test
	void test_a_b() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("a", "b"), 2);
	}

	@Test
	void test_b_a() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("b", "a"), 2);
	}

	@Test
	void test_a_aa() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("a", "aa"), 1);
	}

	@Test
	void test_a_ab() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("a", "ab"), 1);
	}

	@Test
	void test_aa_a() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("aa", "a"), 1);
	}

	@Test
	void test_ab_a() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("ab", "a"), 1);
	}

	@Test
	void test_cde_abc() {
		assertEquals(minimumDeletionCountToMakeAnagramsOfEachOther("cde", "abc"), 4);
	}
}
