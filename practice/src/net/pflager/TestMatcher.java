package net.pflager;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

class TestMatcher {
	
	@Test
	void testFindLines() throws FileNotFoundException {
		System.out.println(System.getProperty("user.dir"));

		try (Scanner scanner = new Scanner(new File("test/test/java/util/regex/testData/convertMeToAStringArray.txt"))) {
			String content = scanner.useDelimiter("\\Z").next();
			Matcher matcher = Pattern.compile(".*").matcher(content);
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println();
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println();
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println();
		}
	}

	@Test
	void testFindLineBreaks() throws FileNotFoundException {
		try (Scanner scanner = new Scanner(new File("test/test/java/util/regex/testData/convertMeToAStringArray.txt"))) {
			String content = scanner.useDelimiter("\\Z").next();
			Matcher matcher = Pattern.compile("\\R").matcher(content);
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println("\"" + Arrays.toString(matcher.group().getBytes()) + "\"");
			System.out.println();
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println("\"" + Arrays.toString(matcher.group().getBytes()) + "\"");
			System.out.println();
			assertTrue(matcher.find());
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println("\"" + matcher.group() + "\"");
			System.out.println("\"" + Arrays.toString(matcher.group().getBytes()) + "\"");
			System.out.println();			
		}
	}
	
	@Test
	void testFindTrimAndConvertLinesToJavaStrings() throws FileNotFoundException {
		try (Scanner scanner = new Scanner(new File("test/test/java/util/regex/testData/convertMeToAStringArray.txt"))) {
			String content = scanner.useDelimiter("\\Z").next();
			Matcher matcher = Pattern.compile(".*").matcher(content);
			while (matcher.find()) {
				String s = matcher.group().trim();
				if (s.isEmpty()) {
					continue;
				}
				System.out.println("\"" + matcher.group().trim() + "\"");
			}
			
		}
	}
	
	String testData[] = {
		"Goodbye bye bye world world world",
		"Sam went went to to to his business",
		"Reya is is the the best player in eye eye game",
		"in inthe",
		"Hello hello Ab aB"
	};
	
	@Test
	void testMatchDuplicateWordsReplaceWithFirstWord() {
		for (String s : testData) {
			//Matcher matcher = Pattern.compile("\\b(\\w+)(\\b\\W+\\b\\1\\b)*").matcher(s);
			//Matcher matcher = Pattern.compile("\\b(\\w+)\\b\\s+\\b\\1\\b").matcher(s);
			Matcher matcher = Pattern.compile("\\b(\\w+)((\\b\\W+\\b\\1\\b)*)").matcher(s);
			while(matcher.find()) {
				System.out.println();
				System.out.println("matcher.group() === \"" + matcher.group() + "\"");
				System.out.println("matcher.groupCount() === \"" + matcher.groupCount() + "\"");
				System.out.println("matcher.group(0) === \"" + matcher.group(0) + "\"");
				System.out.println("matcher.group(1) === \"" + matcher.group(1) + "\"");
				System.out.println("matcher.group(2) === \"" + matcher.group(2) + "\"");
				try {
				System.out.println("matcher.group(3) === \"" + matcher.group(3) + "\"");
				System.out.println("matcher.group(4) === \"" + matcher.group(4) + "\"");
				} catch (RuntimeException rte) {
					continue;
				}
			}
			// System.out.println(matcher.groupCount());
			// System.out.println(s.replaceAll(matcher.group(), matcher.group(2)));
			break;
		}
	}
	
}
