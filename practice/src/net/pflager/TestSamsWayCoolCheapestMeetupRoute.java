package net.pflager;
import java.util.Random;

import org.junit.jupiter.api.Test;

class TestSamsWayCoolCheapestMeetupRoute extends SamsWayCoolCheapestMeetupRoute {
	
	private static Random random = new Random();

	/*
	 * This displays the numeric contents of each element of a given matrix, as a decimal number.
	 * 
	 * It displays the matrix so that its columns will all align and be the same width, leaving a minimum of 1 space
	 * between columns. matrix[r][c] is preconditioned to be positive. matrix is also
	 * preconditioned to be a rectangular (non-ragged) 2d array.  Each column's contents are right-justified.
	 */
	public static String toString(int[][] matrix) {
		StringBuilder[][] matrixAsStringBuilders = new StringBuilder[matrix.length][];				
		int maxWidth = 0;
		for (int r = 0; r < matrix.length; r++) {
			matrixAsStringBuilders[r] = new StringBuilder[matrix[r].length];
			for (int c = 0; c < matrix[r].length; c++) {
				int width = matrix[r][c] == 0 ? 1 : (int)Math.log10(matrix[r][c]) + 1; // + 1 is minimum number of digits
				
				matrixAsStringBuilders[r][c] = new StringBuilder(Integer.toString(matrix[r][c]));
				assert width == matrixAsStringBuilders[r][c].length();
						
				maxWidth = maxWidth < width ? width : maxWidth;
			}
		}
		maxWidth++; // pad with single space

		StringBuilder spacesStringBuilder = new StringBuilder();
		for (int i = 0; i < maxWidth; i++) {
			spacesStringBuilder.append(' ');
		}
		
		StringBuilder sb = new StringBuilder();
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[r].length; c++) {
				matrixAsStringBuilders[r][c].insert(0, spacesStringBuilder.substring(0, maxWidth - matrixAsStringBuilders[r][c].length()));
				System.out.print(matrixAsStringBuilders[r][c].toString());
			}
			System.out.println();
		}
		
		return sb.toString();
	}

	/*
	 * This displays the numeric contents of each element of a given matrix, as a decimal number.
	 * 
	 * Like toString(int[][] matrix, Coordinate coordinateOfA, Coordinate coordinateOfB), this
	 * function arranges the matrix so that its columns will all align and be the same width, leaving only 1 space
	 * between the widest numeric elements. matrix[r][c] is preconditioned to be positive. matrix is also
	 * preconditioned to be a rectangular (non-ragged) 2d array.  Each column's contents are right-justified.
	 * 
	 * Unlike toString(int[][] matrix, Coordinate coordinateOfA, Coordinate coordinateOfB), this function
	 * also appends an 'A' to a specified matrix element's displayed numeric contents. It also appends a 'B' to a (possibly different)
	 * specified matrix element's displayed numeric contents. Both 'A' and 'B'  can be appended to the same matrix element's displayed numeric 
	 * contents. In this latter case A is displayed before B.  E.g. 243AB
	 */
	public static String toString(int[][] matrix, Coordinate coordinateOfA, Coordinate coordinateOfB) {
		StringBuilder[][] matrixAsStringBuilders = new StringBuilder[matrix.length][];				
		int maxWidth = 0;
		for (int r = 0; r < matrix.length; r++) {
			matrixAsStringBuilders[r] = new StringBuilder[matrix[r].length];
			for (int c = 0; c < matrix[r].length; c++) {
				int width = matrix[r][c] == 0 ? 1 : (int)Math.log10(matrix[r][c]) + 1; // + 1 is minimum number of digits
				
				matrixAsStringBuilders[r][c] = new StringBuilder(Integer.toString(matrix[r][c]));
				assert width == matrixAsStringBuilders[r][c].length();
						
				maxWidth = maxWidth < width ? width : maxWidth;
			}
		}
		maxWidth++; // pad with single space to separate columns

		StringBuilder spacesStringBuilder = new StringBuilder();
		for (int i = 0; i < maxWidth; i++) {
			spacesStringBuilder.append(' ');
		}
		
		StringBuilder sb = new StringBuilder();
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[r].length; c++) {
				matrixAsStringBuilders[r][c].insert(0, spacesStringBuilder.substring(0, maxWidth - matrixAsStringBuilders[r][c].length()));
				if (coordinateOfA.row == r && coordinateOfA.col == c) {
					matrixAsStringBuilders[r][c].append('A');
				}

				if (coordinateOfB.row == r && coordinateOfB.col == c) {
					matrixAsStringBuilders[r][c].append('B');
				} else {
					matrixAsStringBuilders[r][c].append(' ');
				}
				
				if (coordinateOfA.row != r || coordinateOfA.col != c) {
					matrixAsStringBuilders[r][c].append(' ');
				}

				System.out.print(matrixAsStringBuilders[r][c].toString());
			}
			System.out.println();
		}
		
		return sb.toString();
	}

	
	
	@Test
	void testManuallyCreated3x3_1() {
		int[][] testMatrix = new int[][] { new int[] {1, 2, 3}, new int[] {4, 5, 6}, new int[] {7, 8, 9} };
		
		System.out.println(toString(testMatrix));
	}

	@Test
	// Arbitrary upper limit of 100 (exclusive) on matrix element values.
	void testRandomlyGenerated3x3_1() { 
		int[][] testMatrix = new int[3][3];
		
		for (int r = 0; r < testMatrix.length; r++) {
			for (int c = 0; c < testMatrix[r].length; c++) {
				testMatrix[r][c] = random.nextInt(100);
			}
		}
		
		System.out.println(toString(testMatrix));
	}

	@Test
	// Arbitrary upper limit of 100 (exclusive) on matrix element values.
	void testRandomlyGenerated7x10_1() { 
		int[][] testMatrix = new int[7][10];
		Random random = new Random();
		
		for (int r = 0; r < testMatrix.length; r++) {
			for (int c = 0; c < testMatrix[r].length; c++) {
				testMatrix[r][c] = random.nextInt(100);
			}
		}
		
		System.out.println(toString(testMatrix));
	}
	
	@Test
	// Arbitrary upper limit of 100 (exclusive) on matrix element values.
	void testRandomlyGenerated9x100_1() { 
		int[][] testMatrix = new int[9][100];
		Random random = new Random();
		
		for (int r = 0; r < testMatrix.length; r++) {
			for (int c = 0; c < testMatrix[r].length; c++) {
				testMatrix[r][c] = random.nextInt(100);
			}
		}
		
		System.out.println(toString(testMatrix));
	}

	@Test
	// Arbitrary upper limit of 100 (exclusive) on matrix element values.
	void testRandomlyGenerated9x100_2() { 
		int[][] testMatrix = new int[9][100];
		Random random = new Random();
		
		for (int r = 0; r < testMatrix.length; r++) {
			for (int c = 0; c < testMatrix[r].length; c++) {
				testMatrix[r][c] = random.nextInt(100);
			}
		}
		
		Coordinate coordinateOfA = new Coordinate(random.nextInt(testMatrix.length), random.nextInt(testMatrix[0].length));
		
		Coordinate coordinateOfB = new Coordinate(random.nextInt(testMatrix.length), random.nextInt(testMatrix[0].length));
		
		System.out.println(toString(testMatrix, coordinateOfA, coordinateOfB));
	}

	@Test
	// Arbitrary upper limit of 100 (exclusive) on matrix element values.
	void testRandomlyGenerated9x100_3() { 
		int[][] testMatrix = new int[9][100];
		Random random = new Random();
		
		for (int r = 0; r < testMatrix.length; r++) {
			for (int c = 0; c < testMatrix[r].length; c++) {
				testMatrix[r][c] = random.nextInt(100);
			}
		}
		
		Coordinate coordinateOfA = new Coordinate(0, 0);
		
		Coordinate coordinateOfB = new Coordinate(0, 0);
		
		System.out.println(toString(testMatrix, coordinateOfA, coordinateOfB));
	}
	
	//@Test
	void testBinaryTreeEmptyMatrix() {
		
	}
	
	//@Test
	void testBinaryTreeSingleElementMatrix() {
		
	}
	
	//@Test
	void testBinaryTree2ElementVerticalMatrix() {
		
	}

	//@Test
	void testBinaryTree2ElementHorizontalMatrix() {
		
	}

	@Test
	void testBinaryTree2ElementSquareMatrix1() {
		int[][] matrix = new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9} };
		
		System.out.println(interpretMatrixAsBinaryTree(matrix, 0, 0, 0, new StringBuffer()));
	}
	
}
