package net.pflager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

public class EristothenesSieve {
  static boolean[] isComposite = new boolean[1_000_000];
  static {
    for (int i = 2; i * i < isComposite.length;) {
      for (int j = i * i; j < isComposite.length; j += i) {
        isComposite[j] = true;
      }
      while (++i < isComposite.length && isComposite[i])
        ;
    }
  }

  static boolean isPrime(int i) {
    if (i >= isComposite.length)
      throw new IllegalArgumentException("i must be less than " + isComposite.length);

    if (i < 2)
      throw new IllegalArgumentException("i must be greater than 2");

    return !isComposite[i];
  }

  @TestFactory
  public Collection<DynamicTest> translateDynamicTests() {
    Collection<DynamicTest> dynamicTests = new ArrayList<>();

    Executable exec;
    String testName;
    DynamicTest dTest;

    exec = () -> assertEquals(true, isPrime(2));
    testName = "assertEquals(true, isPrime(2))";
    dTest = DynamicTest.dynamicTest(testName, exec);
    dynamicTests.add(dTest);

    return dynamicTests;

  }

}
