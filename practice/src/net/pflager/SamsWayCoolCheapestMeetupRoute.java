package net.pflager;
/*
 * You are given an m x n array, where 0 < n \le 2^31-1, and where 0 < n \le 2^31-1, and where each array element
 * contains a non-negative integer.
 * 
 * You have two persons. One person called A is located at (0, 0). 
 * There is another person called B located at (m-1, n-1). 
 *
 * The ordered pair (0, 0) means (row = 0, column = 0), numbering rows starting at zero, going from top to bottom, 
 * and numbering columns starting at zero, going from left to right.
 * 
 * A and B take turns moving.  They may move horizontally or vertically within the bounds of the array.  They may NOT
 * move diagonally.
 * 
 * There is a cost associated with moving into a new array element.  The cost is the number of the array element (in dollars)
 * the person moves out of.
 * 
 * e.g.   1A 2  3
 *        4  5  6
 *        7  8  9B
 *        
 * A's position is the top left corner of the array.
 * B's position is the bottom right corner of the array.
 * 
 * A can move from (0, 0) to (0, 1) or from (0, 0) to (1, 0)
 * In either case the cost of that move is the value at (0, 0), which is 1 (dollar).
 * 
 * B can move from (2, 2) to (2, 1) or from (2, 2) to (1, 2).
 * In either case the cost of that move is the value at (2, 2), which is 9 (dollars).
 * 
 * The goal is to find the location of the array element where both A and B can meet which costs them the least total dollars.
 * In other words, the sum of their individual costs must be the lowest total cost to meet.
 * 
 */
public class SamsWayCoolCheapestMeetupRoute {
	public static class Coordinate {
		public Coordinate(int row, int col) {
			this.row = row;
			this.col = col;
		}

		public int row;
		public int col;
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder("(");
			sb.append(row);
			sb.append(", ");
			sb.append(col);
			sb.append(')');
			return sb.toString();
		}
	}

	public static class BinaryTree {
		public BinaryTree(Coordinate coordinate) {
			this.coordinate = coordinate;
		}

		Coordinate coordinate;
		BinaryTree horizontal;
		BinaryTree vertical;
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			
			return sb.toString();
		}
	}

	/*
	 * Given a coordinate, produce a binary tree who's nodes have a value equal to
	 * the cumulative sum of the integers of all nodes in its direct lineage. Each
	 * node corresponds to an element of the given matrix. The left branch of a
	 * non-leaf node is a reference to a node corresponding to the matrix element
	 * that can be reached by traveling horizontally in the matrix. The right branch
	 * of a non-leaf node is a reference to a node corresponding to the matrix
	 * element that can be reached by traveling vertically in the matrix.
	 * 
	 * Each matrix element appears only once in the tree. In other words, each
	 * matrix element is visited only once.
	 * 
	 * This function only traverses horizontally and vertically in increasing values
	 * of row and column.
	 * 
	 * TODO: Must re-order the matrix achieve the same result as starting from the
	 * bottom right and proceeding only in decreasing values of row and column.
	 * 
	 */
	public static BinaryTree interpretMatrixAsBinaryTree(int[][] matrix, int row, int column, int sum, StringBuffer route) {
		if (row >= matrix.length) {
			return null;
		}

		if (column >= matrix[0].length) {
			return null;
		}
		
		BinaryTree binaryTree = new BinaryTree(new Coordinate(row, column));
		System.out.println(binaryTree.coordinate + " " + (matrix[row][column] + sum));
		if (route.length() != 0) {
			route.append(" => ");
		}
		route.append(binaryTree.coordinate);
		route.append(" ");
		route.append(matrix[row][column] + sum);
		System.out.println(route.toString());
		System.out.println();

		binaryTree.horizontal = interpretMatrixAsBinaryTree(matrix, row, column + 1, matrix[row][column] + sum, new StringBuffer(route));

		binaryTree.vertical = interpretMatrixAsBinaryTree(matrix, row + 1, column, matrix[row][column] + sum, new StringBuffer(route));

		return binaryTree;
	}
}
