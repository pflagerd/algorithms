package net.pflager;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class BinarySearchTree<T extends Comparable<T>>{

	T value;

	BinarySearchTree<T> left;
	BinarySearchTree<T> right;
	
	public BinarySearchTree(T value) {
		this.value = value;
	}
	
	public BinarySearchTree<T> add(T value) {
		if (value == null) {
			throw new IllegalArgumentException("You must pass a non-null value");
		}
		
		if (value.compareTo(this.value) < 0) {
			if (left == null) {
				left = new BinarySearchTree<T>(value);
				return this;
			}
			left.add(value);
			return this;
		} 
		// >= 
		
		if (right == null) {
			right = new BinarySearchTree<T>(value);			
		}
		
		return this;
	}

	public int size() {
		return 0;
	}

	public boolean isEmpty() {
		return false;
	}

	public boolean contains(Object o) {
		return false;
	}

	public Iterator<?> iterator() {
		return null;
	}

	public Object[] toArray() {
		return null;
	}

	public Object[] toArray(Object[] a) {
		return null;
	}

	public boolean remove(Object o) {
		return false;
	}

	public boolean containsAll(Collection<?> c) {
		return false;
	}

	public boolean addAll(Collection<?> c) {
		return false;
	}

	public boolean addAll(int index, Collection<?> c) {
		return false;
	}

	public boolean removeAll(Collection<?> c) {
		return false;
	}

	public boolean retainAll(Collection<?> c) {
		return false;
	}

	public void clear() {
		
	}

	public Object get(int index) {
		return null;
	}

	public Object set(int index, Object element) {
		return null;
	}

	public void add(int index, Object element) {
		
	}

	public Object remove(int index) {
		return null;
	}

	public int indexOf(Object o) {
		return 0;
	}

	public int lastIndexOf(Object o) {
		return 0;
	}

	public ListIterator<?> listIterator() {
		return null;
	}

	public ListIterator<?> listIterator(int index) {
		return null;
	}

	public List<?> subList(int fromIndex, int toIndex) {
		return null;
	}
}
