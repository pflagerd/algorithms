package net.pflager;

import org.apache.commons.math3.util.CombinatoricsUtils;

public class AllSubstrings {

//    private static void printAllSubStringsStartPositionFixed(String s) {
//        for (int i = 0; i < s.length(); i++) { // i is starting position
//            for (int j = 1; j <= s.length() - i; j++) { // j is current length
//                System.out.println(s.substring(i, i + j));
//            }
//
//        }
//    }

//    private static void printAllSubStringsLengthFixed(String s) {
//        for (int j = 1; j <= s.length(); j++) { // j is current length
//            for (int i = 0; i < s.length() - j + 1; i++) { // i is starting position
//                System.out.println(s.substring(i, i + j));
//            }
//        }
//    }

    public static void main(String[] args) {
        //printAllSubStringsStartPositionFixed("abcd");
        //printAllSubStringsLengthFixed("abcd");
        for (int i = 1; i < 20; i++) {
            System.out.println(i * (i + 1) / 2);
            System.out.println(CombinatoricsUtils.binomialCoefficient(i + 1, 2));
        }

    }
}
