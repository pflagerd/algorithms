package net.pflager;

public class SquareRoot200118 {
    double sqrt(double n) {
        if (n < 0) {
            return -Double.NaN;
        }

        double t = n;
        while (Math.abs(n/t - t) > Math.ulp(t)) {
            t = (n/t + t) / 2.0;
        }
        return t;
    }
}