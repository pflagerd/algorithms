package net.pflager;
import java.util.HashSet;

public class BeautifulQuadruples {
	static HashSet<String> hashSet = new HashSet<>();
	
	static String makeKey(int a, int b, int c, int d) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(a);
		sb.append(" ");
		sb.append(b);
		sb.append(" ");
		sb.append(c);
		sb.append(" ");
		sb.append(d);
		
		return sb.toString();
	}
	
	static int[] sort(int a, int b, int c, int d) {
		int[] sorted = new int[4];
		sorted[0] = a;
		sorted[1] = b;
		sorted[2] = c;
		sorted[3] = d;

		if (b < a) {
			sorted[1] = a;
			sorted[0] = b;
		}

		if (c < sorted[1]) {
			if (c < sorted[0]) {
				sorted[2] = sorted[1];
				sorted[1] = sorted[0];
				sorted[0] = c;
			} else {
				sorted[2] = sorted[1];
				sorted[1] = c;
			}
		}

		if (d < sorted[2]) {
			if (d < sorted[1]) {
				if (d < sorted[0]) {
					sorted[3] = sorted[2];
					sorted[2] = sorted[1];
					sorted[1] = sorted[0];
					sorted[0] = d;
				} else {
					sorted[3] = sorted[2];
					sorted[2] = sorted[1];
					sorted[1] = d;
				}
			} else {
				sorted[3] = sorted[2];
				sorted[2] = d;
			}
		}

		return sorted;
	}

	static int beautifulQuadruples(int a, int b, int c, int d) {
		int[] sorted = sort(a, b, c, d);
		a = sorted[0];
		b = sorted[1];
		c = sorted[2];
		d = sorted[3];

		System.out.println(a + " " + b + " " + c + " " + d);

		int count = 0;

		for (int i = 1; i < a + 1; i++) {
			for (int j = i; j < b + 1; j++) {
				for (int k = j; k < c + 1; k++) {
					for (int l = k; l < d + 1; l++) {						
						if ((i ^ j ^ k ^ l) != 0 && !hashSet.contains(makeKey(i, j, k, l))) {
							count++;
							hashSet.add(makeKey(i, j, k, l));
						}
					}
				}
			}
		}

		return count;
	}

	public static void main(String[] args) {
		long start = System.nanoTime();
		long startMillis = System.currentTimeMillis();
		
		System.out.println(beautifulQuadruples(1, 2, 3, 4));
		
		System.out.println(beautifulQuadruples(60, 2, 80, 12));

		long end = System.nanoTime();
		long endMillis = System.currentTimeMillis();
		System.out.println("Done nanos: " + (end - start));
		System.out.println("Done millis: " + (endMillis - startMillis));
	}

}
