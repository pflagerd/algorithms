const canvas = document.getElementsByTagName("canvas").item(0);
document.body.append(canvas);
canvas.context = canvas.getContext("2d");
canvas.scale = 0;

canvas.render = ((thing) => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    this.scale = Math.min(canvas.width, canvas.height) / 2; // normalize to between -1 and 1 using \frac{2}{this.scale} as the scaling factor to convert from normalized coordinates to screen coordinates

    console.log("this === " + this + " width === " + canvas.width + " height === " + canvas.height + " scale === " + this.scale);
    window.requestAnimationFrame(canvas.render);
}).bind(canvas);

window.requestAnimationFrame(canvas.render);

