package net.pflager;

public class SimpleLoop {

    public static void main(String[] args) {
        String s = "The quick brown fox jumps over the lazy dog.";
        int sum = 0;
        final int length = s.length();
        for (int i = 0; i < length; i++) {
            sum += s.charAt(0);
        }
        System.out.println(sum);
    }
}
