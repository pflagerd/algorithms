package net.pflager;

import java.util.ArrayDeque;

public class BinaryTree {
    private BinaryTree left;
    private BinaryTree right;


    private static ArrayDeque<BinaryTree> arrayDeque = new ArrayDeque<>();

    public static void levelOrderSearch(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }

        arrayDeque.add(binaryTree);

        while (arrayDeque.size() != 0) {
            BinaryTree subTree = arrayDeque.getFirst();

            // Process root

            if (subTree.left != null) {
                arrayDeque.add(subTree.left);
            }

            if (subTree.right != null) {
                arrayDeque.add(subTree.right);
            }
        }

    }
}
