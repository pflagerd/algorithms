package net.pflager;

public class SquareRoot {
	static final double maxError = 1e-15;

	public static double sqrt(double c) {
		if (c < 0) {
			throw new IllegalArgumentException("n cannot be negative");
		}
		double t = c, last_t = -1.0;
		while (t != last_t) {
			last_t = t;
			t = (c / t + t) / 2.0;
		}
		return t;
	}

	public static double sqrtOld(double c) {
		if (c < 0) {
			throw new IllegalArgumentException("n cannot be negative");
		}
		double t = c;
		while (Math.abs(c / t - t) > maxError * t) {
			t = (c / t + t) / 2.0;
		}
		return t;
	}
	
	private static double nearestLowerPowerOf2(double c) {
		double log2 = 1;
		while ((c /= 2) > 1) {
			log2 *= 2;
		}
		return log2;
	}

	//
	// F(x) = x * x
	//
	public static double sqrtNaive1(double c) {
		if (c < 0) {
			throw new IllegalArgumentException("n cannot be negative");
		}

		double t = c / 2;
		int iterations = 0;
		double correction = 16;
		while (true) {
			iterations++;
			if (t * t - c > maxError) {
				t -= (t * t - c) / correction;
			} else if (t * t - c < -maxError) {
				t += (t * t + c) / correction;
			}
			else
				return t;
			System.err.println(iterations + " " + t);
		}
	}
	
	//
	// F(x) = x * x
	//
	public static double sqrtNaive2(double c) {
		if (c < 0) {
			throw new IllegalArgumentException("n cannot be negative");
		}

		double t = c / 2;
		double correction = nearestLowerPowerOf2(c);
		int iterations = 0;
		while (true) {
			iterations++;
			if (t * t - c > maxError) {
				t -= (t * t - c) / correction;
			} else if (t * t - c < -maxError) {
				t += (t * t + c) / correction;
			}
			else
				return t;
			System.err.println(iterations + " " + t);
		}
	}
	
	public static void main(String[] args) {
		System.out.println(sqrtNaive1(9));
	}
}
