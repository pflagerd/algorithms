package net.pflager;

public class DiscreteHarmonicSeries {

		public static int computeSeries(int n) {
			int sum = 0;
			for (int i = 1; n / i != 0; sum += n / i, i++);
			return sum;
		}
	
	
		public static void main(String[] args) {
			for (int i = 1; i < 1000; i++) {
				System.out.println(i + " " + computeSeries(i));
			}
		}
}
