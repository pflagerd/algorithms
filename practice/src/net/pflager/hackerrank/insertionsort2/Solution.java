package net.pflager.hackerrank.insertionsort2;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Stream;

class Result {

    public static String format(List<Integer> arr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0, arrSize = arr.size(); i < arrSize; i++) {
            if (i != 0)
                stringBuilder.append(" ");
            stringBuilder.append(arr.get(i));
        }
        return stringBuilder.toString();
    }

    /*
     * Complete the 'insertionSort2' function below.
     *
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. INTEGER_ARRAY arr
     */

    public static void insertionSort2(int n, List<Integer> arr) {
        if (n < 0 || arr == null)
            throw new IllegalArgumentException("n must be >= 0, arr must be non-null");
        
        for (int i = 0, arrSize = arr.size(); i < arrSize - 1; i++) {
            for (int j = i; j >= 0 && arr.get(j) > arr.get(j + 1); j--) {
                int tmp = arr.get(j); 
                arr.set(j, arr.get(j + 1)); 
                arr.set(j + 1, tmp);                
            }
            System.out.println(format(arr));
        }
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        Result.insertionSort2(n, arr);

        bufferedReader.close();
    }
}
