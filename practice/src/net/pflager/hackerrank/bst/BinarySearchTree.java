package net.pflager.hackerrank.bst;

class EmptyBinarySearchTree<D extends Comparable<D>> implements Tree<D> {
	
	@Override
	public int cardinality() {
		return 0;
	}

	@Override
	public boolean isEmpty() {
		return true;
	}

	@Override
	public boolean member(D element) {
		return false;
	}

	@Override
	public NonEmptyBinarySearchTree<D> add(D element) {
		return new NonEmptyBinarySearchTree<D>(element);
	}
	
	public String toString(int offset) {
		return "";
	}
	
}

class NonEmptyBinarySearchTree<D extends Comparable<D>> implements Tree<D> {
	D element;
	Tree<D> left;
	Tree<D> right;
		
	public NonEmptyBinarySearchTree(D element) {
		this.element = element;
		left = new EmptyBinarySearchTree<D>();
		left = new EmptyBinarySearchTree<D>();
	}
	
	public NonEmptyBinarySearchTree(D element, Tree<D> left, Tree<D> right) {
		this.element = element;
		this.left = left;
		this.right = right;
	}

	@Override
	public int cardinality() {
		return 1 + left.cardinality() + right.cardinality();
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public boolean member(D element) {
		return element == this.element || this.element.compareTo(element) < 0 ? left.member(element) : right.member(element);
	}

	@Override
	public NonEmptyBinarySearchTree<D> add(D element) {
		if (element == this.element) {
			return this;
		}
		
		return element.compareTo(element) < 0 ? new NonEmptyBinarySearchTree<D>(element, left.add(element), right): new NonEmptyBinarySearchTree<D>(element, left, right.add(element));
	}
	
	@Override
	public String toString() {
		return "";
	}
	
}

public class BinarySearchTree {

}

interface Tree<D extends Comparable<D>> {
	public int cardinality();

	public boolean isEmpty();
	
	public boolean member(D element);
	
	public NonEmptyBinarySearchTree<D> add (D element);
}

