package net.pflager;

import java.util.ArrayList;
import java.util.List;

public class ListOfListOfString {
	
	public static void main(String[] args) {
		List<List<String>> lls = new ArrayList<>();
		List<List<String>> lls1 = new ArrayList<List<String>>();
		// Type mismatch:*-.
		
		// List<List<String>> lls2 = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> lls3 = new ArrayList<>(); 
		ArrayList<ArrayList<String>> lls4 = new ArrayList<ArrayList<String>>(); 
		System.out.println(lls);
		System.out.println(lls1);
		// System.out.println(lls2);
		System.out.println(lls3);
		System.out.println(lls4);
	}

}
