package net.pflager;

public class AllNumbersWithSpecificDifference3558 {
	
	static long sumDigits(long l) {
		long sum = 0;
		while (l != 0) {
			sum += l % 10;
			l /= 10;
		}
		return sum;
	}
	
	static long diffWithSumDigits(long l) {
		return l - sumDigits(l);
	}
	
	static int nDigits(long l) {
		int n = 0;
		while (l != 0) {
			n++;
			l /= 10;
		}
		return n;
	}
	
	static long pow10(int n) {
		long pow = 1;
		for (int i = 0; i < n; i++) {
			pow *= 10;
		}
		return pow;
	}
	
	static long invDiffWithSumDigits(long l) { // like long division
		long divisor = pow10(nDigits(l) - 1) - 1;
		long quotient = 0;
		
		while (l > 9) {
			long nextDigit = l / divisor;
			l -= nextDigit * divisor;
			quotient *= 10;
			quotient += nextDigit;
			divisor /= 10;
		}
		
		return quotient * 10;
	}

	public static void main(String[] args) {
			long N = 475125762500L;
			long D = 372561197206L;

			//N = 23814200355280L;
			//D = 292492901550L;
			
			//N = 5082861158424;
			//D = 948817099230;
			System.out.println(N + " " + D + "\n");

			
			System.out.println("diffWithSumDigits(D) = " + diffWithSumDigits(D) + "\n");
			System.out.println("invDiffWithSumDigits(D) = " + invDiffWithSumDigits(D) + "\n");
			System.out.println("diffWithSumDigits(invDiffWithSumDigits(D)) = " + diffWithSumDigits(invDiffWithSumDigits(D)) + "\n");
			
			long invDiffWithSumDigits = invDiffWithSumDigits(D);
			
			if (invDiffWithSumDigits < D) {
				System.out.println("diffWithSumDigits(invDiffWithSumDigits(D) / 10 * 10 + 10) = " + diffWithSumDigits(invDiffWithSumDigits(D) / 10 * 10 + 10) + "\n");
				System.out.println("N - diffWithSumDigits(invDiffWithSumDigits(D)) + 1 = " + (N - diffWithSumDigits(invDiffWithSumDigits(D) / 10 * 10 + 10) + 1) + "\n");
			} else {
				System.out.println("invDiffWithSumDigits(D) / 10 * 10 = " + invDiffWithSumDigits(D) / 10 * 10 + "\n");
				System.out.println("N - invDiffWithSumDigits(D) / 10 * 10 + 1 = " + (N - invDiffWithSumDigits(D) / 10 * 10 + 1) + "\n");
			}
						
			long d0;
			long D0 = D;
			D = D / 10 * 10;
			d0 = diffWithSumDigits(D);
			System.out.println(D + " " + sumDigits(D) + " " + d0);
			long d = D;
			int count = 1;
			while ((d = diffWithSumDigits(D += 10)) < D0) {
				System.out.println(count + " " + D + " " + sumDigits(D) + " " + d);
				count++;
			}
			System.out.println(count + " " + D + " " + sumDigits(D) + " " + d);

			System.out.println();
			System.out.println(N < D ? 0 : N - D + 1);
	}

}
