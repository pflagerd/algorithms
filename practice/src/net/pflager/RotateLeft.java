package net.pflager;

import java.util.Scanner;

public class RotateLeft {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        int t = scanner.nextInt();
        for (int i = 0; i < t; i++) {
            scanner.nextLine();
            int n = scanner.nextInt();
            int d = scanner.nextInt(); scanner.nextLine();
            int[] array = new int[n];
            int k = n - d;
            for (int j = 0; j < n; j++) {
                array[k] = scanner.nextInt();
                k = k < n - 1 ? k + 1 : 0;
            }
            for (int j = 0; j < n; j++) {
                if (j != 0) {
                    System.out.print(" ");
                }
                System.out.print(array[j]);
            }
            System.out.println();
        }

        scanner.close();
    }
}