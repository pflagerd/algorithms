Given a rectangular array of 1s and 0s, of dimension m x n, for each 1 in an element of a given row, zero all elements in that elements row and column. What is the resulting array.



array

1 0 0 1 0

0 0 0 1 0

0 1 0 0 1

1 0 0 1 0



for row = 2 (numbering from 0), what's the resulting array

For the example above:

1 0 0 1 0

0 0 0 1 0

0 0 0 0 0

1 0 0 1 0

