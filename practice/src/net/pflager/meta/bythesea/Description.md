Given an array of integers, where each element represents the height of a building, arranged so that the element with the highest index is closest to the ocean, and each element with a successively lower index is successively further from the ocean, which building furthest from the shore can see the ocean.



Time O(n)

Space O(1)

