package net.pflager;

public class HowShortCutOperatorsWork {

    private static char [] truthyc = { 'F', 'T' };

    public static void main(String[] args) {
        System.out.println("a b c | a && (b || c) | b || a && c");
        for (int a = 0; a <= 1; a++)
            for (int b = 0; b <= 1; b++)
                for (int c = 0; c <= 1; c++) {
                    System.out.println(truthyc[a] + " " + truthyc[b] + " " + truthyc[c] + " |       " + ((a != 0 && (b != 0 || c != 0)) ? "T" : "F")  + " |            " + ((b != 0 || a != 0 && c != 0) ? "T" : "F"));
                }
    }
}
