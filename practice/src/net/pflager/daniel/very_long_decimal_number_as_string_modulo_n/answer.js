/* int */ function remainder(/* string */ ds, /* int */ n) {
  let accumulator = 0;
  for (let i = 0; i < ds.length; i++) {
    let digit = ds.charAt(i) - '0';
    console.log(digit);
    accumulator *= (10 % n);
    accumulator %= n;
    accumulator += digit % n;
  }
  return accumulator;
}