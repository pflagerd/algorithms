package net.pflager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
	
	public static final String regex = "(\\w+)(\\W+\\1)+";
	
	static final Pattern regexPattern = Pattern.compile(regex);
	
	public static String cat(String[] strings) {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (String string : strings) {
			if (stringBuilder.length() != 0) {
				stringBuilder.append(" ");
			}
			stringBuilder.append(string);
		}
		
		return stringBuilder.toString();
	}

	public static String removeDuplicateWords(String in) {
		Matcher matcher = regexPattern.matcher(in);
		
		while (matcher.find()) {			
			System.out.println(matcher.groupCount());
			for (int i = 0; i <= matcher.groupCount(); i++) {
				System.out.println(matcher.group(i));
			}
			System.out.println();
		}
		
		return "";
	}
	
	public static void main(String[] args) {
		System.out.println(removeDuplicateWords(cat(args)));
	}
}
