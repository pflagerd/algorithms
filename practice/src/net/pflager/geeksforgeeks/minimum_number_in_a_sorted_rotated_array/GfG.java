package net.pflager.geeksforgeeks.minimum_number_in_a_sorted_rotated_array;

//{ Driver Code Starts
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GfG {
	public static void main(String args[]) throws IOException {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(read.readLine());

		while (t-- > 0) {
			int n = Integer.parseInt(read.readLine());
			String st[] = read.readLine().trim().split("\\s+");

			int arr[] = new int[n];

			for (int i = 0; i < n; i++)
				arr[i] = Integer.parseInt(st[i]);

			System.out.println(Solution.minNumber(arr, 0, n - 1));
		}
	}

}// } Driver Code Ends

class Solution {

	private static int predecessor(int[] a, int i) {
		return i == 0 ? a[a.length - 1] : a[i - 1];
	}

	private static int successor(int[] a, int i) {
		return i == a.length - 1 ? a[0] : a[i + 1];
	}

	//
	// a) Assume rotation r > 0 && r < arr.length && r means rotate to the
	// <b>r</b>ight.
	// b) Assume arr is sorted ascending: arr[0] > arr[arr.length] && forall (int i
	// = 0; i < arr.length - 1; i++) { arr[i] < arr[i + 1] }
	//
	//
	//
	//
//	static int minNumberIfSortedAscending(int[] arr, int low, int high) {
//		if (arr[low] < arr[high]) { // (r == 0 && "arr sorted ascending" == true) or (r != 0 && "arr sorted descending" == true)
//			
//		} else {  // (r == 0 && "arr sorted descending" == true) or (r != 0 && "arr sorted ascending" == true)
//			
//		}
//		return 0;
//	}

	static int minNumberIfSortedAscending(int[] arr, int low, int high) {
	    // This condition is needed to handle the case when array
	    // is not rotated at all
	    if (high < low)  return arr[0];
	
	    // If there is only one element left
	    if (high == low) return arr[low];
	
	    // Find mid
	    int mid = low + (high - low)/2; /*(low + high)/2;*/
	
	    // Check if element (mid+1) is minimum element. Consider
	    // the cases like {3, 4, 5, 1, 2}
	    if (mid < high && arr[mid+1] < arr[mid])
	        return arr[mid+1];
	
	    // Check if mid itself is minimum element
	    if (mid > low && arr[mid] < arr[mid - 1])
	        return arr[mid];
	
	    // Decide whether we need to go to left half or right half
	    if (arr[high] > arr[mid])
	        return minNumberIfSortedAscending(arr, low, mid-1);
	    return minNumberIfSortedAscending(arr, mid+1, high);
	}
	
	
	static int minNumber(int[] arr, int low, int high) {
		return minNumberIfSortedAscending(arr, low, high);
	}

	// Function to find the minimum element in sorted and rotated array.
	// Find element whose predecessor and successor are both greater than or equal
	// itself
	static int minNumberModuloIndexingAscending(int arr[], int low, int high) {
		high = (arr.length << 1) - 1; // use non-negative modulo arithmetic. "double" the length less one
		while (low <= high) {
			int middle = (low + (high - low) / 2) % arr.length;
			if (predecessor(arr, middle) >= arr[middle] && arr[middle] <= successor(arr, middle))
				return arr[middle];
			else { // ascending, therefore must be in lower half
				high = middle;
			}
		}

		return 0;
	}
}