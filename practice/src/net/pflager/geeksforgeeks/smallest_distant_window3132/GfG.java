package net.pflager.geeksforgeeks.smallest_distant_window3132;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GfG {
	public static void main(String[] args) throws IOException
	{
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      int t = Integer.parseInt(br.readLine().trim());
      while(t-->0)
      {
          String str = br.readLine();
          
          Solution obj = new Solution();
          System.out.println(obj.findSubString(str));
          
      }
	}
}


//} Driver Code Ends


//User function Template for Java

class Solution {
  public int findSubString(String str) {
      if (str == null)
          return -1;
          
      int strLength = str.length();
      if (strLength == 0)
          return -1;
          
      if (str.length() == 1)
          return 1;

      boolean[] charsFound = new boolean[128];
      int distinctCharsToBeFound = 0;
      for (int i = 0; i < strLength; i++) {
          char c = str.charAt(i);
          if (!charsFound[c]) {
              distinctCharsToBeFound++;
              charsFound[c] = true;
          }
      }
      int distinctCharsNotYetFound = distinctCharsToBeFound;
      int[] charCounts = new int[128];
      
      int head = 0;
      for (; distinctCharsNotYetFound > 0; head++) {
          if (head == strLength) {
              return -1;
          }
          
          if (charCounts[str.charAt(head)]++ == 0)
              distinctCharsNotYetFound--;
      }
      int minWindowSize = head;
      
      int tail = 0;
      while (charCounts[str.charAt(tail)] > 1) {
          charCounts[str.charAt(tail++)]--;
          minWindowSize--;
          if (minWindowSize == distinctCharsToBeFound)
              return minWindowSize;
      }

      // move head and tail together now
      for (; head < strLength; head++) {
          if (charCounts[str.charAt(head)]++ == 0)
              distinctCharsNotYetFound--;
              
          if (charCounts[str.charAt(tail++)]-- == 1)
              distinctCharsNotYetFound++;

          while (distinctCharsNotYetFound == 0 && charCounts[str.charAt(tail)] > 1) {
              charCounts[str.charAt(tail++)]--;
              minWindowSize--;
              if (minWindowSize == distinctCharsToBeFound)
                  return minWindowSize;
          }
          
      }
      
      return minWindowSize;
  }

}