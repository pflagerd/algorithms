package net.pflager.geeksforgeeks.segregate_0s_and_1s;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GFG {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            inputLine = br.readLine().trim().split(" ");
            int n = Integer.parseInt(inputLine[0]);
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }
            new Solution().segregate0and1(arr, n);
            for (int i = 0; i < n; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
    }
}

// } Driver Code Ends


//User function Template for Java

class Solution {
    void segregate0and1(int[] arr, int n) {

        for (int left = 0, right = n - 1; left < right; ) {
            /* Increment left index while we see 0 at left */
            if (arr[left] == 0) {
                left++;
                continue;
            }

            /* Decrement right index while we see 1 at right */
            if (arr[right] == 1) {
                right--;
                continue;
            }

            /* If left is smaller than right then there is a 1 at left
               and a 0 at right.  Exchange arr[left] and arr[right]*/
            arr[left] = 0;
            arr[right] = 1;
            left++;
            right--;
        }
    }

}
