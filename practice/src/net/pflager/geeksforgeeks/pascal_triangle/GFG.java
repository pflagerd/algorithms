package net.pflager.geeksforgeeks.pascal_triangle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// } Driver Code Ends
public class GFG
{
    public static long nchoosek(int n, int k) {
        if (n <= 1 || k == 0 || n == k) {
            return 1;
        }

        if (k > n - k) // take advantage of symmetry
            k = n - k;

        long c = 1;

        for (int i = 1; i <= k; i++, n--) {
            c = c / i * n + c % i * n / i;  // split c * n / i into (c / i * i + c % i) * n / i
        }

        return c;
    }

    // return a string containing the nth row of Pascal's triangle, numbering from 0. Row 0 is "1".
    public static String p(int n) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n + 1; i++) {
            if (i != 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(nchoosek(n, i));
        }
        return stringBuilder.toString();
    }

    public static void main (String[] args) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            for (int test = 0; test < numberOfTests; test++) {
                System.out.println(p(Integer.parseInt(br.readLine().trim())));
            }
        }
        catch (IOException e) {
            System.err.println(e);
        }
    }

}
