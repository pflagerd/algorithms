Each element of pascal's triangle is represented as $n \choose k$.

${n \choose k} = \frac{n!}{(n-k)!k!}$

e.g. for $n = 4, k = 3$:

${4 \choose 3} = \frac{4\cdot 3\cdot 2}{3\cdot 2\cdot 1}=4$



e.g. for n = 7, k = 4:

${7 \choose 4}=\frac{7\cdot 6\cdot 5\cdot 4}{4 \cdot 3 \cdot 2 \cdot 1}$



Each row of Pascal's triangle is a vector defined as follows:

$\vec{P}_n = \{{n\choose k}|_{k=0}^{n} \land n \in \mathbb{N} \}$



