#!/usr/bin/env rdmd

import std.conv;
import std.stdio;
import std.file;
import std.string;

void main(string[] args) {
    foreach (line; args) {
        writeln(line);
    }
}
