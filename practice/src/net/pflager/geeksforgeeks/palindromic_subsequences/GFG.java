package net.pflager.geeksforgeeks.palindromic_subsequences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GFG {
    private static boolean isPalindrome(String s) {
        for (int i = 0, j = s.length() - 1; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j))
                return false;
        }
        return true;
    }

    // Return number of sequence to remove to completely empty s
    // Return -1 if no such sequences could be found.
    private static int findAndRemovePalindromes(String s, int count) {
        int length = s.length();
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < length; i++) {
            for (int j = length; j > i + 1; j--) {
                if (isPalindrome(s.substring(i, j))) {
                    count++;
                    String newS = new StringBuilder(s).delete(i, j).toString();
                    if (newS.isEmpty())
                        return count;
                    count += findAndRemovePalindromes(newS, count);
                }
            }
        }
        return count < min ? count : min;
    }

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            for (int test = 0; test < numberOfTests; test++) {
                System.out.println(findAndRemovePalindromes(br.readLine().trim(), 0));
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}