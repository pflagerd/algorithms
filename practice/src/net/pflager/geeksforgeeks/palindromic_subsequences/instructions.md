Given a Binary String S (string that contains only 0's and 1's). Find minimum number of palindromic subsequences that are needed to be removed such that string S becomes empty.

INPUT
The first line of the input contains an integer T denoting the number of test cases. Then T test cases follow. Each test case consists of two lines. The first line of each test case consists of an integer N denoting the size of the string S. The second line of each test case consists of a binary string S.

OUTPUT
For each test case print in a new line the number of palindromic subsequences to be removed. 

EXAMPLE
INPUT
2
5
10001
8
10001001

OUTPUT
1
2

CONSTRAINTS
1<=T<=50
1<=N<=300