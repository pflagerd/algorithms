package net.pflager.geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class lucky_number {
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());
		    A:
		    for (int test = 0; test < numberOfTests; test++) {
		        String nAsString = br.readLine().trim();
		        int maxDigitProducts = 1 << nAsString.length();
		        long[] digitProducts = new long[maxDigitProducts];
		        int digitProductsLength = 0;
		        
		        // To find all combinations, use a binary number where each digit value of 1
		        // represents one of the digits from n, and count by ones.
		        for (long i = 0; i < maxDigitProducts; i++) {
		            // sweep through the current value of i, looking for 1 bits
		            // for each 1 bit found, include the digit from n.
		            StringBuilder stringBuilder = new StringBuilder();
		            long product = 1;
		            for (int j = 0; j < nAsString.length(); j++) {
		                if (((1 << j) & i) != 0) {
		                    stringBuilder.append(nAsString.charAt(j));
		                    product *= nAsString.charAt(j) - '0';
		                }
		            }
		            if (stringBuilder.length() == 0)
		                continue;
		            for (int j = 0; j < digitProductsLength; j++) {
		                if (digitProducts[j] == product) {
		                    // Not Lucky
		                    System.out.println(0);
		                    continue A;
		                }
		            }
		            digitProducts[digitProductsLength++] = product;
		        }
		        
		        System.out.println(1);
		    }
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}