package net.pflager.geeksforgeeks.print_all_possible_strings_1;

import java.util.ArrayList;
import java.util.Scanner;

public class GFG
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int t =  sc.nextInt();
        sc.nextLine();
        while(t>0)
        {
            String str = sc.nextLine();
            str = sc.nextLine();
            GFG g = new GFG();
            ArrayList<String> arr = g.spaceString(str);
            for(String s : arr){
                System.out.print(s+"$");
            }
            System.out.println();
            t--;
        }
        sc.close();
    }

    ArrayList<String> spaceString(String str)
    {
        ArrayList<String> al = new ArrayList<String>();

        // Length of string is <= 10
        // that means there are 9 possible places between characters in which to insert a space
        // assigning each of the 9 positions to a binary digit of an integer < 2^9, we can
        // insert a space at the corresponding string position (if digit is 1), or not insert
        // a space at the corresponding string position (if digit is 0).  This means there are a total
        // of 512 possibilites (2^9)

        // Let's start with a stringBuilder containing exactly s, which corresponds to NO
        // bits being set, which means no spaces inserted.
        // Space insertion will happen starting at index 1 (immediately to the right of index 0)
        for (int i = 0; i < (1 << (str.length() - 1)); i++) {  // let i iterate through the possible number of spaces, starting at 0
            int startingLength = str.length();
            StringBuilder stringBuilder = new StringBuilder(str);
            for (int j = 0, k = 1, l = startingLength - 1; j < startingLength - 1; j++, k = k << 1) {  // let j iterate through the indices of each spaces
                // let k be a bit mask containing a single 1 digit.
                // Let this bit mask be used to test for a corresponding
                // bit in i.  If the two bits are set, insert a space, else don't
                // Let l track the (moving) index of the next index at which
                // a space may be inserted. It starts at 1.
                if ((i & k) != 0) { // if kth bit of i is set
                    stringBuilder.insert(l, ' '); // insert a space
                }
                l--; // skip the next non-space so l is pointing at the next possible insertion point
            }
            //System.err.println(stringBuilder.toString());
            al.add(stringBuilder.toString());
        }
        return al;
    }
}

