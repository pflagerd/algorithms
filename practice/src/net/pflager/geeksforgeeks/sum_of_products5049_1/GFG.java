package net.pflager.geeksforgeeks.sum_of_products5049_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
 public static void main(String args[]) throws IOException {
     BufferedReader read =
         new BufferedReader(new InputStreamReader(System.in));
     int t = Integer.parseInt(read.readLine());
     while (t-- > 0) {
         int N = Integer.parseInt(read.readLine());
         long Arr[] = new long[N];
         String[] S = read.readLine().split(" ");
         for (int i = 0; i < N; i++) Arr[i] = Long.parseLong(S[i]);
         System.out.println(Solution.pairAndSum(N, Arr));
     }
 }
}
//} Driver Code Ends


//User function Template for Java
class Solution {
 static long pairAndSum(int N, long Arr[]) {
     long sum = 0;
     for (int i = 0; i < N - 1; i++) {
         for (int j = i + 1; j < N; j++) {
             sum += Arr[i] & Arr[j];
         }
     }
     return sum;
 }
}