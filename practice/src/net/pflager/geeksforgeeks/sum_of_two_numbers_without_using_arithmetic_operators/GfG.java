package net.pflager.geeksforgeeks.sum_of_two_numbers_without_using_arithmetic_operators;

// { Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GfG {
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while (t > 0) {
			String S[] = br.readLine().split(" ");

			int a = Integer.parseInt(S[0]);
			int b = Integer.parseInt(S[1]);
			Solution obj = new Solution();
			System.out.println(obj.sum(a, b));
			t--;
		}
	}
} // } Driver Code Ends

//User function Template for Java

class Solution {
	int sum(int a, int b) {
        int carry = (a & b) << 1; // 5 & 3 == 1, 1 << 1 == 2
        int sum = a ^ b; // 5 ^ 3 == 6
        for (int i = 1; i != (1 << 30) && carry != 0; i <<= 1) { // i == 4
            int nextsum = sum ^ carry; // 0 ^ 8 == 8
            carry = (sum & carry) << 1; // 0 & 8 == 0
            sum = nextsum; // 8
        }
        return sum;
	}
}