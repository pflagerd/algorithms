#!/usr/bin/env rdmd

import std.conv;
import std.stdio;
import std.file;
import std.string;

string testcase = "
	<testcase>
		<input>
			$input$
		</input>
		<expected_output>
			$output$
		</expected_output>
	</testcase>";

void main(string[] args) {
    foreach (line; readText(args[1]).splitLines()) {
        string[] a = line.split();
        writeln(testcase.replace("$input$", a[0]).replace("$output$", ""));
    }
}
