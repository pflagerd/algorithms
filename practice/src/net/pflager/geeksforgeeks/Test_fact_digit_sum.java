package net.pflager.geeksforgeeks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.output.TeeOutputStream;
import org.junit.jupiter.api.Test;

public class Test_fact_digit_sum extends fact_digit_sum {

    @Test
    public void case_002() throws IOException {
        try (PrintWriter processPrintWriter = new PrintWriter(new FileWriter("input.data"))) {
	        processPrintWriter.println(1);
	        //processPrintWriter.println("40321");
	        processPrintWriter.println("804288383");
//	        processPrintWriter.println("846930886");
        }

        PrintStream console = System.out;
        
        try (InputStream inputStream = new FileInputStream(new File("input.data"))) {
        	try (PrintStream printStream = new PrintStream(new TeeOutputStream(console, new FileOutputStream(new File("output.data"))))) {
	            System.setIn(inputStream);
	            System.setOut(printStream);
	            long startTime = System.currentTimeMillis();
	        	main(new String[0]);
	            long endTime = System.currentTimeMillis();
	            System.setOut(console);
	            System.out.println("Elapsed time == " + (endTime - startTime) / 1000.0 + " s");
        	}
        }
        
		try (BufferedReader br = new BufferedReader(new FileReader("output.data"))) {
			String s = br.readLine();
			System.out.println(s);
			assertEquals("18", s);
		}

		Files.delete(Paths.get("input.data"));
		Files.delete(Paths.get("output.data"));
		
    }
    
    
}