package net.pflager.geeksforgeeks.number_of_unique_rectangles1849_0;

//{ Driver Code Starts
//Initial Template for Java

//{ Driver Code Starts
//Initial Template for Java

//{ Driver Code Starts

//Initial Template for Java

//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main(String args[]) throws IOException {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(read.readLine());
		while (t-- > 0) {
			int N = Integer.parseInt(read.readLine());

			Solution ob = new Solution();
			System.out.println(ob.noOfUniqueRectangles(N));
		}
	}
}
//} Driver Code Ends

//User function Template for Java

class Solution {
	public static void main(String args[]) {
		for (int i = 1; i < 1000; i++) {
			System.out.println(i + " " + new Solution().noOfUniqueRectangles(i));
		}
	}
	
	int noOfUniqueRectangles(int N) {
		int sum = 0;
		for (int i = 1; i * i <= N; i++)
			sum += N / i - (i - 1);
		return sum;
	}
};