package net.pflager.geeksforgeeks.sum_of_gcds1050_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.IOException;
import java.util.Scanner;

class GFG {
	public static void main(String args[]) throws IOException {
		try (Scanner sc = new Scanner(System.in)) {
			int t = sc.nextInt();
			while (t-- > 0) {
				int n = sc.nextInt();

				System.out.println(Solution.sumOfGCDofPairs(n));
			}
		}
	}
}
//} Driver Code Ends

//User function Template for Java

class Solution {

	static int gcd(int a, int d) {
		int r;
		while ((r = a % d) != 0) {
			a = d; d = r;
		}
		return d;
	}
	
	static long sumOfGCDofPairs(int N) {

		long sum = 0;
		for (int i = 1; i <= N; i++) {
			sum += gcd(i, N);
		}

		return sum;
	}
}