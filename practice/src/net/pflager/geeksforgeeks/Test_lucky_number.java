package net.pflager.geeksforgeeks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.output.TeeOutputStream;
import org.junit.jupiter.api.Test;


public class Test_lucky_number extends lucky_number {

    @Test
    public void case_002() throws IOException {
        PrintStream console = System.out;
        
        try (PrintWriter printWriter = new PrintWriter(new TeeOutputStream(console, new FileOutputStream("input.data")))) {
        	String testData = 
	        	"2\n" +
	        	"1270\n" +
	        	"97825349";
	        printWriter.println(testData);
        }

        try (InputStream inputStream = new FileInputStream(new File("input.data"))) {
        	try (PrintStream printStream = new PrintStream(new TeeOutputStream(console, new FileOutputStream(new File("output.data"))))) {
	            System.setIn(inputStream);
	            System.setOut(printStream);
	            long startTime = System.currentTimeMillis();
	        	main(new String[0]);
	            long endTime = System.currentTimeMillis();
	            System.setOut(console);
	            System.out.println("Elapsed time == " + (endTime - startTime) / 1000.0 + " s");
        	}
        }
        
		try (BufferedReader br = new BufferedReader(new FileReader("output.data"))) {
			String s = br.readLine();
			System.out.println(s);
			assertEquals("0", s);
			s = br.readLine();
			System.out.println(s);
			assertEquals("0", s);
		}

		Files.delete(Paths.get("input.data"));
		Files.delete(Paths.get("output.data"));
    }
}