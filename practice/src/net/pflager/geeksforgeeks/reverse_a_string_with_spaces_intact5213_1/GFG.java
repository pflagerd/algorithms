package net.pflager.geeksforgeeks.reverse_a_string_with_spaces_intact5213_1;

//} Driver Code Ends

//User function Template for Java

//Initial Template for Java

//{ Driver Code Starts
//Initial Template for Java

//{ Driver Code Starts

//Initial Template for Java

//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

	class GFG
	{
	    public static void main(String args[])throws IOException
	    {
	        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        int t = Integer.parseInt(br.readLine());
	        while(t-- > 0)
	        {
	            String s;
	            s = br.readLine();
	            
	            Solution ob = new Solution();
	            
	            System.out.println(ob.reverseWithSpacesIntact(s));    
	        }
	    }
	}
	// } Driver Code Ends




	//User function Template for Java
	class Solution
	{
	    static final char SPACE = ' ';
	    
	    String reverseWithSpacesIntact(String S)
	    {	        
	    	
	    	S = "  fny";
	    	
	        StringBuilder sb = new StringBuilder();
	        ArrayList<Integer> spacePositions = new ArrayList<>();

	        for (int i = 0; i < S.length(); i++) {
	            if (S.charAt(i) == SPACE) {
	                //System.out.println("i = " + i);
	                spacePositions.add(i);
	            }
	        }
	            
	        int nextLetter = S.length() - 1;
	        int lastSpace = 0;
	        for (int k = 0; k < spacePositions.size(); k++) {
	            int currentSpace = spacePositions.get(k);
	            int span = currentSpace - lastSpace;
	            lastSpace = currentSpace;

	            for (int i = 0; i < span; i++) {
	                char ch = S.charAt( nextLetter-- );
	                if (ch != SPACE) {
	                    sb.append( ch );
	                    //System.out.println( " -" + ch + "- ");
	                } 
	            }
	            sb.append( SPACE );
	            //System.out.println( " - - ");
	            
	        }
	        
	        while (nextLetter >= 0) {
	            char ch = S.charAt( nextLetter-- );
	            if (ch != SPACE) {
	                sb.append( ch );
	                //System.out.println( " -" + ch + "- ");
	            }
	        }
	        
	        return sb.toString();
	    }
	}