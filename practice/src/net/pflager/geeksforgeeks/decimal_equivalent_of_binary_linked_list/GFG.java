package net.pflager.geeksforgeeks.decimal_equivalent_of_binary_linked_list;

//{ Driver Code Starts
import java.util.Scanner;

class Node {
	int data;
	Node next;

	Node(int d) {
		data = d;
		next = null;
	}
}

class LinkedList1 {
	Node head; // head of lisl
	/* Inserts a new Node at front of the list. */

	public void addToTheLast(Node node) {
		if (head == null) {
			head = node;
		} else {
			Node temp = head;
			while (temp.next != null)
				temp = temp.next;

			temp.next = node;
		}
	}

	/* Function to print linked list */
	void printList() {
		Node temp = head;
		while (temp != null) {
			System.out.print(temp.data + " ");
			temp = temp.next;
		}
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		sc.nextLine();
		while (t > 0) {
			int n = sc.nextInt();
			sc.nextLine();
			LinkedList1 llist = new LinkedList1();

			if (n > 0) {
				int a1 = sc.nextInt();
				Node head = new Node(a1);
				llist.addToTheLast(head);
			}
			for (int i = 1; i < n; i++) {
				int a = sc.nextInt();
				llist.addToTheLast(new Node(a));
			}
			System.out.println(new GFG().DecimalValue(llist.head));
			t--;
		}
		sc.close();
	}
}

//} Driver Code Ends
/*
 * Node of a linked list class Node { int data; Node next; Node(int d) { data =
 * d; next = null; } } Linked List class class LinkedList { Node head; // head
 * of list } This is method only submission. You only need to complete the
 * method.
 */

public class GFG {
	long DecimalValue(Node head) {
		Node hn = head.next;
		Node hnn = head.next.next;
		hn.next = head;
		head.next = null;

		while (hnn != null) {
			head = hn;
			hn = hnn;
			hnn = hnn.next;

			hn.next = head;
		}
		
		head = hn;

		java.math.BigInteger power = java.math.BigInteger.ONE;
		java.math.BigInteger accumulator = java.math.BigInteger.ZERO;
		do {
			accumulator = accumulator.add(java.math.BigInteger.valueOf(head.data).multiply(power));
			power = power.multiply(new java.math.BigInteger("2"));
			head = head.next;
		} while (head != null);

		return accumulator.mod(new java.math.BigInteger("1000000007")).longValue();
	}
}