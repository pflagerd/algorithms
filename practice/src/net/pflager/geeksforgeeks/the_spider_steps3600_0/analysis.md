$$\left[f(n) = \begin{cases}U&\text{if n = 1}\\U+(n-1)(U-D)&\text{if n > 1}\end{cases}\right] \land (U \in \mathbb{N}) \land (D \in \mathbb{N}) \land (U >= D) \land (n \in \mathbb{N}) \land (f:\mathbb{N} \to \mathbb{N})$$

$H \lt f(n)$

$$\text{Find minimum } n$$

Now, we don't know for sure if the equation above is correct, so let's try $U = 10$, $D = 0$ and $H = 10$

If $n$ is 1, then the equation above is correct.

![image-20221109130524686](.md/analysis/image-20221109130524686.png)

Well it sure isn't 1.  $n = 2$.  So that means that

$H \lt f(n)$

$$\text{Find minimum } n$$



$$\left[H \lt \begin{cases}U&\text{if n = 1}\\U+(n-1)(U-D)&\text{if n > 1}\end{cases}\right] \land (U \in \mathbb{N}) \land (D \in \mathbb{N}) \land (U >= D) \land (n \in \mathbb{N})$$



$$\left[H \lt U + (n-1)(U-D)\right] \land (U \in \mathbb{N}) \land (D \in \mathbb{N}) \land (U >= D) \land (n \in \mathbb{N})$$



$$H \lt U + (n-1)(U-D)$$



$$H \lt U + nU - nD - U + D$$



$$H \lt nU - nD + D$$



$$H \lt n(U - D) + D$$

$$H - D\lt n(U - D)$$



$$a, b, c \in \mathbb{N}$$

$$a=bc$$







$$(H - D)//(U - D)\lt n$$

$$(H - D)\%(U - D) = r$$







m = nq + r

m - r = nq

(m - r) / n = q

(m - r) // n = q



m % n = r