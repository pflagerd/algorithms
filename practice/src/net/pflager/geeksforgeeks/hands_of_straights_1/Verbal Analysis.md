Alice has some cards, each card has one number written on it. She wants to rearrange the cards into groups so that each group is of size **groupSize**, and consists of **groupSize** consecutive cards. Given an integer array **hand** of size **N** where **hand [ i ]** is the value written on the **ith** card and an integer **groupSize**, return **true** if she can rearrange the cards, or **false** otherwise.



<u>Underline</u> here means that it has been drawn in excalidraw.



<u>Daniel has some cards, each card has one number written on it.</u> She wants to rearrange the cards into groups so that each group is of size **groupSize**, and consists of **groupSize** consecutive cards. Given an integer array **hand** of size **N** where **hand [ i ]** is the value written on the **ith** card and an integer **groupSize**, return **true** if she can rearrange the cards, or **false** otherwise.



David observes there is a pronoun cognitive dissonance.



<u>Daniel has some cards, each card has one number written on it.</u> Daniel wants to rearrange the cards into groups so that each group is of size **groupSize**, and consists of **groupSize** consecutive cards. Given an integer array **hand** of size **N** where **hand [ i ]** is the value written on the **ith** card and an integer **groupSize**, return **true** if Daniel can rearrange the cards, or **false** otherwise.



I use Excalidraw to draw a representation of cards, each with the number 1 on it.

I write down all the nouns from above in a list