package net.pflager.geeksforgeeks.euler_totient_sum_and_divisors5501_1;

//{ Driver Code Starts
//Initial Template for Java


import java.io.*;
import java.util.*;

class GFG
{
    public static void main(String args[])throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while(t-- > 0)
        {
            int N = Integer.parseInt(read.readLine());
            Solution ob = new Solution();
            System.out.println(ob.phiSum(N));
            System.err.println(ob.phiSum(N));
        }
    }
}
// } Driver Code Ends


//User function Template for Java
class Solution{
    static int[] phi = new int[1000001];
    
    static int phi(int n) {
        if (phi[n] != 0)
            return phi[n];
        
        int count = 0;
        for (int i = 1; i <= n; i++) {
            if (gcd(n, i) == 1)
                count++;
        }
        phi[n] = count;
        return count;
    }
    
    static int gcd(int a, int d) {
        if (d == 0)
            return a;
        return gcd(d, a % d);
    }

    static int[] phiSum =  new int[1000001];

    static int phiSum(int N) {
        if (phiSum[N] != 0)
            return phiSum[N];
        
        int sum = 0;
        for (int i = 1; i <= N; i++) {
            if (N % i == 0)
                sum += phi(i);
        }
        phiSum[N] = sum;
        return sum;
    }
}