package net.pflager.geeksforgeeks.equilibrium_point;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

public class Test_equilibrium_point extends GFG {
	
	class ConsoleAndFileOutputStream extends OutputStream {
		private OutputStream console = System.out;
		private OutputStream fileOutputStream;
		
		public ConsoleAndFileOutputStream(String fileName) throws FileNotFoundException {
			fileOutputStream = new FileOutputStream(new File(fileName));
		}
		
		@Override
		public void close() throws IOException {
			fileOutputStream.close();
		}

		@Override
		public void write(int arg0) throws IOException {
			console.write(arg0);
			fileOutputStream.write(arg0);
		}
	}

    @Test
    public void case_001() throws IOException {
//        try (PrintWriter processPrintWriter = new PrintWriter(new ConsoleAndFileOutputStream("input.data"))) {
//        	processPrintWriter.println("4");
//	        processPrintWriter.println("5");
//	        processPrintWriter.println("1 3 5 2 2");
//        	processPrintWriter.println("17");
//        	processPrintWriter.println("27 4 25 6 6 1 27 22 19 29 6 9 36 24 6 15 5");
//	        processPrintWriter.println("8");
//	        processPrintWriter.println("43 34 2 8 17 5 11 8");
//        	processPrintWriter.println(
//	        	"19\n" +
//	        	"32 41 34 26 34 30 10 11 23 20 10 12 25 5 7 41 7 43 25"
//        	);
//        }

        PrintStream console = System.out;
        try (InputStream inputStream = new FileInputStream(new File("test/net/pflager/geeksforgeeks/equilibrium_point/input.data"))) {
        	try (PrintStream printStream = new PrintStream(new ConsoleAndFileOutputStream("output.data"))) {
	            System.setIn(inputStream);
	            System.setOut(printStream);
	            long startTime = System.currentTimeMillis();
	        	main(new String[0]);
	            long endTime = System.currentTimeMillis();
	            System.setOut(console);
	            System.out.println("Elapsed time == " + (endTime - startTime) / 1000.0 + " s");
        	}
        }

        int i = 0;
        String[] answers = { "2", "-1", "-1", "-1" };
		try (BufferedReader br = new BufferedReader(new FileReader("output.data"))) {
			for (String s = br.readLine(); s != null; i++, s = br.readLine()) { 
				assertEquals(answers[i], s);
			}
		}

		Files.delete(Paths.get("input.data"));
		Files.delete(Paths.get("output.data"));
		
    }
    
    
}