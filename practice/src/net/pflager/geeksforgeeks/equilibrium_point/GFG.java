package net.pflager.geeksforgeeks.equilibrium_point;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());

		    for (int test = 0; test < numberOfTests; test++) {
		        int n = Integer.parseInt(br.readLine().trim());
		        String[] arrayAsStrings = br.readLine().trim().split("\\s+");
		        int[] array = new int[n];
		        int runningSum = 0;
		        int runningHalfSum = 0;
		        int j = 0; // halfSum point
		        for (int i = 0; i < n; i++) {
		            array[i] = Integer.parseInt(arrayAsStrings[i]);
		            runningSum += array[i];
		            int halfSum = runningSum >> 1;
		            if (halfSum > runningHalfSum) {
                        if (j <= i) { // this might want to be a while loop.
                            if (runningHalfSum + array[j] <= halfSum) {
                                runningHalfSum += array[j++];
                            }
                        }
		            }
		        }
		        if ((runningSum - 2 * runningHalfSum - array[j]) == 0)
		        	System.out.println(j);
		        else
		        	System.out.println(-1);
		    }
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}