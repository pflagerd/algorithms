package net.pflager.geeksforgeeks.number_of_compositions_of_a_natural_number4627_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
class GFG
{
  public static void main(String[] args) throws IOException
  {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      int T = Integer.parseInt(br.readLine().trim());
      while(T-->0)
      {
          int n = Integer.parseInt(br.readLine().trim());
          Solution ob = new Solution();
          int ans = ob.no_of_ways(n);
          System.out.println(ans);
      }
  }
}


//} Driver Code Ends


//User function Template for Java

class Solution
{
  //https://en.wikipedia.org/wiki/Modular_exponentiation#:~:text=Modular%20exponentiation%20is%20the%20remainder,that%200%20%E2%89%A4%20c%20%3C%20m.
  // See lua example. Exponentiation by squaring.
  static int modPowA(int base, int exponent, int modulo) {
    int result = 1;
    base = base % modulo;
    while (exponent > 0) {
      if (exponent % 2 == 1) {
        result = (int)(((long)result * base) % modulo);
      }
      base = (int)(((long)base * base) % modulo);
      exponent >>>= 1;
    }
    return result;
  }
  
  static int modPowB(int base, int exponent, int modulo) {
    int result = 1;
    base = base % modulo;
    int mask = Integer.highestOneBit(exponent);
    while (mask > 0) {
      result = (int)(((long)result * result) % modulo);
      if ((mask & exponent) != 0) {
        result = (int)(((long)result * base) % modulo);
      }
      mask >>>= 1;
    }
    return result;
  }
  
	static final int maxchunksize = 62; // 1L << 62 == 2^63
	
  // $2^{n-1}$
  // $n^29 is just less than 1000000007
  public int no_of_ways(int n) {
    //System.err.println(n);
//      long acc = 1;
//      for (int i = 1; i < n; i++) {
//          acc = (acc << 1) % 1000000007;
//          System.err.println(acc);
//      }
//      return (int)acc;

    
//      long acc = 1;
//      for (int i = n - 2; i >= 0; i--) {
//	      acc = (acc << 1) % 1000000007;
//	      System.err.println(acc);
//	  	}
//      return (int)acc;

    
//      long acc = 1;
//      for (int i = n - 1; i > 0; i--) {
//      	if (i > 29) {
//      		acc = (acc * (1L << 29)) % 1000000007;
//      		i -= 28;
//      	}
//      	else
//      		acc = (acc << 1) % 1000000007;
//	      System.err.println(acc);
//	  	}
//      return (int)acc;

      // start with maxchunksize, and then apply it repeatedly until less than maxchunksize remains
      // then apply the next lowest chunksize
      // works, but not fast enough.
//      long acc = 1;
//      for (int i = n - 1, chunksize = i <= maxchunksize ? i : maxchunksize;
//           i > 0; 
//           i -= chunksize, chunksize = i <= maxchunksize ? i : maxchunksize) {
//      	acc = acc * ((1L << chunksize) % 1000000007L) % 1000000007L;
//        //System.err.println(acc);
//      }
//      return (int)acc;

//    // https://en.wikipedia.org/wiki/Exponentiation_by_squaring    
//    if (--n == 0) 
//        return 1;
//
//    int x = 2;
//    int y = 1;
//    while (n > 1) {
//        if ((n & 1) == 0) {
//            n = n / 2;
//        } else {
//            y = (int)(((long)x * y) % 1000000007);
//            n = (n - 1) / 2;
//        }
//        x = (int)(((long)x * x) % 1000000007);
//    }
//    return (int)(((long)x * y) % 1000000007);

      return modPowB(2, n - 1, 1000000007);
    
    
  }
}
