#include <stdio.h>

void main() {
    long n = 9_199_999_999_999_999_999L;
    // This will fail: n = 9_299_999_999_999_999_999
    printf("%ld\n", n);
}
