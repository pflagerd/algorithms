package net.pflager.geeksforgeeks.count_digits_in_given_number_n_which_divide_n3331_1;
//{ Driver Code Starts
// Initial Template for Java

import java.io.*;
import java.util.*;

class GFG {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
                new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while (t-- > 0) {
            String S = read.readLine();
            Solution ob = new Solution();
            System.out.println(ob.divisibleByDigits(S));
        }
    }
}
// } Driver Code Ends


// User function Template for Java

class Solution {
    static final int length = 9;

    static int divisibleByDigits(String S) {
        //System.err.println(S);

        long[] accumulators = new long[8];  // 2 - 9
        int[] isDigitPresent = new int[10]; // 0 - 9
        int start = 0;
        int sLength = S.length();

        while (sLength != 0) {
            int chunkLength = Math.min(length, sLength);
            String s = S.substring(start, start + chunkLength);
            long longChunk = Long.parseLong(s);

            for (int i = 0; i < chunkLength; i++) {
                isDigitPresent[s.charAt(i) - '0']++;
            }

            long base = 1L;
            for (int i = 0; i < chunkLength; i++) {
                base *= 10;
            }

            for (int i = 2; i <= 9; i++) {
                accumulators[i - 2] = (longChunk + base * accumulators[i - 2]) % (long)i;
            }

            start += chunkLength;
            sLength -= chunkLength;
        }

        int count = 0;
        for (int i = 1; i <= 9; i++) {
            if (isDigitPresent[i] != 0 && (i == 1 || accumulators[i - 2] == 0))
                count += isDigitPresent[i];
        }

        //System.err.println(Arrays.toString(accumulators));
        //System.err.println(Arrays.toString(isDigitPresent));
        return count;
    }
}