package net.pflager.geeksforgeeks.smallest_subarray_with_sum_greater_than_x5651_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//{ Driver Code Starts
//Initial Template for Java
import java.util.StringTokenizer;

class GFG {
  public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int t = Integer.parseInt(br.readLine().trim());
    while (t-- > 0) {
      StringTokenizer stt = new StringTokenizer(br.readLine());

      int n = Integer.parseInt(stt.nextToken());
      int m = Integer.parseInt(stt.nextToken());
      // int n = Integer.parseInt(br.readLine().trim());
      int a[] = new int[n];
      String inputLine[] = br.readLine().trim().split(" ");
      for (int i = 0; i < n; i++) {
        a[i] = Integer.parseInt(inputLine[i]);
      }

      System.out.println(Solution.smallestSubWithSum(a, n, m));
    }
  }
}

//} Driver Code Ends

//User function Template for Java

class Solution {

  public static int smallestSubWithSum(int a[], int n, int x) {
    if (a == null || a.length == 0 || n == 0)
      throw new IllegalArgumentException();

    int hi = 0;
    int lo = 0;
    int minLength = -1;
    int sum = 0;
    while (true) {
      if (sum <= x) {
        if (hi >= a.length)
          return minLength;

        sum += a[hi++];
        continue;
      }

      if (minLength == -1 || hi - lo < minLength)
        minLength = hi - lo;

      sum -= a[lo++];
    }
  }
}
