package net.pflager.geeksforgeeks._0_1_knapsack_problem0945_1;

//{ Driver Code Starts
import java.util.*;
import java.io.*;
import java.lang.*;

class GFG {
	public static void main(String args[]) throws IOException {
		// reading input using BufferedReader class
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));

		// reading total testcases
		int t = Integer.parseInt(read.readLine());

		while (t-- > 0) {
			// reading number of elements and weight
			int n = Integer.parseInt(read.readLine());
			int w = Integer.parseInt(read.readLine());

			int val[] = new int[n];
			int wt[] = new int[n];

			String st[] = read.readLine().trim().split("\\s+");

			// inserting the values
			for (int i = 0; i < n; i++)
				val[i] = Integer.parseInt(st[i]);

			String s[] = read.readLine().trim().split("\\s+");

			// inserting the weigths
			for (int i = 0; i < n; i++)
				wt[i] = Integer.parseInt(s[i]);

			// calling method knapSack() of class Knapsack
			System.out.println(new Solution().knapSack(w, wt, val, n));
		}
	}
}

//} Driver Code Ends

class Solution {
	static class Pair {
		public int w;
		public int v;

		public Pair(int w, int v) {
			this.w = w;
			this.v = v;
		}

		public String toString() {
			return "(" + w + ", " + v + ")";
		}
	}

	// Function to return max value that can be put in knapsack of capacity W.
	static int knapSack(int W, int wt[], int val[], int n) {

		// System.err.println(W + " " + Arrays.toString(wt) + " " +
		// Arrays.toString(val));
		ArrayList<Pair> arrayList = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			arrayList.add(new Pair(wt[i], val[i]));
		}

		arrayList.sort((Pair a, Pair b) -> a.w - b.w);
		// System.err.println(arrayList);

		int maxValueSum = -1;
		int weightSum = 0;
		int valueSum = 0;
		for (int head = 0, tail = 0;;) {
			Pair pair;
			if (weightSum <= W) {
				if (valueSum > maxValueSum) {
					maxValueSum = valueSum;
				}
				if (head >= n)
					break;
				pair = arrayList.get(head++);
				weightSum += pair.w;
				valueSum += pair.v;
				continue;
			}
			if (tail > head)
				break;
			pair = arrayList.get(tail++);
			weightSum -= pair.w;
			valueSum -= pair.v;
		}

		return maxValueSum;
	}
}
