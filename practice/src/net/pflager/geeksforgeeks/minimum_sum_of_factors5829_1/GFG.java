package net.pflager.geeksforgeeks.minimum_sum_of_factors5829_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main(String args[]) throws IOException {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(read.readLine());
		while (t-- > 0) {
			int N = Integer.parseInt(read.readLine());
			Solution ob = new Solution();
			System.out.println(ob.sumOfFactors(N));
		}
	}
}
//} Driver Code Ends

//User function Template for Java

class Solution {
	int sumOfDigits(int i) {
		int sum = 0;
		
		for (; i != 0; i /= 10) {
			sum += i % 10;
		}
		
		return sum;
	}
	
	int sumOfFactors(int N) {
		
		int n = N;
		for (; ((n % 10) == 9) || (n == N && n % 10 == 8) ; n /= 10);
		
		if (n / 10 != 0) {			
			// count down to next greater sumOfDigits
			int m = N - 1;
			for (int sumOfDigitsN = sumOfDigits(N); sumOfDigits(m) <= sumOfDigitsN; m--);
			return m;
		}		
				
		return N;
		//exploration1();
	}
	
	void exploration1() {
		for (int i = 1; i < 2000; i++) {
			System.err.printf("% 4d % 4d\n", i, sumOfDigits(i));
		}
	}
};