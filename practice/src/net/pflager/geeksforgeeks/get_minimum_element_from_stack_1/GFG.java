package net.pflager.geeksforgeeks.get_minimum_element_from_stack_1;

//{ Driver Code Starts
import java.util.*;



class GFG
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		while(T>0)
		{
			int q = sc.nextInt();
			GfG g = new GfG();
			while(q>0)
			{
				int qt = sc.nextInt();
				
				if(qt == 1)
				{
					int att = sc.nextInt();
					g.push(att);
					//System.out.println(att);
				}
				else if(qt == 2)
				{
					System.out.print(g.pop()+" ");
				}
				else if(qt == 3)
				{
					System.out.print(g.getMin()+" ");
				}
			
			q--;
			}
			System.out.println();
		T--;
		}
		
	}
}


// } Driver Code Ends
class GfG
{
    boolean debug = false;
    int minEle;
    Stack<Integer> s = new Stack<>();

    /*returns min element from stack*/
    int getMin() {
        if (s.size() == 0) {
            if (debug) System.err.println(-1);
            return -1;
        }
        
        minEle = s.peek();
        if (debug) System.err.println("getMin(): " + minEle + " " + s);
        return minEle;
    }
    
    /*returns popped element from stack*/
    int pop() {
        if (s.size() == 0) {
            if (debug) System.err.println(-1);
            return -1;
        }
        
        s.pop();
        int thing = s.pop();
        if (s.size() == 0)
            minEle = 0;
        else
        		minEle = s.peek();
        if (debug) System.err.println("pop(): " + thing + " " + minEle + " " + s);
        return thing;
    }

    /*push element x into the stack*/
    void push(int x) {
        if (minEle == 0)
            minEle = x;
        else if (x < minEle)
            minEle = x;
        s.push(x);
        s.push(minEle);
        if (debug) System.err.println("push(): " + minEle + " " + s);
    }	
}

