

```
```//User function Template for Java
class Solution {
    static ArrayList<Integer> factorize(int n) {
        ArrayList<Integer> answer = new ArrayList<>();
        for (int i = 1; i * i <= n; i++) {
            if (n % i == 0) {
                answer.add(i);
                if (i != n / i)
                    answer.add(n / i);
            }
        }
        return answer;
    }

    static long maxSumLCM(int n) {
        ArrayList<Integer> factors = factorize(n);
        // System.err.println(factors.toString());
        
        long sum = 0;
        for (int i = 0; i < factors.size(); i++) {
            sum += factors.get(i);
        }
        return sum;
    }
}```
```

I made the following mistakes. Firstly, I put the summing code inside factorize().  This led me to write 

for (int i = 0; i <=n; i++) {

}

I also wrote the sum as 

sum += factors.get(0);



The first submission failed due to having this spelling:  ArrayList\<Integers\> factors = factorize();

I corrected it to ArrayList\<Integer\> factors = factorize();  // This was a unconscious verbal substitution when  in my brain I wrote the words "ArrayList of Integer**s**" as proper English, instead of "ArrayList of Integer" Pidgin English.

I found the factors.get(0) error first.  // I don't know why I put a zero there, instead of an factors.get(i).

Then upon submitting, I experienced an out-of-bounds error.

I then used a System.err.println(factors.toString()); to confirm that factorize() was producing the correct results.  It was. I unintentionally bisected the code and showed that the first "half" (factorize()) was working.

This led us to find that I should use factors.size() in the for() loop rather than n.  I hastily used n instead of factors.size() when writing the first iteration of that for() loop.  Maybe too many things to remember and I oversimplified to make things fit.  Maybe it's also a mistake in switching between "abstract mode" and "concrete java mode".

But I didn't notice that I needed to correct <= to be < until I submitted and experienced another out-of-bounds error.  I hastily used <= which is not my usual habit with for loops.

Having corrected all these, the solution passed all tests in 0.13 seconds.

It took 8m 47s elapsed time to achieve this result.

