package net.pflager.geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class fact_digit_sum {
    static String increment(String s) {
    	StringBuilder stringBuilder = new StringBuilder(s);
    	boolean carry = true;
    	for (int i = stringBuilder.length() - 1; i >= 0 && carry; i--) {
    		char theChar = stringBuilder.charAt(i);
    		if (theChar < '9') {
    			stringBuilder.setCharAt(i, ++theChar);
    			carry = false;
    			break;
    		}
    		
    		carry = true;
    		stringBuilder.setCharAt(i, '0');
    	}
    	if (carry) {
    		stringBuilder.insert(0, '1');
    	}
    	
    	return stringBuilder.toString();
    }

    // 12! is the biggest that will fit in 1,000,000
    static int[] factorials = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600};
    
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());
		    A:
		    for (int test = 0; test < numberOfTests; test++) {
		        int n = Integer.parseInt(br.readLine().trim());
		        String integer = "1";
		        int integerLength = 1;
		        while(integer.length() < 1_000_000) {
		        	if (integerLength != integer.length()) {
			        	System.out.println(integer.length());
			        	integerLength = integer.length();
		        	}
		            long sum = 0;
		            for (int j = 0; j < integer.length(); j++) {
		                sum += factorials[integer.charAt(j) - '0'];
		                if (sum > n)  {
		                	break;
		                }
		            }
		            if (sum == n) {
		                System.out.println(integer);
		                continue A;
		            }
		            integer = increment(integer);
		        }
		        System.out.println(-1);
		    }
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}