package net.pflager.geeksforgeeks.divide_the_number5320_1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

public class NumberOfWaysToSum4DigitsToN {
	public static int f4(int N) {
		int count = 0;
		for (int a = 1; a <= N / 4; a++) {
			count += NumberOfWaysToSum3DigitsToN.f3(N - a) - 2 * (a - 1);
		}
		return count;
	}
	
	public static int algorithm_1(int N) {
		int count = 0;
		for (int a = 1, b = 1, c = 1, d = N - 3; a <= N / 4; a++, b = a, c = a, d = N - a - b - c) {
			for (; b <= (N - a) / 3; b++, c = b, d = N - a - b - c) {
				for (; c <= d; c++, d--) {
					count++;
					System.err.printf("% 5d: %d + %d + %d + %d = %d\n", count, a, b, c, d, N);
				}
			}
		}
		return count;
	}
 
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertEquals(3, algorithm_1(7));
		testName = "assertEquals(3, algorithm_1(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(5, algorithm_1(8));
		testName = "assertEquals(5, algorithm_1(8))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);		

		exec = () -> assertEquals(6936, algorithm_1(99));
		testName = "assertEquals(6936, algorithm_1(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(7153, algorithm_1(100));
		testName = "assertEquals(7153, algorithm_1(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(3, f4(7));
		testName = "assertEquals(3, f4(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(5, f4(8));
		testName = "assertEquals(5, f4(8))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);		

		exec = () -> assertEquals(6936, f4(99));
		testName = "assertEquals(6936, f4(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(7153, f4(100));
		testName = "assertEquals(7153, f4(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
