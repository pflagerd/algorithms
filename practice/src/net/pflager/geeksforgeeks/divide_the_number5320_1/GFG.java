package net.pflager.geeksforgeeks.divide_the_number5320_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main(String args[]) throws IOException {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(read.readLine());
		while (t-- > 0) {
			int N = Integer.parseInt(read.readLine());
			System.out.println(Solution.countWays(N));
		}
	}
}
// } Driver Code Ends

// User function Template for Java

class Solution {
    /* Simplest */
    static long countWays_1(int N) {
        long count = 0;
        for (int a = 1; a <= N / 4; a++) {
            for (int b = a; b <= N / 3; b++) {
                for (int c = b, d = N - a - b - c; c <= d; c++, d--) {
                    count++;
                    System.err.println(a + "+" + b + "+" + c + "+" + d + "=" + (a+b+c+d) + " expected " + N);
                }
            }
        }
        return count;
    }

    static long countWays(int N) {
        long count = 0;
        for (int a = 1; a <= N / 4; a++) {
            for (int b = a; b <= N / 3; b++) {
                count += (N - a - b - 2 * (b - 1)) / 2;
                System.err.println(a + "+" + b + " " + count);
            }
        }
        return count;
    }
}
