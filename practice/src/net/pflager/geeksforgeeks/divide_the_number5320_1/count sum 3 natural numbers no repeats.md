How many ways can the natural number $n$​​​ be represented as the sum of $3$​ natural numbers, allowing no repeats of the sets {x,y,z}? e.g. only one of {1, 1, 2} and {1, 2, 1} is allowed.

This implies $n \ge 3$​​​.

One way to achieve the no-repeats constraint is to require that $x \le y \le z.$

Lets define $\mathbb{N}^n_2$ to mean the number of ways two natural numbers may sum to n when no repeats of the set of two numbers is allowed.

| x+y+z=3 | Count | y+z                                 |
| ------- | ----- | ----------------------------------- |
| 1+1+1   | 1     | $\mathbb{N}^2_2=\mathbb{N}^{n-1}_2$ |



| x+y+z=4 | Count | y+z                                 |
| ------- | ----- | ----------------------------------- |
| 1+1+2   | 1     | $\mathbb{N}^3_2=\mathbb{N}^{n-1}_2$ |



| x+y+z=5 | Count | y+z                                 |
| ------- | ----- | ----------------------------------- |
| 1+1+3   | 1     |                                     |
| 1+2+2   | 2     | $\mathbb{N}^4_2=\mathbb{N}^{n-1}_2$ |



| x+y+z=6 | Count | y+z                                     |
| ------- | ----- | --------------------------------------- |
| 1+1+4   | 1     |                                         |
| 1+2+3   | 2     | $\mathbb{N}^5_2=\mathbb{N}^{n-1}_2$     |
| 2+2+2   | 3     | $\mathbb{N}^4_2-1=\mathbb{N}^{n-2}_2-1$ |

In x+y+z=6, the first two sums y+z behave like $\mathbb{N}^5_2$ because $x = 1$.  In the third sum y+z behaves like $\mathbb{N}^4_2$ except that (y, z) = (1, 3) is dropped (this is what the -1 means), because it would result in a duplicate set {2, 1, 3} = {1, 2, 3}. Or thinking in terms of our constraint  $x \le y \le z$, where $x = 2$,  (y, z) = (1, 3) is dropped because it would violate our constraint thus: $x \nleq y \equiv 2 \nleq 1$. 

If we drop a sum like this, isn't that equivalent to subtracting 2 from n, thus: $\mathbb{N}^{n-3}_2 = \mathbb{N}^{6-3}_2 = \mathbb{N}^3_2 = 1$

So in effect, 2+2+2 is $\mathbb{N}^{n-1}_2 + \mathbb{N}^{n-3}_2$

As in $\mathbb{N}^3_2 + \mathbb{N}^5_2$

Is it too soon to hypothesize that the general formula might be $\sum\limits_{i=1}^{n \over 2}\mathbb{N}_2^{1+2i}$ ?



| x+y+z=7 | Count | y+z                                     |
| ------- | ----- | --------------------------------------- |
| 1+1+5   | 1     |                                         |
| 1+2+4   | 2     |                                         |
| 1+3+3   | 3     | $\mathbb{N^6_2}=\mathbb{N^{(n-1)}_2}$   |
| 2+2+3   | 4     | $\mathbb{N}^4_2-1=\mathbb{N}^{n-2}_2-1$ |



| x+y+z=8 | Count | y+z                                     |
| ------- | ----- | --------------------------------------- |
| 1+1+6   | 1     |                                         |
| 1+2+5   | 2     |                                         |
| 1+3+4   | 3     | $\mathbb{N^7_2}=\mathbb{N^{(n-1)}_2}$   |
| 2+2+4   | 4     |                                         |
| 2+3+3   | 5     | $\mathbb{N}^6_2-1=\mathbb{N}^{n-2}_2-1$ |



| x+y+z=9 | Count | y+z                                     |
| ------- | ----- | --------------------------------------- |
| 1+1+7   | 1     |                                         |
| 1+2+6   | 2     |                                         |
| 1+3+5   | 3     |                                         |
| 1+4+4   | 4     | $\mathbb{N^8_2}=\mathbb{N^{(n-1)}_2}$   |
| 2+2+5   | 5     |                                         |
| 2+3+4   | 6     | $\mathbb{N}^7_2-1=\mathbb{N}^{n-2}_2-1$ |
| 3+3+3   | 7     | $\mathbb{N}^6_2-2=\mathbb{N}^{n-3}_2-2$ |





| x+y+z=10 | Count | y+z                                     |
| -------- | ----- | --------------------------------------- |
| 1+1+8    | 1     |                                         |
| 1+2+7    | 2     |                                         |
| 1+3+6    | 3     |                                         |
| 1+4+5    | 4     | $\mathbb{N^9_2}=\mathbb{N^{(n-1)}_2}$   |
| 2+2+6    | 5     |                                         |
| 2+3+5    | 6     |                                         |
| 2+4+4    | 7     | $\mathbb{N}^8_2-1=\mathbb{N}^{n-2}_2-1$ |
| 3+3+4    | 8     | $\mathbb{N}^7_2-2=\mathbb{N}^{n-3}_2-2$ |





THE FOLLOWING IS INCOMPLETE:

| x+y+z=n             | Count                                     |      |
| ------------------- | ----------------------------------------- | ---- |
| 1+1+(n-1)           | 1                                         |      |
| 1+2+(n-2)           | 2                                         |      |
| 1+3+(n-3)           | 3                                         |      |
| ...                 | ...                                       |      |
| (n/3)+(n/3)+(n/3)   | n/3                                       | ?    |
| (n/3)+(n/3)+(n+1)/3 | n​​/3 (integer division, discard remainder) | ?    |
| (n/3)+(n/3)+(n+2)/3 | n​​/3 (integer division, discard remainder) | ?    |



Therefore, there are $n/3$​​​ ways to sum two natural numbers to equal $n$​​, without repeating the set of two.

