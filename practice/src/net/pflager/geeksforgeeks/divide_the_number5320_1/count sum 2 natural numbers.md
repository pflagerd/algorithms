How many ways can the natural number $n$​​​ be represented as the sum of $2$​ natural numbers?

This implies $n \ge 2$​​.



| x+y=n   | Count |
| ------- | ----- |
| 1+(n-1) | 1     |
| 2+(n-2) | 2     |
| 3+(n-3) | 3     |
| ...     | ...   |
| (n-3)+3 | (n-3) |
| (n-2)+2 | (n-2) |
| (n-1)+1 | (n-1) |



Therefore, there are $(n-1)$​ ways to sum two natural numbers to equal $n$.





Lets define a function called $C_{\Sigma}(2, m) := m-1$ where $m\ge2$.

