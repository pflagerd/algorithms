How many ways can the natural number $n$​​​ be represented as the sum of $2$​ natural numbers, allowing no repeated sets?

This implies $n \ge 2$​​​, and either $x \le y$​ xor $x\ge y$.

Lets go with $x\le y$.



| x+y=2 | Count |
| ----- | ----- |
| 1+1   | 1     |

| x+y=3 | Count |
| ----- | ----- |
| 1+2   | 1     |

| x+y=4 | Count |
| ----- | ----- |
| 1+3   | 1     |
| 2+2   | 2     |

| x+y=5 | Count |
| ----- | ----- |
| 1+4   | 1     |
| 2+3   | 2     |

| x+y=6 | Count |
| ----- | ----- |
| 1+5   | 1     |
| 2+4   | 2     |
| 3+3   | 3     |

...



| x+y=n         | Count                                                  |
| ------------- | ------------------------------------------------------ |
| 1+(n-1)       | 1                                                      |
| 1+(n-2)       | 2                                                      |
| 1+(n-3)       | 3                                                      |
| ...           | ...                                                    |
| (n/2)+(n/2)   | n/2                                                    |
| (n/2)+(n+1)/2 | n​​/2 (integer division, discard remainder, if n is odd) |



Therefore, there are $n/2$​​​ ways to sum two natural numbers to equal $n$​​, without repeating the set of two.

Lets define $\mathbb{N}^n_2$ to mean the number of ways two natural numbers may sum to n when no repeats of the set of two numbers is allowed.

