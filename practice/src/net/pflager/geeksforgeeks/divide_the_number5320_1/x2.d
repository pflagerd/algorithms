int main (string[] args) {
	import std.stdio, std.conv;
	
	int n = to!int(args[1]);
	
	if (args.length != 2 || n < 2) {
		writeln("usage: x1 <whole number greater than 2>");
		return 1;
	}


	for (int i = 0, j = n; i <= n; j--, i++) {
		writeln(i,'\t',j);
	}
	return 1;
}