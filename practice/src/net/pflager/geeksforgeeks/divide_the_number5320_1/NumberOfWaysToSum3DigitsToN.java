package net.pflager.geeksforgeeks.divide_the_number5320_1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

public class NumberOfWaysToSum3DigitsToN {
//	public static int f3(int N) {
//		int count = 0;
//		for (int a = 1; a <= N / 3; a++) {
//			//count += NumberOfWaysToSum2DigitsToN.f2(N - a) - (a - 1);
//			count += (N - a) / 2 - (a - 1); // equivalent
//		}
//		return count;
//	}	
	
	public static int f3(int N) {
		int count = 0;
		for (int a = 1; a <= N / 3; a++) {
			count += NumberOfWaysToSum2DigitsToN.f2(N - a); // TODO: DPP: Can the sum of this be made explicit?
     // count += (N - a) / 2; // equivalent
		}
		return count - (N / 3 - 1) * (N / 3) / 2; // Triangular numbers for N / 3 - 1
	}	
	
	public static int algorithm_1(int N) {
		int count = 0;
		for (int a = 1, b = 1, c = N - 2; a <= N / 3; a++, b = a, c = N - a - b) {
			for (; b <= c; b++, c--) {
				count++;
				System.err.printf("% 5d: %d + %d + %d = %d\n", count, a, b, c, N);
			}
		}
		return count;
	}
 
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertEquals(4, algorithm_1(7));
		testName = "assertEquals(4, algorithm_1(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(817, algorithm_1(99));
		testName = "assertEquals(817, algorithm_1(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(833, algorithm_1(100));
		testName = "assertEquals(833, algorithm_1(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);


		exec = () -> assertEquals(4, f3(7));
		testName = "assertEquals(4, f3(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(817, f3(99));
		testName = "assertEquals(817, f3(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(833, f3(100));
		testName = "assertEquals(833, f3(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
