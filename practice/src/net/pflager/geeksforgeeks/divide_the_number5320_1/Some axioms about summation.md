$$\sum\limits_{k=1}^{m}1=m$$



$$\sum\limits_{k=1}^{m}1$$ is equivalent to:

```java, c, c++, c#, d
int sum = 0;
for (int i = 1; i <= m; i++) {
	sum += 1;
}
```

