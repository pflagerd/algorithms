package net.pflager.geeksforgeeks.divide_the_number5320_1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class NumberOfWaysToSum2DigitsToN {
	public static int f2(int n) {
		return n / 2;
	}
	
	public static int algorithm_1(int N) {
		int count = 0;
		for (int a = 1, b = N - 1; a <= b; a++, b--) {
			count++;
			System.err.printf("% 5d: %d + %d = %d\n", count, a, b, N);
		}
		return count;
	}
 
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;

		exec = () -> assertEquals(3, algorithm_1(7));
		testName = "assertEquals(3, algorithm1(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(49, algorithm_1(99));
		testName = "assertEquals(49, algorithm1(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(50, algorithm_1(100));
		testName = "assertEquals(50, algorithm1(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(3, f2(7));
		testName = "assertEquals(3, f2(7))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(49, f2(99));
		testName = "assertEquals(49, f2(99))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(50, f2(100));
		testName = "assertEquals(50, f2(100))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
