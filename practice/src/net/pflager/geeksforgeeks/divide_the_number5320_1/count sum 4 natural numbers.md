How many ways can the natural number $n$​​​​​ be represented as the sum of $4$​​​ natural numbers?

This implies $n \ge 4$.



| w+x+y+z=n | Count                | Expansion |
| --------- | -------------------- | --------- |
| 1         | $C_{\Sigma}(3, n-1)$ |           |
| 2         | $C_{\Sigma}(3, n-2)$ |           |
| 3         | $C_{\Sigma}(3, n-3)$ |           |
| ...       |                      |           |
| (n-5)     | 5                    |           |
| (n-4)     | 3                    |           |
| (n-3)     | 1                    |           |



$\sum\limits_{i=1}^{n-3}\sum\limits_{j=1}^{n-i-2}j=\sum\limits_{i=1}^{n-3}\frac{(n-i-2)(n-i-1)}{2}$​​ where $n\ge4$​​​.

$=\sum\limits_{i=1}^{n-2}\frac{1}{2}(n^2-2ni-3n+i^2+3i+2)$​​​ where $n\ge4$​.

$=\frac{1}{2}\sum\limits_{i=1}^{n-2}n^2-\frac{1}{2}\sum\limits_{i=1}^{n-2}2ni-\frac{1}{2}\sum\limits_{i=1}^{n-2}3n+\frac{1}{2}\sum\limits_{i=1}^{n-2}i^2+\frac{1}{2}\sum\limits_{i=1}^{n-2}3i+\frac{1}{2}\sum\limits_{i=1}^{n-2}2$ where $n\ge4$

$=\frac{1}{2}n^2\sum\limits_{i=1}^{n-2}1-n\sum\limits_{i=1}^{n-2}i-\frac{3}{2}n\sum\limits_{i=1}^{n-2}1+\frac{1}{2}\sum\limits_{i=1}^{n-2}i^2+\frac{3}{2}\sum\limits_{i=1}^{n-2}i+\sum\limits_{i=1}^{n-2}1$ where $n\ge4$

$=\frac{1}{2}n^2(n-2)-\frac{1}{2}n(n-2)(n-1)-\frac{3}{2}n(n-2)+\frac{1}{12}(n-2)(n-1)(2n-3)+\frac{3}{4}(n-2)(n-1)+(n-2)$ where $n\ge4$

$=(n-2)(\frac{1}{2}n^2-\frac{1}{2}n(n-1)-\frac{3}{2}n+\frac{1}{12}(n-1)(2n-3)+\frac{3}{4}(n-1)+1)$ where $n\ge4$

$=(n-2)(\frac{1}{2}n^2-\frac{1}{2}n^2+\frac{1}{2}n-\frac{3}{2}n+\frac{1}{12}(2n^2-5n+3)+\frac{3}{4}n-\frac{3}{4}+1)$ where $n\ge4$

$=(n-2)(-n+\frac{1}{6}n^2-\frac{5}{12}n+\frac{1}{4}+\frac{3}{4}n+\frac{1}{4})$ where $n\ge4$

$=(n-2)(\frac{1}{6}n^2-\frac{2}{3}n+\frac{1}{2})$ where $n\ge4$

$=\frac{1}{6}(n-2)(n^2-4n+3)$ where $n\ge4$

$=\frac{1}{6}(n-2)(n-1)(n-3)$ where $n\ge4$

$=\frac{1}{6}(n-1)(n-2)(n-3)$ where $n\ge4$



This is the same as $(n-1) \choose 3$ .   How curious.

