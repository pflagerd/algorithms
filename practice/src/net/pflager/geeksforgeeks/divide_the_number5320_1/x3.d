int main (string[] args) {
	import std.stdio, std.conv;
	
	int n = to!int(args[1]);
	
	if (args.length != 2 || n < 3) {
		writeln("usage: x1 <whole number greater than 3>");
		return 1;
	}

	int count = 0;
	for (int i = 0, j = 0, k = n; i <= n; j++, k--) {
		if (k == -1) {
			i++;
			if(i > n)
				break;
			j = 0;
			k = n - i;
		}
		writeln(i,'\t',j,'\t',k);
		count++;
	}
	writeln("\n",count);
	return 1;
}