int main (string[] args) {
	import std.stdio, std.conv;
	
	int n = to!int(args[1]);
	
	if (args.length != 2 || n < 4) {
		writeln("usage: x1 <whole number greater than 4>");
		return 1;
	}

	int count = 0;
	for (int i = 0, j = 0, k = 0, l = n; i <= n; k++, l--) {
		if (l == -1) {
			j++;
			if (j > n)
				break;
			k = 0;
			l = n - i;
		}
		writeln(i,'\t',j,'\t',k,'\t',l);
		count++;
	}
	writeln("\n",count);
	return 1;
}