How many ways can the natural number $n$​​​​ be represented as the sum of $3$​​ natural numbers?

This implies $n \ge 3$​​​.



| x+y+z=n   | Count | in terms of $C_{\Sigma}(2, m)$              | Expanded $m-1$ |
| --------- | ----- | ------------------------------------------- | -------------- |
| 1+1+(n-2) | 1     |                                             |                |
| 1+2+(n-3) | 2     |                                             |                |
| 1+3+(n-4) | 3     |                                             |                |
| ...       | ...   |                                             |                |
| 1+(n-4)+3 | (n-4) |                                             |                |
| 1+(n-3)+2 | (n-3) |                                             |                |
| 1+(n-2)+1 | (n-2) | $C_{\Sigma}(2, n-1)$                        | $n-2$          |
|           |       |                                             |                |
| 2+1+(n-3) | 1     |                                             |                |
| 2+2+(n-4) | 2     |                                             |                |
| 2+3+(n-5) | 3     |                                             |                |
| ...       | ...   |                                             |                |
| 2+(n-5)+3 | (n-5) |                                             |                |
| 2+(n-4)+2 | (n-4) |                                             |                |
| 2+(n-3)+1 | (n-3) | $C_{\Sigma}(2, n-2)$                        | $n-3$          |
|           |       |                                             |                |
| 3+1+(n-4) | 1     |                                             |                |
| 3+2+(n-5) | 2     |                                             |                |
| 3+3+(n-6) | 3     |                                             |                |
| ...       | ...   |                                             |                |
| 3+(n-6)+3 | (n-6) |                                             |                |
| 3+(n-5)+2 | (n-5) |                                             |                |
| 3+(n-4)+1 | (n-4) | $C_{\Sigma}(2, n-3)$                        | $n-4$          |
| .         |       |                                             |                |
| .         |       |                                             |                |
| .         |       |                                             |                |
| (n-4)+1+3 | 1     |                                             |                |
| (n-4)+2+2 | 2     |                                             |                |
| (n-4)+3+1 | 3     | $C_{\Sigma}(2, n+2-(n-2))=C_{\Sigma}(2, 4)$ | $3$            |
|           |       |                                             |                |
| (n-3)+1+2 | 1     |                                             |                |
| (n-3)+2+1 | 2     | $C_{\Sigma}(2, n+1-(n-2))=C_{\Sigma}(2, 3)$ | $2$            |
|           |       |                                             |                |
| (n-2)+1+1 | 1     | $C_{\Sigma}(2, n+(n-2))=C_{\Sigma}(2, 2)$   | $1$            |



Since integer addition is commutative, summing Count from bottom to top gives

$1+2+3+...+(n-4)+(n-3)+(n-2)=\sum\limits_{i=1}^{n-2}i$ where $n\ge3$​​​​

We know that $\sum\limits_{i=1}^{m}i=\frac{m(m+1)}{2}$​, so if $m=n-2$​, then $\sum\limits_{i=1}^{n-2}i=\frac{(n-2)(n-1)}{2}$​​​​ where $n\ge3$.





Lets define a function called $C_{\Sigma}(3, m) :=\sum\limits_{i=1}^{m-2}i$ where $m\ge3$.

