package net.pflager.geeksforgeeks.maximum_xor_subarray;

//{ Driver Code Starts
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GfG {
	public static void main(String args[]) throws IOException {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(read.readLine());
		while (t-- > 0) {
			int N = Integer.parseInt(read.readLine());
			String input_line[] = read.readLine().trim().split("\\s+");
			int arr[] = new int[N];
			for (int i = 0; i < N; i++)
				arr[i] = Integer.parseInt(input_line[i]);

			System.out.println(Solution.maxSubarrayXOR(N, arr));

		}
	}
}
//} Driver Code Ends

class Solution {

	static int maxSubarrayXOR(int N, int arr[]) {
		// code here
		return -1;
	}
}