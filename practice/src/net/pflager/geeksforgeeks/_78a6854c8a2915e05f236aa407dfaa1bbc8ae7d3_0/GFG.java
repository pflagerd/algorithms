package net.pflager.geeksforgeeks._78a6854c8a2915e05f236aa407dfaa1bbc8ae7d3_0;

//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

class GFG {
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int test = Integer.parseInt(br.readLine());
		while (test-- > 0) {
			int N = Integer.parseInt(br.readLine());
			int[] A = new int[N];
			String[] str = br.readLine().trim().split(" ");
			for (int i = 0; i < N; i++)
				A[i] = Integer.parseInt(str[i]);
			Solution ob = new Solution();
			System.out.println(ob.equalSum(A, N));
		}
	}
}
//} Driver Code Ends

//User function Template for Java
class Solution {
	int equalSum(int[] A, int N) {
		if (A.length == 1)
			return 1;

		int leftSum = 0;
		int rightSum = Arrays.stream(A).sum();
		for (int i = 0; i < N; i++) {
			rightSum -= A[i];
			if (leftSum == rightSum)
				return i + 1;
			leftSum += A[i];
		}
		return -1;
	}
}