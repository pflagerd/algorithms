package net.pflager.geeksforgeeks.binary_fraction_to_decimal4656_1;

//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
class GFG {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
            new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while (t-- > 0) {
            String n = read.readLine();

            Solution ob = new Solution();
            double ans = ob.convertToDecimal(n);
            if((int)ans+0.0==ans)
            System.out.println((int)ans);
            else
            System.out.println(ans);
        }
    }
}
// } Driver Code Ends


class Solution {
    static double convertToDecimal(String n) {
        double powerOf2 = (double)(1L << (n.indexOf('.') - 1));
        double accumulator = 0;
        for (int i = 0, nLength = n.length(); i < nLength; i++) {
            char c = n.charAt(i);
            if (c == '.')
                continue;
            if (c == '1')
                accumulator += (double)powerOf2;
            powerOf2 /= 2.0;
        }
        
        // input: 100110000111011001111010010101.10011111011111
        System.err.printf("%.17g\n", 639475349.62298583984375);
        System.err.printf("%a\n", 639475349.62298583984375);
        double d = Double.valueOf("0x1.30ecf4acfbep29");
        System.err.printf("%.17g\n", (double)d);
        System.err.printf("%.32g\n", (double)d);
        System.err.println(639475349.62298583984375);
        System.err.println(0b100110000111011001111010010101L);
        System.err.println(Long.toString(0b100110000111011001111010010101L, 16));
        System.err.println(Long.toString(0b100110000111011001111010010101_10011111011111L, 16));
        System.err.println(Long.toString(Double.doubleToRawLongBits(d), 16));
        
        return accumulator;
    }
}

