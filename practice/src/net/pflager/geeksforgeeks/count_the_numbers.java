package net.pflager.geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class count_the_numbers {
    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            for (int test = 0; test < numberOfTests; test++) {
                String number = br.readLine().trim();
                int products = 1;
                int sums = 0;
                for (int i = number.length() - 1; i >= 0; i--) {
                    int digitValue = number.charAt(i) - '0';
                    if (i == 0) {
                        if (digitValue > 1) {
                            products *= digitValue - 1;
                            sums += products;
                        }
                    } else {
                        if (number.charAt(i) == '0') {
                            products *= 5;
                            sums += products;
                        } else {
                            products *= digitValue;
                            sums += digitValue;
                        }
                    }
                }
                System.out.println(sums);
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}