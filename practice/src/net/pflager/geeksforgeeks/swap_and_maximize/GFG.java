package net.pflager.geeksforgeeks.swap_and_maximize;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
class GFG {
    private static String toString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(array[i]);
        }
        return stringBuilder.toString();
    }
    
    private static long computeSumOfDifferences(int[] arrayIn) {
    	long sum = 0;
    	for (int i = 0; i < arrayIn.length - 1; i++) {
    		sum += Math.abs(arrayIn[i] - arrayIn[i + 1]);
    	}
    	sum += Math.abs(arrayIn[0] - arrayIn[arrayIn.length - 1]);
    	return sum;
    }
    
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());
		    for (int test = 0; test < numberOfTests; test++) {
		        int n = Integer.parseInt(br.readLine().trim());
		        String[] stringArrayIn = br.readLine().trim().split("\\s+");
		        int[] arrayIn = new int[n];
		        for (int i = 0; i < n; i++) {
		            arrayIn[i] = Integer.parseInt(stringArrayIn[i]);
		        }
		        // |1 - 4| + |4 - 2| + |2 - 8| + |8 - 1|
		        // each number may appear twice. We want to match the biggest number with the smallest, and next smallest
		        
		        System.err.println(toString(arrayIn));
		        System.err.println(computeSumOfDifferences(arrayIn));
		        
		        Arrays.sort(arrayIn);
		        for (int i = 1, j = arrayIn.length - 2; i < j; i += 2, j -= 2) {
		        	int tmp = arrayIn[i]; arrayIn[i] = arrayIn[j]; arrayIn[j] = tmp;
		        }

		        System.err.println(toString(arrayIn));
		        System.err.println(computeSumOfDifferences(arrayIn));
		        
		    }
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}