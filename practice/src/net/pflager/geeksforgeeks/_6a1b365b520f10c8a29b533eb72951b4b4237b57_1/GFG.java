package net.pflager.geeksforgeeks._6a1b365b520f10c8a29b533eb72951b4b4237b57_1;

//{ Driver Code Starts
//Initial Template for Java

//{ Driver Code Starts

//Initial Template for Java

//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG{
  public static void main(String args[])throws IOException
  {
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
      int t = Integer.parseInt(in.readLine());
      while(t-- > 0){
          String arr[] = in.readLine().trim().split("\\s+");
          int H = Integer.parseInt(arr[0]);
          int U = Integer.parseInt(arr[1]);
          int D = Integer.parseInt(arr[2]);
          
          System.out.println(Solution.minStep(H, U, D));
      }
  }
}
//} Driver Code Ends


//User function Template for Java

class Solution {

  // Observations:

  // Write a division algorithm as repeated subtraction by hand.
  
  // Also can iteration always be transformed to recursion (algebraically).

  // Check out integerDivide().
  // Except for the obvious difference of i starting at different numbers, they look
  // very much alike.
  
  static int J = 0;
  
  static int minStep(int H, int U, int D) {
      J++;
      //System.err.println(J);

      if (J > 234)
          System.err.println(H + " " + U + " " + D);
      // Solution A:
      // for (int n = 1; n < 1001; n++) {
      //     if (H - D < n * (U - D))
      //         return n;
      // }
      // return 1;
      
      // return ((H - D) % (U - D) == 0 ? 1 : 0) + (int)Math.ceil((double)(H - D) / (U - D));

      // Solution B:
      // int i = 1;
      // int acc = U;
      // while (true) {
      //     if (acc > H)
      //         return i;
      //     acc += U - D;
      //     i++;
      // }

      // Solution C:
      // int i = 1;
      // int acc = U;
      // for (;;) {
      //     if (acc > H)
      //         return i;
      //     acc += U - D;
      //     i++;
      // }

      // Solution D:
      // for (int i = 1, acc = U; ; acc += U - D, i++) {
      //     if (acc > H)
      //         return i;
      // }

      // Solution E:
      // int i = 1;
      // for (int acc = U; acc <= H; acc += U - D, i++);
      // return i;

      // Solution F:
      // int i = 1;
      // int acc = U;
      // while (acc <= H) {
      //     acc += U - D;
      //     i++;
      // }
      // return i;
      
      // Solution G:
      // int i = 1;
      // int acc = H;
      // while (true) {
      //     if (acc < U) // You always find the thing you're looking for in the last place you look
      //         return i;
      //     acc -= U - D;
      //     i++;
      // }

      // Solution H:
      // int i = 1;
      // int acc = 0;
      // while (true) {
      //     if (acc > H - U)
      //         return i;
      //     acc += U - D;
      //     i++;
      // }

      // Solution I:
      // It's kind of like we're redefining 'acc' by sliding it by D along the number line or some
      // Have to "slide" both the initialization and the "end". i.e. we have an answer that works
      // so we can simply offset it by D, and since we have an algorithm that produces the same
      // answer, we can also "slide it" by sliding the beginning and the ending of the algorithm.
      // i.e. the initialization and the final comparison.
      //
      // int acc = H - D; int i = 1;
      // while (true) {
      //     if (acc < U - D) // You always find the thing you're looking for in the last place you look
      //         return i;
      //     acc -= U - D; i++;
      // }

      // Solution J.
      // So, just substituting H - D for a and U - D for b into integerDivide isn't quite right
      // because of i starting at 1 in Solution I. So why not just add 1 to the answer?
      // 
      // return integerDivide(H - D, U - D) + 1;
      
      // Solution K.
      // well then, if my implementation of integerDivide() is accurate, I should be able
      // to use Java's "/" operator directly
      return (H - D) / (U - D) + 1;
  }
  
  // Here's straight-up integer division as repeated subtraction.
  static int integerDivide(int a, int b) {
      int acc = a; int i = 0;
      while (true) {
          if (acc < b)
              return i;
              acc -= b; i++;
      }
  }
      
  // Here's straight-up integer division as repeated subtraction.
  static int[] integerDivideWithRemainder(int a, int b) {
      if (a < b) { int tmp = a; a = b; b = tmp; }
      
      int acc = a; int i = 0;
      while (true) {
          if (acc < b)
              return new int[] { i, acc };
              acc -= b; i++;
      }
      
  }
  

}
