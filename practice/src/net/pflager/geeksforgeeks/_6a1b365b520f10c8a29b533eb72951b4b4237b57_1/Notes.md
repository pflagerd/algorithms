I observe that in the example "bbaaabb" that "aaa" could be considered a separator, like x,x.  So the answer would be to remove the (single) separator (counts as 1 step), and then remove all the remaining contiguous characters (that would be the 2nd step)

We observe that there will always be a "2nd step", or more accurately a "last step" where we remove the last remaining contiguous block.



David infers that the solution might be the minimum number of "separators" plus 1.

