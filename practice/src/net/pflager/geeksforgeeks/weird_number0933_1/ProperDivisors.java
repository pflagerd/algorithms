package net.pflager.geeksforgeeks.weird_number0933_1;

import java.util.ArrayList;

public class ProperDivisors {
	
	public ArrayList<Integer> find(int n) {
		ArrayList arrayList = new ArrayList<>();
		
		arrayList.add(1);
				
		for (int i = 2; i * i <= n; i++) {
			if (n % i == 0) {
				arrayList.add(i);
				if (n / i != i) {
					arrayList.add(n / i);
				}
			}
		}
		
		return arrayList;
	}

	public static void main(String[] args) {
		ProperDivisors properDivisors = new ProperDivisors();
		
		for (int i = 1; i <= 10000; i++) {
			ArrayList<Integer> arrayList = properDivisors.find(i);
			arrayList.sort(null);
			int sum = arrayList.stream().reduce(0, Integer::sum);
			if (sum > i)
				System.out.println(i + " " + arrayList.toString() + " " + sum);
		}
	}
	
}
