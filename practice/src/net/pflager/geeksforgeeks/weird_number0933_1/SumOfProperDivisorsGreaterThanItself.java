package net.pflager.geeksforgeeks.weird_number0933_1;

import java.util.stream.Collectors;

public class SumOfProperDivisorsGreaterThanItself {

	public static void main(String[] args) {
		Solution ob = new Solution();
		for (int i = 2; i <= 10000; i++) {
			if (ob.is_weird(i) == 1) {
				System.out.println(i + " " + new ProperDivisors().find(i).stream().sorted().collect(Collectors.toList()));
			}
		}
	}

}
