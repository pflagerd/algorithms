package net.pflager.geeksforgeeks.weird_number0933_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//{ Driver Code Starts
//Initial Template for Java
import java.util.Arrays;

class GFG {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		while (T-- > 0) {
			int n = Integer.parseInt(br.readLine().trim());
			Solution ob = new Solution();
			int ans = ob.is_weird(n);
			System.out.println(ans);
		}
	}
}

//} Driver Code Ends

//User function Template for Java

class Solution {
	static final boolean debug = false;

	public int is_weird(int n) {
    int x = n;
    int sum = 0, i;
    
    for (i = n - 1; i >= 1; i--) {
        if ((n % i) == 0) {
            sum += i;
            if (i <= x)
                x -= i;
        }
    }
    
    if (x != 0 && sum > n)
        return 1;
    else
        return 0;
    }
}