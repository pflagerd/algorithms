package net.pflager.geeksforgeeks.two_repeated_elements_1587115621_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		for (int i = 0; i < t; i++) {
			int n = Integer.parseInt(br.readLine());
			String l = br.readLine();
			String[] sarr = l.split(" ");
			int[] arr = new int[sarr.length];
			for (int i1 = 0; i1 < arr.length; i1++) {
				arr[i1] = Integer.parseInt(sarr[i1]);
			}

			Solution obj = new Solution();

			int[] res = obj.twoRepeated(arr, n);
			System.out.println(res[0] + " " + res[1]);
		}
	}
}
//} Driver Code Ends

//User function template for JAVA

class Solution {
	// Function to find two repeated elements.
	
	/* This solution uses `int[] arr` as an array of counts,
	 * encoding the count for each number, which can be either 
	 * 0 or 1 as the product of the element already in each array
	 * element multiplied by 1 or -1 respectively.  By taking
	 * the absolute value of the element, the original value may
	 * be recovered. By checking the value for sign, a negative
	 * value for the sign shows that index of the negative value
	 * was already encountered (else it would be positive).
	 */
	public int[] twoRepeated(int arr[], int N) {
		int returnValueIndex = -1;
		int[] returnValue = new int[2];

		for (int i = 0; i < arr.length; i++) {
			int abs_val = Math.abs(arr[i]);
			if (arr[abs_val] > 0)
				arr[abs_val] = -arr[abs_val];
			else
				returnValue[++returnValueIndex] = abs_val;
		}
		return returnValue;
	}

	public int[] twoRepeatedB(int arr[], int N) { /* Will hold xor of all elements */
		int xor = arr[0];

		/* Will have only single set bit of xor */
		int set_bit_no;

		int i;
		int n = arr.length - 2;
		int x = 0, y = 0;

		/* Get the xor of all elements in arr[] and {1, 2 .. n} */
		for (i = 1; i < arr.length; i++)
			xor ^= arr[i];

		for (i = 1; i <= n; i++)
			xor ^= i;

		/* Get the rightmost set bit in set_bit_no */
		set_bit_no = (xor & ~(xor - 1));

		/*
		 * Now divide elements in two sets by comparing rightmost set bit of xor with
		 * bit at same position in each element.
		 */
		for (i = 0; i < arr.length; i++) {
			int a = arr[i] & set_bit_no;
			if (a != 0)
				x = x ^ arr[i]; /* XOR of first set in arr[] */
			else
				y = y ^ arr[i]; /* XOR of second set in arr[] */
		}

		for (i = 1; i <= n; i++) {
			int a = i & set_bit_no;
			if (a != 0)
				x = x ^ i; /* XOR of first set in arr[] and {1, 2, ...n } */
			else
				y = y ^ i; /* XOR of second set in arr[] and {1, 2, ...n } */
		}

		return new int[] { x, y };
	}
}