package net.pflager.geeksforgeeks.lottery_money0853_1;

//{ Driver Code Starts
import java.io.*;
import java.util.*;

class GFG
{
  public static void main(String args[])throws IOException
  {
      BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
      int t = Integer.parseInt(read.readLine());
      while(t-- > 0)
      {
          Long N = Long.parseLong(read.readLine());

          System.out.println(Solution.totalMoney(N));
      }
  }
}

//} Driver Code Ends


class Solution{
  static long totalMoney(long N){
      return 0;
  }
}
