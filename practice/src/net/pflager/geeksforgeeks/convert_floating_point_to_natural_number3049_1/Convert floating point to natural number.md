Given a positive floating point number N, find the smallest integer k such that when we multiply k and N, we get a natural number.

e.g. 856.36778



Look at the fractional part by itself, since the non-fractional part when multiplied by any k will STILL be an integer.

e.g. 0.36778



The fractional part may be written as $\frac{some\ integer}{some\ integral\ power\ of\ 10}$

e.g. $\frac{36778}{100000}$



So finding the integral power of 10 and multiplying some integer by it will produce an integer, but it might not be the smallest possible integer.  If we could reduce the fraction to its lowest terms, the divisor value would be the smallest k, wouldn't it?



What does it mean to reduce the fraction to its lowest terms?  Doesn't that mean that there is no number other than 1 that divides them both? Doesn't it mean that numerator and denominator are co-prime?  



How may one make both terms co-prime?  Isn't it by dividing them both by their Greatest Common Factor (Greatest Common Divisor)?

e.g. Notice that 36778 and 100000 have a gcd of 2, so the fraction may be reduced to $\frac{18389}{50000}$, so k here would be 50000.


