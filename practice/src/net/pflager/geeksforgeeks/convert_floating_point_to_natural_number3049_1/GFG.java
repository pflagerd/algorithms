package net.pflager.geeksforgeeks.convert_floating_point_to_natural_number3049_1;

//{ Driver Code Starts
//Initial Template for Java

import java.util.*;
import java.lang.*;
import java.io.*;
class GFG
{
  public static void main(String[] args) throws IOException
  {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      int T = Integer.parseInt(br.readLine().trim());
      while(T-->0)
      {
          String N = br.readLine().trim();
          Solution ob = new Solution();
          int ans = ob.findMinMultiple(N);
          System.out.println(ans);
      }
  }
}

//} Driver Code Ends


//User function Template for Java

class Solution
{
  private long pow10(int n) {
      long pow10 = 1l;
      for (int i = 0; i < n; i++) {
          pow10 *= 10;
      }
      return pow10;
  }
  
  private long gcd(long a, long b) {
      long r;
      while ((r = a % b) != 0) {
          a = b; b = r;
      }
      return b;
  }
  
  public int findMinMultiple(String N)
  {
      int decimalPointIndex = N.indexOf('.');
      if (decimalPointIndex == -1)
          return 0;
          
      String fractionalPartString = N.substring(decimalPointIndex + 1);
      long fractionalPartLong = Long.parseLong(fractionalPartString);
      
      long decimalDivisorLong = pow10(fractionalPartString.length());
      
      long gcd = gcd(fractionalPartLong, decimalDivisorLong);
      
      return (int)(decimalDivisorLong / gcd);
  }
}