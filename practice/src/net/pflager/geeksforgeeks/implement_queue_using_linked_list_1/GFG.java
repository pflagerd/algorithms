package net.pflager.geeksforgeeks.implement_queue_using_linked_list_1;

//{ Driver Code Starts
import java.util.*;

class QueueNode {
	int data;
	QueueNode next;

	QueueNode(int a) {
		data = a;
		next = null;
	}
}

class GFG {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while (t > 0) {
			MyQueue obj = new MyQueue();
			int Q = sc.nextInt();
			while (Q > 0) {
				int QueryType = 0;
				QueryType = sc.nextInt();
				if (QueryType == 1) {
					int a = sc.nextInt();

					obj.push(a);

				} else if (QueryType == 2) {
					System.out.print(obj.pop() + " ");
				}
				Q--;
			}
			System.out.println("");
			t--;
		}
		sc.close();
	}
}

// } Driver Code Ends

/*
 * The structure of the node of the queue is class QueueNode { int data;
 * QueueNode next; QueueNode(int a) { data = a; next = null; } }
 */

class MyQueue {
	QueueNode front, rear;

	// Function to push an element into the queue.
	void push(int a) {
		if (front == null && rear == null) {
			front = rear = new QueueNode(a);
		} else {
			rear.next = new QueueNode(a);
			rear = rear.next;
			rear.next = null;
		}
	}

	// Function to pop front element from the queue.
	int pop() {
		if (front == null && rear == null) {
			return -1;
		} else {
			int data = front.data;
			front = front.next;
			if (front == null)
				rear = null;
			return data;
		}
	}
}
