package net.pflager.geeksforgeeks.thief_try_to_excape0710_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
	public static void main(String args[]) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(in.readLine());
		while (t-- > 0) {
			String a[] = in.readLine().trim().split("\\s+");
			int X = Integer.parseInt(a[0]);
			int Y = Integer.parseInt(a[1]);
			int N = Integer.parseInt(a[2]);
			String a1[] = in.readLine().trim().split("\\s+");
			int[] arr = new int[N];
			for (int i = 0; i < N; i++)
				arr[i] = Integer.parseInt(a1[i]);

			System.out.println(Solution.totalJumps(X, Y, N, arr));
		}
	}
}
//} Driver Code Ends

//User function Template for Java

class Solution {
	static int jumps(int h, int X, int Y) {
		int jumps = 1;
		int height = X;
		
		while (true) {
			if (height >= h)
				return jumps;
			height += (X - Y);
			jumps++;
		}
	}
	
	static int totalJumps(int X, int Y, int N, int arr[]) {
		int jumps = 0;
		for (int h : arr) {
			if (X >= h) {
				jumps++;
				System.err.println(h + " " + 1 + " vs " + ((h - Y - 1) / (X - Y) + 1));
				continue;
			}
			
          jumps += (h - Y - 1) / (X - Y) + 1;
//			jumps += jumps(h, X, Y);
			System.err.println(jumps);
		}

		return jumps;
	}
}