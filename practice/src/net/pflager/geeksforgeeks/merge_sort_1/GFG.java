package net.pflager.geeksforgeeks.merge_sort_1;

//{ Driver Code Starts

import java.util.*;

class GFG {
    //method to print the elements of the array
    static void printArray(int arr[]) {
        StringBuffer sb = new StringBuffer("");
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            sb.append(arr[i] + " ");
        System.out.println(sb.toString());
    }

    public static void main(String args[]) {
        //taking input using Scanner class
        Scanner sc = new Scanner(System.in);

        //taking testcases
        int T = sc.nextInt();
        while (T > 0) {
            //taking elements count
            int n = sc.nextInt();

            //creating an object of class Merge_Sort
            GFG ms = new GFG();

            //creating an array of size n
            int arr[] = new int[n];

            //adding elements to the array
            for (int i = 0; i < n; i++)
                arr[i] = sc.nextInt();


            Solution g = new Solution();

            //calling the method mergeSort
            g.mergeSort(arr, 0, arr.length - 1);

            //calling the method printArray
            ms.printArray(arr);
            T--;
        }
    }
} // Driver Code Ends


class Solution {
    void merge(int[] arr, int l, int m, int r) {
        System.err.println("merge(" + Arrays.toString(arr) + ", " + l + ", " + m + ", " + r + ")");

        int lp = l;
        int mp = m + 1;

        int[] tmparr = new int[r - l + 1];

        for (int i = 0; i < tmparr.length; i++) {
            if ((lp <= m && mp <= r && arr[lp] <= arr[mp]) || (lp <= m && mp > r)) {
                tmparr[i] = arr[lp++];
                continue;
            }
            if ((lp <= m && mp <= r && arr[lp] > arr[mp]) || (lp > m && mp <= r)) {
                tmparr[i] = arr[mp++];
            }
        }

        for (int i = 0; i < tmparr.length; i++) {
            arr[l + i] = tmparr[i];
        }

        System.err.println("after merge: " + Arrays.toString(arr) + "\n");
    }


    void mergeSort(int arr[], int l, int r) // l and r are inclusive.
    {
        System.err.println("mergeSort(" + Arrays.toString(arr) + ", " + l + ", " + r + ")");

        if (r - l == 0)
            return;

        if (r - l == 1) {
            if (arr[l] > arr[r]) {
                int temp = arr[l];
                arr[l] = arr[r];
                arr[r] = temp;
            }
            return;
        }

        int midpoint = l + (r - l) / 2;
        mergeSort(arr, l, midpoint);
        mergeSort(arr, midpoint + 1, r);
        merge(arr, l, midpoint, r);
        System.err.println(Arrays.toString(arr) + " l == " + l + " r == " + r + "\n");
    }
}
