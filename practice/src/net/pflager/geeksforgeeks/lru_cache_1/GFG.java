package net.pflager.geeksforgeeks.lru_cache_1;

//{ Driver Code Starts
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;

public class GFG {

  public static void main(String[] args) throws IOException {
      BufferedReader read =
          new BufferedReader(new InputStreamReader(System.in));

      int t = Integer.parseInt(read.readLine());

      while (t-- > 0) {

          int capacity = Integer.parseInt(read.readLine());
          int queries = Integer.parseInt(read.readLine());
          LRUCache cache = new LRUCache(capacity);
          String str[] = read.readLine().trim().split(" ");
          int len = str.length;
          int itr = 0;

          for (int i = 0; (i < queries) && (itr < len); i++) {
              String queryType = str[itr++];
              if (queryType.equals("SET")) {
                  int key = Integer.parseInt(str[itr++]);
                  int value = Integer.parseInt(str[itr++]);
                  cache.set(key, value);
              }
              if (queryType.equals("GET")) {
                  int key = Integer.parseInt(str[itr++]);
                  System.out.print(cache.get(key) + " ");
              }
          }
          System.out.println();
      }
  }
}

//} Driver Code Ends


//design the class in the most optimal way

class LRUCache
{

  static LinkedHashMap<Integer, Integer> lruCache;
  static int capacity;

  //Constructor for initializing the cache capacity with the given value.
  LRUCache(int cap)
  {
      lruCache = new LinkedHashMap<>(cap);
      capacity = cap;
  }

  // Function to return value corresponding to the key.
  public static int get(int key)
  {
      if (!lruCache.containsKey(key))
          return -1;
          
      Integer value = lruCache.remove(key);
      lruCache.put(key, value);
      return value;
      }

  //Function for storing key-value pair.
  public static void set(int key, int value)
  {
      //System.err.println(lruCache.size() + " " + capacity);
      Integer v = lruCache.get(key);
      if (v == null) {
          if (lruCache.size() == capacity) {
              lruCache.remove(lruCache.keySet().iterator().next());
          }
      } else {
      	lruCache.remove(key);
      }

      lruCache.put(key, value);
  }
}
