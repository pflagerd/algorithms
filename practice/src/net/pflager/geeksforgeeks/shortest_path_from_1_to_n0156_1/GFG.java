package net.pflager.geeksforgeeks.shortest_path_from_1_to_n0156_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG
{
    public static void main(String args[])throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while(t-- > 0)
        {
            int n = Integer.parseInt(read.readLine());

            System.out.println(Solution.minStep(n));
        }
    }
}
// } Driver Code Ends


//User function Template for Java
/*
887
493
541
926
337
847
546
365*/
class Solution{
    static int minStep(int n) {
        // System.err.println(n);
        int count = 0;
        int i = n;
        while (i != 0) {
            while (i % 3 == 0) {
                count++;
                i /= 3;
                System.err.println("A: " + count + " " + i);
            }
            if (i == 1) {
                return count;
            }
            System.err.println("B: " + count + " " + i);
            count += i % 3;
            i -= i % 3;
        }
        return count;
    }
}
