package net.pflager.geeksforgeeks.largest_subarray_with_0_sum;

//{ Driver Code Starts
import java.util.HashMap;
import java.util.Scanner;

class GfG {

	// Returns length of the maximum length subarray with 0 sum

	// Drive method
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		while (T > 0) {
			int n = sc.nextInt();
			int arr[] = new int[n];
			for (int i = 0; i < n; i++)
				arr[i] = sc.nextInt();

			Solution g = new Solution();
			System.out.println(g.maxLen(arr, n));
			T--;
		}
		sc.close();
	}
}// } Driver Code Ends

class Solution {
	int maxLen(int arr[], int n) {
		if (arr.length != n) {
			System.err.println("Botch");
			return -1;
		}

		if (arr.length == 0)
			return 0;

		if (arr.length == 1)
			return arr[0] == 0 ? 1 : 0;

		HashMap<Integer, Integer> hashMap = new HashMap<>();

		int maxLength = 0;
		hashMap.put(0, -1);
		int accumulator = 0;

		for (int i = 0; i < arr.length; i++) {
			accumulator += arr[i];
			Integer foundIndex = hashMap.get(accumulator); // autoboxed
			if (foundIndex != null) {
				int foundLength = i - foundIndex;
				if (foundLength > maxLength) {
					maxLength = foundLength;
				}
			} else {
				hashMap.put(accumulator, i);
			}
		}

		return maxLength;
	}
}
