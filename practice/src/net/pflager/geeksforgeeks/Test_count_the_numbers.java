package net.pflager.geeksforgeeks;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

public class Test_count_the_numbers {

    @Test
    public void case_001() throws InterruptedException, IOException {
        String javaHome = System.getProperty("java.home");
        String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
        String classpath = System.getProperty("java.class.path");
        String className = count_the_numbers.class.getName();
        List<String> command = new ArrayList<>();
        command.add(javaBin);
        //command.addAll(jvmArgs);
        command.add("-cp");
        command.add(classpath);
        command.add(className);
        //command.addAll(args);

        ProcessBuilder processBuilder = new ProcessBuilder(command);
//        Map<String, String> env = processBuilder.environment();
//        env.put("VAR1", "myValue");
//        env.remove("OTHERVAR");
//        env.put("VAR2", env.get("VAR1") + "suffix");

        Process process = processBuilder.start();
        PrintWriter processPrintWriter = new PrintWriter(new OutputStreamWriter(process.getOutputStream()));

        processPrintWriter.println(1);
        processPrintWriter.println(10);
        processPrintWriter.close();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {

            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        }

        long startTime = System.currentTimeMillis();
        assertTrue(process.waitFor(10000, TimeUnit.MILLISECONDS));
        process.destroy();
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time == " + (endTime - startTime) / 1000.0 + " s");
    }

}