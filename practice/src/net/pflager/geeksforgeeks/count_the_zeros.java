package net.pflager.geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class count_the_zeros {
    public static int indexOfLeftMostZero(int[] array) {
        int i = (array.length >> 1);
        int j = i;
        while (j < array.length) {
            i = Math.max(i >> 1, 1);
            if (array[j] == 1) {
                j += i;
            } else if (j == 0 || array[j - 1] == 1) {
                return j;
            } else {
                j -= i;
            }
        }
        return -1;
    }
    
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());
		    System.out.println("numberOfTests == " + numberOfTests);
		    for (int test = 0; test < numberOfTests; test++) {
		        int n = Integer.parseInt(br.readLine().trim());
		        System.out.println("n == " + n);
		        String[] arrayAsStrings = br.readLine().trim().split("\\s+");
		        System.out.println("arrayAsStrings[] == " + Arrays.toString(arrayAsStrings));
		        int[] array = new int[n];
		        for (int i = 0; i < n; i++) {
		            array[i] = Integer.parseInt(arrayAsStrings[i]);
		        }

                System.out.println(n - indexOfLeftMostZero(array));
		    }
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}