package net.pflager.geeksforgeeks.nth_multiple_1;

//{ Driver Code Starts
import java.util.Scanner; 
class GFG{
  public static void main(String[] args)
  {
      Scanner sc = new Scanner(System.in);
      long test = sc.nextLong(); 
      while(test!=0)
      {
          long n,k;
          n = sc.nextLong();
          k = sc.nextLong();
          Solution g=new Solution();
          System.out.println(g.findPosition(k,n));
          test--; 
      }
      sc.close();
  }
}

//} Driver Code Ends


class Solution {
  public long findPosition(long A, long N) {
  	long i = 0;
  	
  	for (long f = 0, g = 1; f % A != 0 || f == 0; g = g + f, f = g - f, i++);

  	return N * i;
  }
}