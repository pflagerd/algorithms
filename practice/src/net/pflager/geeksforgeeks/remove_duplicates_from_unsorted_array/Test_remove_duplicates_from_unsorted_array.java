package net.pflager.geeksforgeeks.remove_duplicates_from_unsorted_array;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

public class Test_remove_duplicates_from_unsorted_array extends GFG {
	
	class ConsoleAndFileOutputStream extends OutputStream {
		private OutputStream console = System.out;
		private OutputStream fileOutputStream;
		
		public ConsoleAndFileOutputStream(String fileName) throws FileNotFoundException {
			fileOutputStream = new FileOutputStream(new File(fileName));
		}
		
		@Override
		public void close() throws IOException {
			fileOutputStream.close();
		}

		@Override
		public void write(int arg0) throws IOException {
			console.write(arg0);
			fileOutputStream.write(arg0);
		}
	}

    @Test
    public void case_001() throws IOException {
        try (PrintWriter processPrintWriter = new PrintWriter(new ConsoleAndFileOutputStream("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/input.data"))) {    	
        	String[] questions = {
	    		"1",
		    	"6",
		    	"1 2 3 1 4 2"
        	};
        	for (int i = 0; i < questions.length; i++) {
        		processPrintWriter.println(questions[i]);
        	}
        }

        PrintStream console = System.out;
        try (InputStream inputStream = new FileInputStream(new File("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/input.data"))) {
        	try (PrintStream printStream = new PrintStream(new ConsoleAndFileOutputStream("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/output.data"))) {
	            System.setIn(inputStream);
	            System.setOut(printStream);
	            long startTime = System.currentTimeMillis();
	        	main(new String[0]);
	            long endTime = System.currentTimeMillis();
	            System.setOut(console);
	            System.out.println("Elapsed time == " + (endTime - startTime) / 1000.0 + " s");
        	}
        }

        int i = 0;
        String[] answers = { "2", "-1", "-1", "-1" };
		try (BufferedReader br = new BufferedReader(new FileReader("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/output.data"))) {
			for (String s = br.readLine(); s != null; i++, s = br.readLine()) { 
				assertEquals(answers[i], s);
			}
		}

		Files.delete(Paths.get("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/input.data"));
		Files.delete(Paths.get("test/net/pflager/geeksforgeeks/remove_duplicates_from_unsorted_array/output.data"));
		
    }
    
    
}