package net.pflager.geeksforgeeks.remove_duplicates_from_unsorted_array;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

class GFG {
    private static String toString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(array[i]);
        }
        return stringBuilder.toString();
    }
    
	public static void main (String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
		    int numberOfTests = Integer.parseInt(br.readLine());
		    for (int test = 0; test < numberOfTests; test++) {
		        int n = Integer.parseInt(br.readLine().trim());
		        String[] arrayAsStrings = br.readLine().trim().split("\\s+");
		        int[] array = new int[n];
		        for (int i = 0; i < n; i++) {
		            array[i] = Integer.parseInt(arrayAsStrings[i]);
		        }
		        
		        boolean[] found = new boolean[101];
		        
		        int state = 0;
		        int j = 0;
		        for (int i = 0; i < n; i++) {
		            switch (state) {
		                case 0: // looking for duplicate. copying nothing
		                    if (found[array[i]]) {
		                        state = 1; // found duplicate
		                        j = i; // insert here.
		                        continue;
		                    }
		                    found[array[i]] = true;
		                    break;
		                    
		                case 1: // found duplicate, look for non-duplicate, not copying
		                    if (found[array[i]]) {
		                        continue;
		                    }
		                    found[array[i]] = true;
                            array[j++] = array[i];
		                    state = 2;
		                    break;
		                    
		                case 2: // found non-duplicate, looking for duplicate, copying
		                    if (found[array[i]]) {
		                        state = 1; // found duplicate
		                        continue;
		                    }
                            array[j++] = array[i];
                            found[array[i]] = true;
		                    break;
		            }
		        }
		        if (state == 0) {
		            System.out.println(toString(array));
		        } else {
		            System.out.println(toString(Arrays.copyOf(array, j)));
		        }
			}
		}
		catch (IOException e) {
			System.err.println(e);
		}
    }
}