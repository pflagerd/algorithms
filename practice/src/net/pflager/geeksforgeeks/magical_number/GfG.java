package net.pflager.geeksforgeeks.magical_number;

// { Driver Code Starts
import java.util.Scanner;

class Magic
{
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while(t-- > 0)
        {
            int n = sc.nextInt();
            int arr[] = new int[n];

            for(int i = 0; i < n; i++)
                arr[i] = sc.nextInt();

            System.out.println(GfG.binarySearch(arr, 0, n-1));
        }
        sc.close();
    }
}

// } Driver Code Ends
class GfG
{
    public static int binarySearchA(int arr[], int low, int high)
    {
        int currentIndex = (high - low) / 2 + low;
        System.err.println("currentIndex == " + currentIndex);

        if (arr[currentIndex] == currentIndex) {
            return currentIndex;
        }

        if (high == low) {
            return -1;
        }

        if (arr[currentIndex] > currentIndex) {
            return binarySearch(arr, low, currentIndex - 1);
        }

        return binarySearch(arr, currentIndex + 1, high);
    }

    public static int binarySearch(int arr[], int low, int high)
    {
        for (;;) {
            int currentIndex = (high - low) / 2 + low;
            System.err.println("currentIndex == " + currentIndex);

            if (arr[currentIndex] == currentIndex) {
                return currentIndex;
            }

            if (high == low) {
                return -1;
            }

            if (arr[currentIndex] > currentIndex) {
                high = currentIndex - 1;
                continue;
            }

            low = currentIndex + 1;
        }
    }
}

