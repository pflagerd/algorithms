package net.pflager.geeksforgeeks.lucky_number_and_a_sum0329_1;

//{ Driver Code Starts
// Initial Template for Java

import java.io.*;
import java.util.*;

class GFG {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
            new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while (t-- > 0) {
            int N = Integer.parseInt(read.readLine());
            Solution ob = new Solution();
            System.out.println(ob.minimumLuckyNumber(N));
        }
    }
}
// } Driver Code Ends


// User function Template for Java

class Solution {
    // 
    // 4 and 7
    //
    //  maximize 4s and minimize 7s
    //  put 4s before 7s in the number
    //
    //  4a + 7b = N
    //
    //  if b = 0, a can at most be N/4
    //  if a = 0, b can at most be N/7
    // 
    // 
    // String minimumLuckyNumber(int N) {
    //     int a = 0, b = 0;
    //     for (; a <= N/4; a++) {
    //         if ((N - 4 * a) % 7 == 0) {
    //             b = (N - 4 * a) / 7;
    //             System.err.println("a == " + a + ", b == " + b);
    //             StringBuilder stringBuilder = new StringBuilder();
    //             for (int i = 0; i < a; i++) {
    //                 stringBuilder.append('4');
    //             }
    //             for (int i = 0; i < b; i++) {
    //                 stringBuilder.append('7');
    //             }
    //             return stringBuilder.toString();
    //         }
    //     }
    //     return "-1";
    // }

    String minimumLuckyNumber(int N) {
        int a = 0, b = 0;
        for (b = N / 7; b >= 0; b--) {
            if ((N - 7 * b) % 4 == 0) {
                a = (N - 7 * b) / 4;
                //System.err.println("a == " + a + ", b == " + b);
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < a; i++) {
                    stringBuilder.append('4');
                }
                for (int i = 0; i < b; i++) {
                    stringBuilder.append('7');
                }
                return stringBuilder.toString();
            }
        }
        return "-1";
    }

}
