package net.pflager.geeksforgeeks.coin_piles5152_1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class AllTests extends GFG {

	class ConsoleAndFileOutputStream extends OutputStream {
		private OutputStream console = System.out;
		private OutputStream fileOutputStream;

		public ConsoleAndFileOutputStream(String fileName) throws FileNotFoundException {
			fileOutputStream = new FileOutputStream(new File(fileName));
		}

		@Override
		public void close() throws IOException {
			fileOutputStream.close();
		}

		@Override
		public void write(int arg0) throws IOException {
			console.write(arg0);
			fileOutputStream.write(arg0);
		}
	}

	class ConsoleAndByteArrayOutputStream extends ByteArrayOutputStream {
		private OutputStream console = System.out;

		@Override
		public void close() throws IOException {
			super.close();
		}

		@Override
		public void write(int arg0) {
			try {
				console.write(arg0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.write(arg0);
		}
	}

	public static int countSpaces(String string) {
		int count = 0;
		int length = string.length();
		for (int i = 0; i < length; i++) {
			if (string.charAt(i) == ' ')
				count++;
		}
		return count;
	}
	
	private static class TestCase {
		StringBuilder input = new StringBuilder();
		StringBuilder expected_output = new StringBuilder();
	}

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() throws IOException, XPathExpressionException {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		PrintStream console = System.out;

		var testCases = new ArrayList<TestCase>();
		StringBuilder rawTestData = new StringBuilder();
		StringBuilder rawTestResults = new StringBuilder();

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			try {
				Document document = builder.parse(new FileInputStream(
						"src/" + getClass().getPackage().getName().replaceAll("\\.", "/") + "/testdata.xml"));

				XPathFactory xpathfactory = XPathFactory.newInstance();
				XPath xpath = xpathfactory.newXPath();

				XPathExpression expr = xpath.compile("//testdata/testcase");
				NodeList testCaseNodeList = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
				rawTestData.append(testCaseNodeList.getLength() + "\n"); // Puts the number of test cases into rawTestData
				for (int i = 0; i < testCaseNodeList.getLength(); i++) {
					NodeList testCaseChildNodeList = testCaseNodeList.item(i).getChildNodes();
					String input = null;
					String output = null;
					for (int j = 0; j < testCaseChildNodeList.getLength(); j++) {
						Node testCaseChildNode = testCaseChildNodeList.item(j);
						if (testCaseChildNode.getNodeType() == Node.ELEMENT_NODE) {
							if (testCaseChildNode.getNodeName().contentEquals("input"))
								input = testCaseChildNode.getTextContent().trim();
							else if (testCaseChildNode.getNodeName().contentEquals("expected_output"))
								output = testCaseChildNode.getTextContent().trim();
						}
					}
					
					TestCase testCase = new TestCase();
					
					if (input.indexOf("\n") != -1) {
						String[] ins = input.split("\n");
						for (String in : ins) {
							in = in.strip();
							testCase.input.append(in + "\n");
						}
					} else {
						testCase.input.append(input + "\n");
					}
					rawTestData.append(testCase.input.toString());
					testCase.input.deleteCharAt(testCase.input.length() - 1); // I know it's a little goofy, but trim the trailing newline for the testCase.input.
					
					testCase.expected_output.append(output); // See, this has no trailing newline either.
					rawTestResults.append(output + "\n");
					
					testCases.add(testCase);
				}

				ConsoleAndByteArrayOutputStream consoleAndByteArrayOutputStream = new ConsoleAndByteArrayOutputStream();
				try (InputStream inputStream = new ByteArrayInputStream(rawTestData.toString().getBytes())) {
					try (PrintStream printStream = new PrintStream(consoleAndByteArrayOutputStream)) {
						System.setIn(inputStream);
						System.setOut(printStream);
						main(new String[0]);
						System.setOut(console);
					}
				}

				int i = 0;
				try (BufferedReader rawTestResultsData = new BufferedReader(
						new InputStreamReader(new ByteArrayInputStream(rawTestResults.toString().getBytes())))) {
					try (BufferedReader rawOutputData = new BufferedReader(new InputStreamReader(
							new ByteArrayInputStream(consoleAndByteArrayOutputStream.toString().getBytes())))) {
						for (String rawOutputDataString = rawOutputData.readLine(),
								rawTestResultsString = rawTestResultsData.readLine(); rawOutputDataString != null
										&& rawTestResultsString != null; rawOutputDataString = rawOutputData
												.readLine(), rawTestResultsString = rawTestResultsData.readLine()) {							
							assertEquals(rawTestResultsString, rawOutputDataString);
							final String rawTestResultsStringFinal = rawTestResultsString;
							final String rawOutputDataStringFinal = rawOutputDataString;
							
							Executable exec = () -> assertEquals(rawTestResultsStringFinal, rawOutputDataStringFinal);
							TestCase testCase = testCases.get(i++);
							String testName = "input == \"" + testCase.input + "\", expected_output == \"" + testCase.expected_output + "\"";
							DynamicTest dTest = DynamicTest.dynamicTest(testName, exec);
							dynamicTests.add(dTest);
						}
					}
				}

				if (System.getProperty("compute.performance") != null) {
					long elapsedTime = 0;
					for (i = 0; i < 1_000_000; i++) {
						consoleAndByteArrayOutputStream = new ConsoleAndByteArrayOutputStream();
						try (InputStream inputStream = new ByteArrayInputStream(rawTestData.toString().getBytes())) {
							try (PrintStream printStream = new PrintStream(consoleAndByteArrayOutputStream)) {
								System.setIn(inputStream);
								System.setOut(printStream);
								long startTime = System.currentTimeMillis();
								main(new String[0]);
								elapsedTime += System.currentTimeMillis() - startTime;
								System.setOut(console);
							}
						}
					}
					System.out.println("Elapsed time == " + elapsedTime / 1000.0 + " s");
				}
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return dynamicTests;
	}
}