package net.pflager.geeksforgeeks.coin_piles5152;

//{ Driver Code Starts
//Initial Template for Java

//{ Driver Code Starts

//Initial Template for Java

//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

class GfG {
  public static void main(String args[]) throws IOException {
      BufferedReader read =
          new BufferedReader(new InputStreamReader(System.in));
      int t = Integer.parseInt(read.readLine());
      while (t-- > 0) {
          String S1[] = read.readLine().split(" ");
          int N = Integer.parseInt(S1[0]);
          int K = Integer.parseInt(S1[1]);
          
          String S[] = read.readLine().split(" ");
          int[] A = new int[N];
          
          for(int i=0; i<N; i++)
              A[i] = Integer.parseInt(S[i]);

          Solution ob = new Solution();
          System.out.println(ob.minSteps(A,N,K));
      }
  }
}
//} Driver Code Ends


//User function Template for Java

class Solution {
  private final boolean debug = false;
  
  static class CostAndNewMaxOrNewMin {
      public CostAndNewMaxOrNewMin(int cost, int maxOrMin) {
          this.cost = cost;
          this.newMaxOrMin = maxOrMin;
      }
      
      int cost;
      int newMaxOrMin;
  }
  
  
  // Can lower maximum by 1) removing one coin, or 2) removing one pile
  double benefitCostLoweringMaximum(int[] coinCounts, int min, int max) {
      int costToRemovePile = coinCounts[max] * max;
      int benefitToRemovePile = 1;
      for (int i = max - 1; coinCounts[i] == 0; i--) {
          benefitToRemovePile++;
      }
      
      double benefitCostToRemovePile = (double)benefitToRemovePile / costToRemovePile;
      
      int costToRemoveCoin = coinCounts[max];
      double benefitCostToRemoveCoin = 1.0 / costToRemoveCoin;
      if (benefitCostToRemovePile < benefitCostToRemoveCoin)
          return benefitCostToRemoveCoin;
      
      return (double)benefitToRemovePile / costToRemovePile;
  }
  
  static CostAndNewMaxOrNewMin lowerMaximum(int [] coinCounts, int min, int max) {
      int costToRemovePile = coinCounts[max] * max;
      int benefitToRemovePile = 1;
      for (int i = max - 1; coinCounts[i] == 0; i--) {
          benefitToRemovePile++;
      }
      
      double benefitCostToRemovePile = (double)benefitToRemovePile / costToRemovePile;
      
      // the benefitCost (benefit/Cost) to remove one coin is 1 / coinCount[max].
      int costToRemoveCoin = coinCounts[max];
      if (benefitCostToRemovePile > 1.0 / costToRemoveCoin) {
          coinCounts[max] = 0;
          return new CostAndNewMaxOrNewMin(costToRemovePile, max - benefitToRemovePile);
      }
          
      coinCounts[max - 1] += coinCounts[max];
      coinCounts[max] = 0;
      
      return new CostAndNewMaxOrNewMin(costToRemoveCoin, max - 1);
  }

  // Can raise minimum by 1) removing one pile    
  double benefitCostRaisingMinimum(int[] coinCounts, int min, int max) {
      int costToRemovePile = coinCounts[min] * min;
      int benefitToRemovePile = 1;
      for (int i = min + 1; coinCounts[i] == 0; i++) {
          benefitToRemovePile++;
      }
      
      return (double)benefitToRemovePile / costToRemovePile;
  }
  
  CostAndNewMaxOrNewMin raiseMinimum(int [] coinCounts, int min, int max) {
      int costToRemovePile = coinCounts[min] * min;
      int benefitToRemovePile = 1;
      for (int i = min + 1; coinCounts[i] == 0; i++) {
          benefitToRemovePile++;
      }
      coinCounts[min] = 0;
      return new CostAndNewMaxOrNewMin(costToRemovePile, min + benefitToRemovePile);
  }
  
  int minSteps(int[] A, int N, int K) {
      int max = Integer.MIN_VALUE;
      for (int i = 0; i < A.length; i++) {
          if (A[i] > max)
              max = A[i];
      }
      
      int[] coinCounts = new int[max + 1];   // The problem statement is inaccurate. 5 added as buffer.
      int min = Integer.MAX_VALUE;
      // int max = Integer.MIN_VALUE;
      for (int i = 0; i < A.length; i++) {
          coinCounts[A[i]]++;
          
          if (A[i] < min)
              min = A[i];
              
          // if (A[i] > max)
          //     max = A[i];
      }
      if (debug) System.err.println(Arrays.toString(coinCounts) + ", max = " + max + ", min = " + min);
      
      if (max == min)
          return 0;

      int cost = 0;
      while (max - min > K) {
          double bclm = benefitCostLoweringMaximum(coinCounts, min, max);
          if (debug) System.err.println("bclm = " + bclm);
          double bcrm = benefitCostRaisingMinimum(coinCounts, min, max);
          if (debug) System.err.println("bcrm = "+ bcrm);
          if (bclm <= bcrm) {
              CostAndNewMaxOrNewMin thing = raiseMinimum(coinCounts, min, max);
              cost += thing.cost;
              min = thing.newMaxOrMin;
          } else {
              CostAndNewMaxOrNewMin thing = lowerMaximum(coinCounts, min, max);
              cost += thing.cost;
              max = thing.newMaxOrMin;
          }
      if (debug) System.err.println(Arrays.toString(coinCounts) + ", max = " + max + ", min = " + min);
      }
      
      if (debug) System.err.println("cost == " + cost);
      return cost;
  }
};
