package net.pflager.geeksforgeeks.template;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import javax.swing.JOptionPane;

public class TemplateInstantiator {
	
	/**
	 * Creates a new instance of a practice problem template.  Creates packages in src/net/pflager and net/pflager
	 * based on a problem name which you provide.
	 * 
	 * @param problemName
	 * 	    e.g.
	 * 			compute_square_root_using_newtons_method
	 * 
	 * @return 0 if successful, 1 if not successful.
	 */
	public static int addNewProjectFromTemplate(String uri) {
		String packageName = extractPackageNameFromUri(uri); // System.getProperty("user.dir");
		
		Path srcPath = Path.of("src/" + packageName.replaceAll("\\.", "/"));
		// create package directory in src/
		try {
			Files.createDirectory(srcPath);
		} catch (FileAlreadyExistsException faee) {			
		} catch (IOException e) {
			throw new RuntimeException("IOException " + e);
		}
		
		// copy GfG.java from net.pflager.geeksforgeeks.template and update its <code>package</code> statement to the 
		// packagename extracted from the uri
		Path gfgJavaPath = Path.of(srcPath.toString() + "/GFG.java");
		try {
			Files.writeString(gfgJavaPath, Files.readString(Path.of("src/net/pflager/geeksforgeeks/template/GFG.java")).replace("package net.pflager.geeksforgeeks.template;", "package " + packageName + ";"), StandardOpenOption.CREATE_NEW);
		} catch (FileAlreadyExistsException faee) {			
		} catch (IOException e) {
			throw new RuntimeException("IOException " + e);
		}
		
		// copy AllTests.java and update its <code>package</code> statement to the packagename extracted
		// from the uri
		Path allTestsJavaPath = Path.of(srcPath + "/AllTests.java");
		try {
			Files.writeString(allTestsJavaPath, Files.readString(Path.of("src/net/pflager/geeksforgeeks/template/AllTests.java")).replace("package net.pflager.geeksforgeeks.template;", "package " + packageName + ";"), StandardOpenOption.CREATE_NEW);
		} catch (FileAlreadyExistsException faee) {			
		} catch (IOException e) {
			throw new RuntimeException("IOException " + e);
		}
		
		// copy analysis.html and update its contents to match the given uri
		Path analysisHtmlPath = Path.of(srcPath + "/analysis.html");
		try {
			Files.writeString(analysisHtmlPath, Files.readString(Path.of("src/net/pflager/geeksforgeeks/template/analysis.html")).replaceAll("https://practice.geeksforgeeks.org/problems/equalize-the-towers2804/1", uri), StandardOpenOption.CREATE_NEW);
		} catch (FileAlreadyExistsException faee) {
		} catch (IOException e) {
			throw new RuntimeException("IOException " + e);
		}
		
		// copy testdata.xml
		try {
			Files.copy(Path.of("src/net/pflager/geeksforgeeks/template/testdata.xml"), Path.of(srcPath + "/testdata.xml"));
		} catch (FileAlreadyExistsException faee) {			
		} catch (IOException e) {
			throw new RuntimeException("IOException " + e);
		}
		
		return 0;
	}
	
	public static int addNewProjectsFromTemplate(String[] uris) {
		if (uris.length == 0) {
			return addNewProjectFromTemplate(JOptionPane.showInputDialog("Please enter the GeeksForGeeks problem URI"));
		}
		
		int returnValue = 0;
		for (String uri : uris) {
			int rv = addNewProjectFromTemplate(uri);
			if (rv != 0)
				returnValue = rv; // most recent nonzero returnValue
		}
		return returnValue;
	}
	
	/**
	 * 
	 * @param uriString A string containing the GeeksForGeeks uri of a practice problem.
	 * 	    e.g.
	 * 			"https://practice.geeksforgeeks.org/problems/all-numbers-with-specific-difference3558/1"
	 * 
	 * @return A fully qualified package name.
	 * 		e.g.
	 * 			"net.pflager.geeksforgeeks.all_numbers_with_specific_differences"
	 */
	public static String extractPackageNameFromUri(String uriString) {
		try {
			URI uri = new URI(uriString);
			uri = new URI("https://practice.geeksforgeeks.org/problems/").relativize(uri);
			StringBuilder targetStringBuilder = new StringBuilder("net.pflager.geeksforgeeks." + uri.getPath().replaceAll("\\-", "_").replaceAll("/", "_"));
			for (int i = targetStringBuilder.indexOf("."); i >= 0; i = targetStringBuilder.indexOf(".", i + 1)) {
				if (!Character.isJavaIdentifierStart(targetStringBuilder.charAt(i + 1))) {
					targetStringBuilder.insert(i + 1, "_");
				}
			}
			return targetStringBuilder.toString();
		} catch (URISyntaxException e) {
			throw new RuntimeException("URISyntaxException " + e);
		}
	}
	
	public static void main(String[] args) {
		System.exit(addNewProjectsFromTemplate(args));
	}
}
