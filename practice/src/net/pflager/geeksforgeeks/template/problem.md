**Equalize the Towers** 

**Easy** Accuracy: 48.77% Submissions: 680 Points: 2

------



Given heights **h[]** of **N** towers, the task is to bring every tower to the same height by either adding or removing blocks in a tower. Every addition or removal operation costs **cost[]** a particular value for the respective tower. Find out the **Minimum cost to Equalize the Towers**.

**Example 1:**

```
Input: N = 3, h[] = {1, 2, 3} 
cost[] = {10, 100, 1000}
Output: 120
Explanation: The heights can be equalized 
by either "Removing one block from 3 and 
adding one in 1" or "Adding two blocks in 
1 and adding one in 2". Since the cost 
of operation in tower 3 is 1000, the first 
process would yield 1010 while the second 
one yields 120. Since the second process 
yields the lowest cost of operation, it is 
the required output.
```

 

**Example 2:**

```
Input: N = 5, h[] = {9, 12, 18, 3, 10} 
cost[] = {100, 110, 150, 25, 99}
Output: 1623 
```

 

**Your Task:**
This is a function problem. You don't need to take any input, as it is already accomplished by the driver code. You just need to complete the function **Bsearch**() that takes integer **N,** array **H**, and array **Cost** as parameters and returns the minimum cost required to equalize the towers.

 

**Expected Time Complexity:** O(NlogN). 
**Expected Auxiliary Space:** O(1).

**Constraints:**
1 ≤ N ≤ 106

```java
// { Driver Code Starts
//Initial Template for Java


//Initial Template for Java



import java.io.*;
import java.util.*;


 // } Driver Code Ends
//User function Template for Java



class Solution
{
    long Bsearch(int N, long h[],long Cost[])
    {


    }
}


// { Driver Code Starts.

// Driver class
class Array {

    // Driver code
    public static void main(String[] args) throws IOException {
        // Taking input using buffered reader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int testcases = Integer.parseInt(br.readLine());
        // looping through all testcases
        while (testcases-- > 0) {
            int n = Integer.parseInt(br.readLine());
//            String line = br.readLine();
//            String[] q = line.trim().split("\\s+");
//            int n =Integer.parseInt(q[0]);
//            int m =Integer.parseInt(q[1]);
//            //int y =Integer.parseInt(q[2]);
            String line1 = br.readLine();
            String[] a1 = line1.trim().split("\\s+");
            long a[] = new long[n];
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(a1[i]);
            }
            String line2 = br.readLine();
            String[] a2 = line2.trim().split("\\s+");
            long b[] = new long[n];
            for (int i = 0; i < n; i++) {
                b[i] = Long.parseLong(a2[i]);
            }
            Solution ob = new Solution();
            long ans=ob.Bsearch(n,a,b);
            System.out.println(ans);
        }
    }
}


  // } Driver Code Ends
```

