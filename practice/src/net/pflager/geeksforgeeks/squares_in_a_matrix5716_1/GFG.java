package net.pflager.geeksforgeeks.squares_in_a_matrix5716_1;

//{ Driver Code Starts

//Initial Template for Java

//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// } Driver Code Ends
//User function Template for Java

class Solution {
    long c(long h, long[] height, long[] cost) {
        long value = 0;
        for (int i = 0; i < height.length; i++) {
            value += Math.abs(height[i] - h) * cost[i];
        }
        return value;
    }
    
    long bsearch(long min, long max, long[] h, long[] Cost) {
        long mid = (max + 1 + min) / 2;

        long costMid = c(mid, h, Cost);
        if (max == min) {
            return costMid;            
        }   
        
        if (costMid <= c(mid - 1, h, Cost) && costMid <= c(mid + 1, h, Cost))
            return costMid;
            
        if (costMid > c(mid - 1, h, Cost)) {
            return bsearch(min, mid - 1, h, Cost);
        } else {
            return bsearch(mid + 1, max, h, Cost);
        }

    }
    
    
    long Bsearch(int N, long h[], long Cost[])
    {
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        for (int i = 0; i < h.length; i++) {
            if (h[i] < min)
                min = h[i];
            if (h[i] > max)
                max = h[i];
        }
        // System.err.println("min == " + min + ", max == " + max);

        return bsearch(min, max, h, Cost);
    }
}

//{ Driver Code Starts.

//Driver class
class GFG {

	// Driver code
	public static void main(String[] args) throws IOException {
		// Taking input using buffered reader
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int testcases = Integer.parseInt(br.readLine());
		// looping through all testcases
		while (testcases-- > 0) {
			int n = Integer.parseInt(br.readLine());
//         String line = br.readLine();
//         String[] q = line.trim().split("\\s+");
//         int n = Integer.parseInt(q[0]);
//         int m = Integer.parseInt(q[1]);
//         //int y = Integer.parseInt(q[2]);
			String line1 = br.readLine();
			String[] a1 = line1.trim().split("\\s+");
			long a[] = new long[n];
			for (int i = 0; i < n; i++) {
				a[i] = Long.parseLong(a1[i]);
			}
			String line2 = br.readLine();
			String[] a2 = line2.trim().split("\\s+");
			long b[] = new long[n];
			for (int i = 0; i < n; i++) {
				b[i] = Long.parseLong(a2[i]);
			}
			Solution ob = new Solution();
			long ans = ob.Bsearch(n, a, b);
			System.out.println(ans);
		}
	}
}

// } Driver Code Ends
