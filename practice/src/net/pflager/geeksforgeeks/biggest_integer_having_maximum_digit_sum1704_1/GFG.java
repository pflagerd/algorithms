package net.pflager.geeksforgeeks.biggest_integer_having_maximum_digit_sum1704_1;

//{ Driver Code Starts
//Initial Template for Java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GFG {
 public static void main(String args[]) throws IOException {
     BufferedReader read =
         new BufferedReader(new InputStreamReader(System.in));
     int t = Integer.parseInt(read.readLine());
     while (t-- > 0) {
         String S = read.readLine();
         System.out.println(Solution.divisibleByDigits(S));
     }
 }
}
//} Driver Code Ends


//User function Template for Java

class Solution {
 static java.math.BigInteger[] bigIntegers = new java.math.BigInteger[10];
 static {
     for (int i = 1; i <= 9; i++)
         bigIntegers[i] = java.math.BigInteger.valueOf(i);
 }
 
 static int testCount = 0;

 static int divisibleByDigits(String S) {
     if (testCount++ >= 1) {
         System.err.println(S);
         return -1;
     }
     
     int[] digitCounts = new int[10];
     for (int i = 0, sLength = S.length(); i < sLength; i++) {
         digitCounts[S.charAt(i) - '0']++;
     }
     
     // If all zeros
     if (digitCounts[0] != 0 && digitCounts[1] == 0  && digitCounts[2] == 0 && digitCounts[3] == 0 && digitCounts[4] == 0 && digitCounts[5] == 0 && digitCounts[6] == 0 && digitCounts[7] == 0 && digitCounts[8] == 0 && digitCounts[9] == 0) {
         System.err.println("All Zeros");
         return 0;
     }

     java.math.BigInteger N = new java.math.BigInteger(S);
     int count = 0;
     boolean[] visited = new boolean[10];
     for (int i = 9; i >= 1; i--) {
         if (digitCounts[i] == 0)
             continue;
             
         if (!visited[i]) {
             if (N.remainder(bigIntegers[i]).equals(java.math.BigInteger.ZERO)) {
                 if (i == 9) {
                     count += digitCounts[9] + digitCounts[3];
                     visited[3] = visited[9] = true;
                 } else if (i == 8) {
                     count += digitCounts[8] + digitCounts[4] + digitCounts[2];
                     visited[2] = visited[4] = visited[8] = true;
                 } else if (i == 6) {
                     count += digitCounts[6] + digitCounts[3] + digitCounts[2];
                     visited[2] = visited[3] = visited[6] = true;
                 } else if (i == 4) {
                     count += digitCounts[4] + digitCounts[2];
                     visited[2] = visited[4] = true;
                 } else {
                     visited[i] = true;
                     count += digitCounts[i];
                 }
             }
         }
     }
     
     // for (int i = 0, sLength = S.length(); i < sLength; i++) {
     //     int digit = S.charAt(i) - '0';
         
     //     if (digit == 0)
     //         continue;
     //     else if (digit == 1 || isDivisible[digit])
     //         count++;
     // }
     return count;
 }
}


