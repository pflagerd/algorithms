package net.pflager.trace;

public class fib1 {
	//@trace
	public static void main(String[] args) {
		System.err.println("fib.java(6):     int f = 0; 			// (int f = 0) == void"); int f = 0;
		System.err.println("fib.java(7):     int g = 1; 			// (int g = 1) == void"); int g = 1;		
		System.err.println("fib.java(8):     for (int i = 0; i <= 15; i++)	// (int i = 0) == void"); for (int i = 0;
			 i <= for_test(i, 15); 
			 i++) {
		    System.err.println("fib.java(9)\u00d7" + i + ":   System.out.println(f); 	// (System.out.println(" + f + ")) == void"); System.out.println(f);
		    System.err.println("fib.java(10)\u00d7" + i + ":   f = f + g; 			// (" + (f + g) + " = " + f + " + " + g + ") == " + (f + g)); f = f + g;
		    System.err.println("fib.java(11)\u00d7" + i + ":  g = f - g;			// (" + (f - g) + " = " + f + " - " + g + ") == " + (f - g)); g = f - g; 
		    
		    System.err.println("fib.java(8)\u00d7" + i + ":   for (int i = 0; i <= 15; i++);// (" + i + "++) == " + (i + 1));
		}
	}

	private static int for_test(int i, int x) {
		System.err.println("fib.java(7)\u00d7" + i + ":   for (int i = 0; i <= 15; i++)	// (" + i + " <= 15) == " + (i <= x));
		return x;
	}
}
