What I wish is that all programming languages had a means of displaying what they are doing in real time.

What might that look like?

Consider what it might look like for this function:

```
int f = 0;
int g = 1;
for (int i = 0; i <= 15; i++) {
    System.out.println(f);
    f = f + g;
    g = f - g;
}
```

This function is implemented in `net.pflager.trace.fib.java`.

A sort of "instrumented" version of it is implemented in `net.pflager.trae.fib1.java`.



Bytecode manipulation seems like a good answer.

https://newrelic.com/blog/best-practices/diving-bytecode-manipulation-creating-audit-log-asm-javassist

https://www.baeldung.com/java-instrumentation

https://riptutorial.com/java/example/16046/javassist-basic

https://localcoder.org/trace-java-bytecode-stream

https://www.javassist.org/

https://blogs.oracle.com/javamagazine/post/runtime-code-generation-with-byte-buddy

https://www.javassist.org/tutorial/tutorial2.html#intro

https://asm.ow2.io/

https://devtut.github.io/java/bytecode-modification.html#what-is-bytecode

http://www.mikekohn.net/micro/java_grinder.php

Turns out to be rather difficult, because one almost has to be able to decompile, or be able to compile in real time.  For example, if I want to display something like the following:

```
fib.java(6):     int f = 0; 			// (int f = 0) == void
```

When the high-lighted line executes:

![1653175019977](/home/oy753c/Desktop/algorithms/Algorithms/src/net/pflager/trace/.md/README/1653175019977.png)

I have to have parsed enough to know that the variable `f` exists and where to retrieve it's value from in real-time during execution immediately after it has been executed.



So I briefly considered hacking the reference source code for jdb:

https://docs.oracle.com/javase/7/docs/technotes/guides/jpda/trace.html

https://docs.oracle.com/javase/7/docs/technotes/guides/jpda/examples.html

https://docs.oracle.com/javase/7/docs/technotes/guides/jpda/architecture.html#front-end

https://docs.oracle.com/en/java/javase/17/docs/specs/jpda/architecture.html#front-end

https://docs.oracle.com/en/java/javase/17/docs/api/jdk.jdi/com/sun/jdi/Method.html

`https://docs.oracle.com/javase/7/docs/technotes/tools/windows/jdb.html#:~:text=The%20Java%20Debugger%2C%20jdb%2C%20is,or%20remote%20Java%20Virtual%20Machine.`

I found the source code in the jdk17 in lib/src.zip as shown below (it's called `tty`, and its main class is `TTY.java`):

![1653175602665](/home/oy753c/Desktop/algorithms/Algorithms/src/net/pflager/trace/.md/README/1653175602665.png)

The other thing called `expr` is a kind of java expression parser.

I debugged both of them from eclipse to explore them.

I saved the launchers in Algorithms/.eclipse.launchers.  You can see them below.  They are the ones beginning with `com.sun.tools.example.debug.`

![1653175866154](/home/oy753c/Desktop/algorithms/Algorithms/src/net/pflager/trace/.md/README/1653175866154.png)



This led me right back to wondering how to instrument the code at compile time, rather than at runtime using bytecode manipulation.

I was pleased to discover:

https://openjdk.java.net/groups/compiler/

and more specifically

https://openjdk.java.net/groups/compiler/doc/hhgtjavac/index.html

I found a Baeldung example to demonstrate the idea of a compiler plugin:

https://www.baeldung.com/java-build-compiler-plugin

and I found another helpful article here:

https://www.javacodegeeks.com/2015/09/java-compiler-api.html

So some combination of these things might be the answer.

I think implementing the Baeldung example might be the thing to do next.

Of course, reading any of these things underscore the grotesque paucity of writing in English to convey meaning.

I forked the jd projects - java decompiler.  