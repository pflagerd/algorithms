package net.pflager.trace;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

import javassist.ByteArrayClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.MethodInfo;

public class Tracer {

	static class TraceClassTransformer implements ClassFileTransformer {
		private static final String METHOD_ANNOTATION = "net.pflager.trace.trace";

		private ClassPool pool;

		public TraceClassTransformer() {
			pool = ClassPool.getDefault();
		}

		public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
			try {
				pool.insertClassPath(new ByteArrayClassPath(className, classfileBuffer));

				CtClass cclass = pool.get(className.replaceAll("/", "."));
				if (!cclass.isFrozen()) {
					for (CtMethod currentMethod : cclass.getDeclaredMethods()) {
						if (hasTraceAnnotation(currentMethod)) {
							MethodInfo methodInfo = currentMethod.getMethodInfo();
							// List<AttributeInfo> list = methodInfo.getAttributes();
							CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
							CodeIterator i = codeAttribute.iterator();
							while (i.hasNext()) {
								int offset = i.next();
								int lineNumber = methodInfo.getLineNumber(offset);
								System.out.println(lineNumber + " " + offset);
							}
							currentMethod.insertBefore(createJavaString(currentMethod, className));
						}
					}
					return cclass.toBytecode();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		private boolean hasTraceAnnotation(CtMethod method) {
			MethodInfo mInfo = method.getMethodInfo();

			// the attribute we are looking for is a runtime invisible attribute
			// use Retention(RetentionPolicy.RUNTIME) on the annotation to make it
			// visible at runtime
			AnnotationsAttribute attInfo = (AnnotationsAttribute) mInfo.getAttribute(AnnotationsAttribute.invisibleTag);
			if (attInfo != null) {
				// this is the type name meaning use dots instead of slashes
				return attInfo.getAnnotation(METHOD_ANNOTATION) != null;
			}

			return false;
		}

		private String createJavaString(CtMethod currentMethod, String className) {
			StringBuilder sb = new StringBuilder();
			sb.append("{StringBuilder sb = new StringBuilder");
			sb.append("(\"A call was made to method '\");");
			sb.append("sb.append(\"");
			sb.append(currentMethod.getName());
			sb.append("\");sb.append(\"' on class '\");");
			sb.append("sb.append(\"");
			sb.append(className);
			sb.append("\");sb.append(\"'.\");");

			sb.append("System.out.println(sb.toString());}");
			return sb.toString();
		}
	}

	public static void premain(String agentArgs, Instrumentation inst) {
		System.out.println("Starting the agent");

		inst.addTransformer(new TraceClassTransformer());
	}

}
