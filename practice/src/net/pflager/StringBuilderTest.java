package net.pflager;

public class StringBuilderTest {
	public static void main(String[] args) {
		StringBuilder sb1 = new StringBuilder("catfox");
		StringBuilder sb2 = new StringBuilder();
		sb2.append("catf");
		sb2.append("ox");
		System.out.println("sb1 and sb2 are " + (sb1.equals(sb2) ? "" : "not ") + "equal");
	}

}
