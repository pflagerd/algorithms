package net.pflager;

public class FindDuplicatesUnderGivenConstraints {
	private static String toString(int[] array) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			if (i > 0) {
				stringBuilder.append(" ");
			}
			stringBuilder.append(array[i]);
		}
		return stringBuilder.toString();
	}

	public static void main(String[] args) {
		for (int j = 0; j < 6; j++) {
			int[] a = new int[10];
			
			for (int i = 0; i < 10; i++) {
				a[i] = i + 11;
			}
			
			for (int i = j; i < 5 + j; i++) {
				a[i] = 10;
			}

			System.out.println(toString(a));
		}
	}

}
