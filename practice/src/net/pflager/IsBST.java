package net.pflager;
public class IsBST {
	
	class Node {
		Node left;
		Node right;
		int value;
	}
	
	static int lastValue = Integer.MIN_VALUE;
	
	//
	// Precondition. No duplicate entries. System.out.println("HI");
	//
	//
	static boolean isBST(Node node) {
		if (node == null) {
			return true;
		}

		if (!isBST(node.left)) {
			return false;
		}
			
		// process this node
		System.out.println(node.value);
		if (node.value <= lastValue) {
			return false;
		}
		lastValue = node.value;
		
		if (!isBST(node.right)) {
			return false;
		}
		
		return true;
	}

}
