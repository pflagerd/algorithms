package net.pflager;

import java.util.Scanner;

class TicTacToe {
	protected char[] board;
	protected char userMarker;
	protected char aiMarker;
	protected char winner; // winner == null means no winner yet
	protected char currentMarker;
	
	public TicTacToe(char userMarker, char aiMarker, char currentMarker) {
		this.userMarker = userMarker;
		this.aiMarker = aiMarker;
		this.currentMarker = currentMarker;
		this.board = initializeBoard();
	}
	
	public static char[] initializeBoard() {
		char[] board = new char[9];
		for (int i = 0; i < board.length; i++) {
			board[i] = ' ';
		}
		return board;
	}
	
	public boolean playTurn(int spot) { // spot numbers from 1 (rather than 0)
		boolean isValid = withinRange(spot) && !isSpotTaken(spot);
		if (isValid) {
			board[spot - 1] = currentMarker;
			currentMarker = currentMarker == aiMarker ? userMarker : aiMarker;
		}
		return isValid;
	}

	private boolean isSpotTaken(int spot) {
		return board[spot - 1] != ' ';
	}

	private boolean withinRange(int spot) {		
		return 0 <= spot - 1 && spot - 1 < board.length;
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("| ").append(board[0]).append(" |").append(board[1]).append(" |").append(board[2]).append(" |\n");
		stringBuffer.append("-----------");
		stringBuffer.append("| ").append(board[3]).append(" |").append(board[4]).append(" |").append(board[5]).append(" |\n");
		stringBuffer.append("-----------");
		stringBuffer.append("| ").append(board[6]).append(" |").append(board[7]).append(" |").append(board[8]).append(" |\n");
		stringBuffer.append("-----------");
		return stringBuffer.toString();
	}
}

public class TicTacToeApplication {
	
	public static void main(String[] args) {
		String whoGoesFirst = "";
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.print("Who goes first User or Computer? (U or C) ");
			scanner.next();
		} while (whoGoesFirst.toUpperCase().trim().charAt(0) != 'U' && whoGoesFirst.toUpperCase().trim().charAt(0) != 'C');
		
		//TicTacToe ticTacToe = whoGoesFirst.toUpperCase().trim().charAt(0) == 'U' ? new TicTacToe('X', 'O', 'X') : new TicTacToe('X', 'O', 'O');
		
		scanner.close();
	}

}

