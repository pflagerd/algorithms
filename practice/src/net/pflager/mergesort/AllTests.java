package net.pflager.mergesort;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class AllTests extends MergeSort {

	class ConsoleAndFileOutputStream extends OutputStream {
		private OutputStream console = System.out;
		private OutputStream fileOutputStream;

		public ConsoleAndFileOutputStream(String fileName) throws FileNotFoundException {
			fileOutputStream = new FileOutputStream(new File(fileName));
		}

		@Override
		public void close() throws IOException {
			fileOutputStream.close();
		}

		@Override
		public void write(int arg0) throws IOException {
			console.write(arg0);
			fileOutputStream.write(arg0);
		}
	}

	class ConsoleAndByteArrayOutputStream extends ByteArrayOutputStream {
		private OutputStream console = System.out;

		@Override
		public void close() throws IOException {
			super.close();
		}

		@Override
		public void write(int arg0) {
			try {
				console.write(arg0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.write(arg0);
		}
	}

	public static int countSpaces(String string) {
		int count = 0;
		int length = string.length();
		for (int i = 0; i < length; i++) {
			if (string.charAt(i) == ' ')
				count++;
		}
		return count;
	}

	@Test
	public void functionalTest() throws IOException, XPathExpressionException {
		PrintStream console = System.out;

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			try {
				Document document = builder.parse(new FileInputStream(
						"test/" + getClass().getPackage().getName().replaceAll("\\.", "/") + "/testdata.xml"));

				XPathFactory xpathfactory = XPathFactory.newInstance();
				XPath xpath = xpathfactory.newXPath();

				XPathExpression expr = xpath.compile("//testdata/testcase");
				NodeList testCaseNodeList = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
				for (int i = 0; i < testCaseNodeList.getLength(); i++) {
					StringBuilder rawTestData = new StringBuilder();
					StringBuilder rawTestResults = new StringBuilder();

					NodeList testCaseChildNodeList = testCaseNodeList.item(i).getChildNodes();
					String input = null;
					String output = null;
					for (int j = 0; j < testCaseChildNodeList.getLength(); j++) {
						Node testCaseChildNode = testCaseChildNodeList.item(j);
						if (testCaseChildNode.getNodeType() == Node.ELEMENT_NODE) {
							if (testCaseChildNode.getNodeName().contentEquals("input"))
								input = testCaseChildNode.getTextContent().trim();
							else if (testCaseChildNode.getNodeName().contentEquals("expected_output"))
								output = testCaseChildNode.getTextContent().trim();
						}
					}

					String[] ins = input.split("\n");
					for (String in : ins) {
						in = in.strip();
						rawTestData.append(in + "\n");
					}

					String[] outs = output.split("\n");
					for (String out : outs) {
						out = out.strip();
						rawTestResults.append(out + "\n");
					}

					ConsoleAndByteArrayOutputStream consoleAndByteArrayOutputStream = new ConsoleAndByteArrayOutputStream();
					try (InputStream inputStream = new ByteArrayInputStream(rawTestData.toString().getBytes())) {
						try (PrintStream printStream = new PrintStream(consoleAndByteArrayOutputStream)) {
							System.setIn(inputStream);
							System.setOut(printStream);
							main(new String[0]);
							System.setOut(console);
						}
					}

					try (BufferedReader rawTestResultsData = new BufferedReader(
							new InputStreamReader(new ByteArrayInputStream(rawTestResults.toString().getBytes())))) {
						try (BufferedReader rawOutputData = new BufferedReader(new InputStreamReader(
								new ByteArrayInputStream(consoleAndByteArrayOutputStream.toString().getBytes())))) {
							for (String rawOutputDataString = rawOutputData.readLine(),
									rawTestResultsString = rawTestResultsData.readLine(); rawOutputDataString != null
											&& rawTestResultsString != null; rawOutputDataString = rawOutputData
													.readLine(), rawTestResultsString = rawTestResultsData.readLine()) {
								assertEquals(rawTestResultsString, rawOutputDataString);
							}
						}
					}

					if (System.getProperty("compute.performance") != null) {
						long elapsedTime = 0;
						for (int j = 0; j < 1_000_000; j++) {
							consoleAndByteArrayOutputStream = new ConsoleAndByteArrayOutputStream();
							try (InputStream inputStream = new ByteArrayInputStream(
									rawTestData.toString().getBytes())) {
								try (PrintStream printStream = new PrintStream(consoleAndByteArrayOutputStream)) {
									System.setIn(inputStream);
									System.setOut(printStream);
									long startTime = System.currentTimeMillis();
									main(new String[0]);
									elapsedTime += System.currentTimeMillis() - startTime;
									System.setOut(console);
								}
							}
						}
						System.out.println("Elapsed time == " + elapsedTime / 1000.0 + " s");
					}
				}
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
}