package net.pflager.mergesort;

import java.util.Arrays;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class MergeSort {
	public static <T extends Comparable<T>> void mergesort(T[] a) {
//		mergesort(a, 0, a.length);
		if (a.length == 1) {
			return;
		}
		
		if (a.length == 2) {
			if (less(a[1], a[0])) {
				swap(a, 0, 1);
				return;
			}
		}
		T[] left = Arrays.copyOfRange(a, 0, a.length / 2);
		T[] right = Arrays.copyOfRange(a, a.length / 2, a.length);
		mergesort(left);
		System.err.println(Arrays.toString(left));
		mergesort(right);
		System.err.println(Arrays.toString(right));
		T[] merged = merge(left, right);
		System.arraycopy(merged, 0, a, 0, merged.length);
		System.err.println(Arrays.toString(merged) + "\n");
	}
	
	public static <T extends Comparable<T>> T[] merge(T[] left, T[] right) {
		T[] temp = Arrays.copyOf(left, left.length + right.length);
		int indexTemp = 0;
		int indexLeft = 0, indexRight = 0;
		while (indexTemp < temp.length) {
			if (indexLeft >= left.length)
				temp[indexTemp++] = right[indexRight++];
			else if (indexRight >= right.length)
				temp[indexTemp++] = left[indexLeft++];
			else if (less(left[indexLeft], right[indexRight]))
				temp[indexTemp++] = left[indexLeft++];
			else
				temp[indexTemp++] = right[indexRight++];
		}
		return temp;
	}

	private static <T extends Comparable<T>> boolean less(T v, T w) {
		return v.compareTo(w) < 0;
	}

	private static <T extends Comparable<T>> void swap(T[] a, int i, int j) {
		T t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static <T extends Comparable<T>> void show(T[] a) { // Print the array, on a single line.
		for (int i = 0; i < a.length; i++) {
			if (i != 0)
				StdOut.print(" ");
			StdOut.print(a[i]);
		}
		StdOut.println();
	}

	public static <T extends Comparable<T>> boolean isSorted(T[] a) { // Test whether the array entries are in order.
		for (int i = 1; i < a.length; i++)
			if (less(a[i], a[i - 1]))
				return false;
		return true;
	}

	public static void main(String[] args) { // Read strings from standard input, sort them, and print.
		String[] a = new In().readAllStrings();
		Arrays.stream(a).mapToInt(String::length).toArray();
		mergesort(a);
		show(a);
	}
}
