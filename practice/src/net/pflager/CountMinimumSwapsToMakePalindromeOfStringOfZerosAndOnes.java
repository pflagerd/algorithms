package net.pflager;

import java.util.Random;

/*
 * You are given a binary string, s, consisting of characters '0' and '1'. 
 * 
 * Transform this string into a palindrome by performing swap operations.
 * 
 * Determine the minimum number of swaps required to make the string a
 * palindrome.
 * 
 * If it is impossible to do so, then return -1.
 * 
 * 1 \le |s| \le 2 \times 10^5
 * 
 */
public class CountMinimumSwapsToMakePalindromeOfStringOfZerosAndOnes {
	
	//
	// for x, y, z \in { 0  1 }
	//
	// "" is it a palindrome?
	// "x" is always a palindrome (x = 0 or x = 1)
	// "xy" is a palindrome only if x = y
	// "xyz" is a palindrome only if x = z
	//   If x != z, can turn it into a palindrome no matter whether y is 0 or 1
	// "wxyz" 
	//
	public static int minSwapsRequired(String s) {
		return 0;
	}

	public static void main(String[] args) {
		int length = new Random().nextInt(2) + 1;
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			stringBuilder.append((char)(new Random().nextInt(2) + '0'));
		}
		System.out.println(stringBuilder.toString());
	}
	
	

}
