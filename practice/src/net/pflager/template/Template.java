package net.pflager.template;

import java.util.Arrays;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Template {
	public static <T> void sort(Comparable<T>[] a) {
		Arrays.sort(a);
	}

	private static <T extends Comparable<T>> boolean less(T v, T w) {
		return v.compareTo(w) < 0;
	}

//	private static <T extends Comparable<T>> void exch(T[] a, int i, int j) {
//		T t = a[i];
//		a[i] = a[j];
//		a[j] = t;
//	}

	private static <T> void show(Comparable<T>[] a) { // Print the array, on a single line.
		for (int i = 0; i < a.length; i++) {
			if (i != 0)
				StdOut.print(" ");
			StdOut.print(a[i]);
		}
		StdOut.println();
	}

	public static <T extends Comparable<T>> boolean isSorted(T[] a) { // Test whether the array entries are in order.
		for (int i = 1; i < a.length; i++)
			if (less(a[i], a[i - 1]))
				return false;
		return true;
	}

	public static void main(String[] args) { // Read strings from standard input, sort them, and print.
		String[] a = new In().readAllStrings();
		sort(a);
		show(a);
	}
}
