package net.pflager;

public class GreatestCommonDivisor {
	
	public static int gcd(int m, int n) {
		if (m < n) {
			int tmp = n; n = m; m = tmp;
		}
		
		int r;
		while ((r = m % n) != 0) {
			m = n; n = r;
		}
		return n;
	}
	
	public static void main(String[] args) {
		System.out.println(gcd(119, 544));

		System.out.println(gcd(2, 7));
	}

}
