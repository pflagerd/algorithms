package net.pflager;
class Solution {
	public int removeDuplicates(int[] nums) {
		// [0,0,1,1,1,2,2,3,3,4] (10)
		// [0,1,2,3,4,2,2,3,3,4] (10)
		// ^ ^
		// [1]

		int indexOfNextArrayElementToOverwrite = 0; // 1 2 3
		int state = 0; // See state definitions in switch cases below. // 0 1 2 3 2 3 2 3 2

		int i = 0;
		for (; i < nums.length - 1; i++) { // i: 0 1 2 3 4 5 6 7 8 9
			switch (state) {
			case 0: // 0: look for duplicates, no shift
				if (nums[i] == nums[i + 1]) {
					indexOfNextArrayElementToOverwrite = i + 1;
					state = 1;
				}
				break;

			case 1: // 1: look for non-duplicate, no shift
				if (nums[i] != nums[i + 1]) {
					state = 2;
					nums[indexOfNextArrayElementToOverwrite++] = nums[i + 1];
				}
				break;

			case 2: // 2: look for duplicates, shift
				if (nums[i] == nums[i + 1]) {
					state = 3;
				}
				break;

			case 3: // 3: look for non-duplicate, shift
				if (nums[i] != nums[i + 1]) {
					state = 2;
					nums[indexOfNextArrayElementToOverwrite++] = nums[i + 1];
				}
				break;

			default: // don't expect to reach it.
				break;
			}
		}

		return indexOfNextArrayElementToOverwrite;
	}

	static public void main(String[] args) {
		int[] array = new int[] { 1, 2, 3 };
		int length = new Solution().removeDuplicates(array);
		StringBuilder sb = new StringBuilder("length == ");
		sb.append(length);
		sb.append(", array == [");

		for (int i = 0; i < length; i++) {
			if (i != 0) {
				sb.append(" ");
			}

			sb.append(array[i]);
		}
		sb.append("]");
		System.out.println(sb.toString());
	}

}
