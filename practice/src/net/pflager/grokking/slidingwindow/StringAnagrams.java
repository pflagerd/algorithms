package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

/*
 * Given a string and a pattern, find all anagrams of the pattern in the given string.
 */
public class StringAnagrams {
	public static List<Integer> findStringAnagrams(String str, String pattern) {
		if (str == null || pattern == null)
			throw new IllegalArgumentException("str and pattern must be non-null");

		final int xLength = str.length();
		if (xLength == 0)
			throw new IllegalArgumentException("str and pattern must be non-empty");
			
		List<Integer> list = new ArrayList<>();
			
		final int yLength = pattern.length();
		if (yLength == 0) {
			for (int i = 0; i < xLength + 1; i++)
				list.add(i);
			return list;
		}

		str = str.toUpperCase();
		pattern = pattern.toUpperCase();

		// for (int i = 0; i < xLength; i++) {
			
		// 	if (!Character.isUpperCase(str.charAt(i))) {
		// 		throw new IllegalArgumentException("str must contain uppercase characters only");
		// 	}
		// }	
		
		// for (int i = 0; i < yLength; i++) {
		// 	if (!Character.isUpperCase(pattern.charAt(i))) {
		// 		throw new IllegalArgumentException("pattern must contain uppercase characters only");
		// 	}
		// }	
		
		if (xLength < yLength)
			throw new IllegalArgumentException("str must not be shorter than pattern");
			
		int[] metricY = new int[26];
		Arrays.fill(metricY, 0);
		for (int i = 0; i < yLength; i++) {
			int charIndex = pattern.charAt(i) - 'A';
			metricY[charIndex]++;
		}
		
		int[] metricSubstringX = new int[26];
		Arrays.fill(metricSubstringX, 0);
		
		int head = 0;
		for (; head < yLength; head++) {
			int charIndex = str.charAt(head) - 'A';
			metricSubstringX[charIndex]++;
		}
		
		if (Arrays.equals(metricSubstringX, metricY))
			list.add(0);
		
		for (int tail = 0; head < xLength; head++, tail++) {
			int charIndex = str.charAt(head) - 'A';
			metricSubstringX[charIndex]++;
			charIndex = str.charAt(tail) - 'A';
			metricSubstringX[charIndex]--;
			if (Arrays.equals(metricSubstringX, metricY))
				list.add(tail + 1);
		
		}
		
		return list;
	}
 
	@Test
	void test() {
		assertArrayEquals(new Integer[] {1, 2}, findStringAnagrams("ppqp", "pq").toArray(new Integer[0]));
		assertArrayEquals(new Integer[] {2, 3, 4}, findStringAnagrams("abbcabc", "abc").toArray(new Integer[0]));
		assertArrayEquals(new Integer[] {0, 1, 5, 6}, findStringAnagrams("xyzxwxzyx", "xyz").toArray(new Integer[0]));
	}

}
