package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string, find the length of the longest substring in it with no more than K distinct characters.
 * 
 * @time 32m
 */
public class LongestSubstringKDistinct {
	public static int findLength(String str, int k) {
		if (str == null)
			throw new IllegalArgumentException("str cannot be null");
		
		if (str.length() == 0) // Maybe come back to this
			throw new IllegalArgumentException("str must have something in it");
		
		if (k <= 0) // Maybe come back to this
			throw new IllegalArgumentException("k must not be less than or equal to zero");
		
		int[] map = new int[26];
		for (int i = 0; i < map.length; i++) {
			map[i] = 0;
		}
		
		int maxLength = -1;
		for (int lo = 0, hi = 0, distinct = 0, strLen = str.length(); hi < strLen; hi++) {
			if (map[str.charAt(hi) - 'a'] == 0) {
				distinct++;
			}
			map[str.charAt(hi) - 'a']++;
			
			while (distinct > k) {
				if (map[str.charAt(lo) - 'a'] == 1) {
					distinct--;
				}
				map[str.charAt(lo) - 'a']--;
				lo++;
			}

			int len = hi - lo + 1;
			if (len > maxLength)
				maxLength = len;
			
		}
		
		return maxLength;
	}
 
	@Test
	void test() {
		assertEquals(4, findLength("araaci", 2));
		assertEquals(2, findLength("araaci", 1));
		assertEquals(5, findLength("cbbebi", 3));
		assertEquals(6, findLength("cbbebi", 10));
	}

}
