package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string, find the length of the longest substring which has all distinct characters.
 * 
 * @time 21:40
 */
public class LongestSubstringAllDistinct {
	public static int findLength(String str) {
		if (str == null)
			throw new IllegalArgumentException("str must be non-null");
		
		if (str.length() == 0)
			throw new IllegalArgumentException("str must contain something");
		
		// Could check for lowercase alpha only.
		for (int i = 0, strlen = str.length(); i < strlen; i++) {
			char c = str.charAt(i);
			if (c < 'a' || c > 'z')
				throw new IllegalArgumentException("str must contain only letter a-z");
		}
		
		int[] characterCounts = new int[26];
		for (int i = 0; i < characterCounts.length ; i++) {
			characterCounts[i] = 0;			
		}
		
		int longestLength = -1;
		int duplicates = 0;
		for (int lo = 0, hi = 0, strlen = str.length(); hi < strlen; hi++) {
			if (characterCounts[str.charAt(hi) - 'a'] >= 1) {
					duplicates++;
			}
			characterCounts[str.charAt(hi) - 'a']++;
			
			while (duplicates != 0) {
				if (characterCounts[str.charAt(lo) - 'a'] > 1) {
					duplicates--;
				}
				characterCounts[str.charAt(lo) - 'a']--;
				lo++;
			}
			
			int len = hi - lo + 1;
			if (longestLength < len)
				longestLength = len;
		}		
		
		return longestLength;
	}
 
	@Test
	void test() {
		assertEquals(3, findLength("aabccbb"));
		assertEquals(2, findLength("abbbb"));
		assertEquals(3, findLength("abccde"));
	}

}
