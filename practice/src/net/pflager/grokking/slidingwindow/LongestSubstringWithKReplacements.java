package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string with lowercase letters only,
 * 
 * if you are allowed to replace no more than k letters with any letter, 
 * find the length of the longest substring having the same letters after replacement.
 */
public class LongestSubstringWithKReplacements {
	public static int findLength(String str, int k) {
		if (str == null)
			throw new IllegalArgumentException("str must not be null");
		
		if (k <= 0)
			throw new IllegalArgumentException("k must be greater than zero");
		
		int[] charCounts = new int[128];
		
		int maxCharCount = -1;
		int replacements = 0;
		for (int currentCharCount = 0, tail = 0, head = 0, strLength = str.length(); head < strLength; head++) {
			currentCharCount = ++charCounts[str.charAt(head)];
			if (currentCharCount > maxCharCount) {
				maxCharCount = currentCharCount;
				continue;
			}
			
			if (replacements < k) {
				replacements++;
			} else {
				charCounts[str.charAt(tail)]--;
				tail++;				
			}			
		}		
		
		return maxCharCount + replacements;
	}
 
	@Test
	void test() {
		assertEquals(4, findLength("AABABBA", 1));
		assertEquals(5, findLength("aabccbb", 2));
		assertEquals(2, findLength("yjnwf", 1));
		assertEquals(4, findLength("abab", 2));
		assertEquals(4, findLength("abbcb", 1));
		assertEquals(3, findLength("abccde", 1));
	}

}
