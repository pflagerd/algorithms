package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class MinSizeSubArraySum {

	public static int findMinSubArray(int x, int[] a) {
		int head = 0;
		int minLength = Integer.MAX_VALUE;
		String state = "Accumulating sum greater than x";
		int sum = 0;
		int tail = 0;

		while (true) {
			switch (state) {
				case "Accumulating sum greater than x":
					if (head >= a.length)
						return minLength;

					sum += a[head++];
					if (sum < x) {
					} else {
						state = "Excumulating sum greater than x";
					}
					break;

				case "Excumulating sum greater than x":
					sum -= a[tail++];
					if (sum < x) {
						state = "Accumulating sum greater than x";
					} else {
						int length = head - tail;
						if (length < minLength)
							minLength = length;
					}
					break;

				default:
					break;
			}
		}
	}

	public static int findMinSubArrayB(int S, int[] arr) {
		int minSize = Integer.MAX_VALUE;
		for (int lo = 0, hi = -1, sum = 0;;) {
			if (sum < S) {
				if (++hi >= arr.length)
					break;
				sum += arr[hi];
			} else {
				int len = hi - lo + 1;
				if (len < minSize)
					minSize = len;
				sum -= arr[lo++];
			}
		}

		return minSize;
	}

	@Test
	void test() {
		assertEquals(2, findMinSubArray(7, new int[] { 2, 1, 5, 2, 3, 2 }));
		assertEquals(1, findMinSubArray(7, new int[] { 2, 1, 5, 2, 8 }));
		assertEquals(3, findMinSubArray(8, new int[] { 3, 4, 1, 1, 6 }));
	}

}
