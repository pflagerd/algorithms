package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class LongestSubstringWithOneDistinctCharacter {
	public static int find(String s) {
		int charCounts[] = new int[128];
		int maxSize = -1;
		for (int lo = 0, hi = -1, sLength = s.length(), unique = 0;;) {
			if (unique <= 1) {
				if (++hi == sLength) {
					if (unique == 1) {
						int len = hi - lo;
						if (len > maxSize)
							maxSize = len;
					}
					break;
				}
				if (charCounts[s.charAt(hi)]++ == 0)
					unique++;
			} else {
				int len = hi - lo;
				if (len > maxSize)
					maxSize = len;
				if (charCounts[s.charAt(lo++)]-- == 1)
					unique--;
			}
		}
		return maxSize;
	}

	@Test
	void test() {
		assertEquals(3, find("absaabbsjeksjaabbbksjdk"));
		assertEquals(4, find("absaabbsjeksjaabbbb"));
		assertEquals(3, find("aaabsaabbsjeksjaabbksjdk"));
	}

}
