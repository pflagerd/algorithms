package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MaxSumSubArrayOfSizeK {
	public static int findMaxSumSubArray(int k, int[] arr) {
		if (arr == null)
			throw new IllegalArgumentException("arr must not be null");
		
		if (k <= 0)
			throw new IllegalArgumentException("k must be greater than zero");
		
		if (k > arr.length)
			throw new IllegalArgumentException("k must be less than or equal to arr.length.  i.e. <= " + arr.length);

		int sum = 0;
		for (int i = 0; i < k; i++) {
			sum += arr[i];
		}
		int maxSum = sum;
		
		for (int j = 0, i = k; i < arr.length; i++, j++) {
			sum = sum + arr[i] - arr[j];
			if (sum > maxSum)
				maxSum = sum;
		}
				
		return maxSum;
	}
 
	@Test
	void test() {
		assertEquals(9, findMaxSumSubArray(3, new int[] { 5, 1, 3 }));
		assertEquals(7, findMaxSumSubArray(2, new int[] { 2, 3, 4, 1, 5 }));
	}

}
