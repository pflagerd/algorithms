package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given a string and a pattern, find the smallest substring in the 
 * given string which has all the character occurrences of the given pattern.
 */
public class MinimumWindowSubstring {
	
	public static String toString(int[] thing) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < thing.length; i++) {
			if (thing[i] != 0) {
				if (stringBuilder.length() != 0)
					stringBuilder.append(", ");
				stringBuilder.append("'" + (char)i + "'=" + thing[i]);
			}
		}
		
		return stringBuilder.toString();
	}

	/*
	 * Let's start with the simple case of requiring that the substring be
	 * contiguous.
	 */
	public static String findString(String string, String pattern) {
		if (string == null || pattern == null)
			throw new IllegalArgumentException("string and pattern must be non-null");

		int sLength = string.length();
		if (sLength == 0)
			throw new IllegalArgumentException("string must be non-empty");

		int pLength = pattern.length();
		if (pLength == 0)
			throw new IllegalArgumentException("pattern must be non-empty");

		int pCharCounts[] = new int[128];
		for (int i = 0; i < pLength; i++) {
				pCharCounts[pattern.charAt(i)]++;
		}
		
		int[] sCharCounts = new int[128];

		String substring = string;
		for (int head = 0, tail = 0, nMatched = 0; head < sLength; head++) {
			char c = string.charAt(head);
			++sCharCounts[c];
			if (pCharCounts[c] > 0 && sCharCounts[c] <= pCharCounts[c])
				++nMatched;

			while (nMatched == pLength) {
				if (head - tail + 1 < substring.length())
					substring = string.substring(tail, head + 1);

				c = string.charAt(tail);				
				--sCharCounts[c];
				if (pCharCounts[c] > 0 && sCharCounts[c] < pCharCounts[c])
					--nMatched;
				tail++;
			}
		}

		return substring;
	}

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;

		exec = () -> assertEquals("", findString("a", "aa"));
		testName = "assertEquals(\"\", findString(\"a\", \"aa\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("BANC", findString("ADOBECODEBANC", "ABC"));
		testName = "assertEquals(\"BANC\", findString(\"ADOBECODEBANC\", \"ABC\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("abbbbbcdd", findString("aaaaaaaaaaaabbbbbcdd", "abcdd"));
		testName = "assertEquals(\"abbbbbcdd\", findString(\"aaaaaaaaaaaabbbbbcdd\", \"abcdd\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("toprac", findString("timetopractice", "toc"));
		testName = "assertEquals(\"toprac\", findString(\"timetopractice\", \"toc\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("cwae", findString("cabwefgewcwaefgcf", "cae"));
		testName = "assertEquals(\"cwae\", findString(\"cabwefgewcwaefgcf\", \"cae\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("abdec", findString("aabdec", "abc"));
		testName = "assertEquals(\"abdec\", findString(\"aabdec\", \"abc\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("", findString("adcad", "abc"));
		testName = "assertEquals(\"\", findString(\"adcad\", \"abc\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("bca", findString("abdbca", "abc"));
		testName = "assertEquals(\"bca\", findString(\"abdbca\", \"abc\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("apzo", findString("zoomlazapzo", "oza"));
		testName = "assertEquals(\"apzo\", findString(\"zoomlazapzo\", \"oza\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals("bca", findString("abdbca", "abc"));
		testName = "assertEquals(\"bca\", findString(\"abdbca\", \"abc\"))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
