package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;


/**
 * Given an array, find the average of all subarrays of ‘K’ contiguous elements in it.
 * 
 * @author oy753c
 * @time < 8m
 *
 */
class AverageOfSubarrayOfSizeK {
	public static double[] findAverages(int K, int[] arr) {
		if (arr == null)
			throw new IllegalArgumentException("arr must not be null");
		
		if (K <= 0)
			throw new IllegalArgumentException("K must be greater than zero");
		
		if (K > arr.length)
			throw new IllegalArgumentException("K must be less than or equal to arr.length.  i.e. <= " + arr.length);
		
		double sum = 0.0;
		for (int i = 0; i < K; i++) {
			sum += arr[i];
		}		

		double[] averagesOfSubarrays = new double[arr.length - K + 1];
		averagesOfSubarrays[0] = sum / K;
		
		for (int i = K, j = 0; i < arr.length; i++, j++) {
			sum = sum + arr[i] - arr[j];
			averagesOfSubarrays[j + 1] = sum / K;
		}		
		
		return averagesOfSubarrays;
	}

	@Test
	void test() {
		assertArrayEquals(new double[] { 2.2, 2.8, 2.4, 3.6, 2.8 }, AverageOfSubarrayOfSizeK.findAverages(5, new int[] { 1, 3, 2, 6, -1, 4, 1, 8, 2 }), Math.ulp(1));
	}
}