package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * You are given a string s and an array of strings words. All the strings of words are of the same length.
 * 
 * A concatenated substring in s is a substring that contains all the strings of any permutation of <code>words</code> concatenated.
 * 
 * For example, if words = ["ab","cd","ef"], then "abcdef", "abefcd", "cdabef", "cdefab", "efabcd", and "efcdab" are all concatenated strings. 
 * "acdbef" is not a concatenated substring because it is not the concatenation of any permutation of words.
 * 
 * Return the starting indices of all the concatenated substrings in s. You can return the answer in any order.
 */
public class WordConcatenation {
  private static HashMap<String, Integer> createHashMapFromStringArray(String[] words) {
    HashMap<String, Integer> hashMap = new HashMap<>();
    for (String word : words) {
    	Integer value = hashMap.get(word);
    	if (value == null)
    		hashMap.put(word, 1);
    	else
    		hashMap.put(word, value.intValue() + 1);
    }
    return hashMap;
	}
	
	
  public static List<Integer> findSubstring(String string, String[] words) {
  	if (string == null)
  		throw new IllegalArgumentException("string must be non-null");
  	
  	int stringLength = string.length();
  	if (stringLength == 0)
  		throw new IllegalArgumentException("string must be non-empty");
  	
  	if (words == null)
  		throw new IllegalArgumentException("words[] must be non-null");
  	
  	if (words.length == 0)
  		throw new IllegalArgumentException("words[] must be non-empty");
  	
  	int wordLength = words[0].length();
  	if (wordLength == 0)
  		throw new IllegalArgumentException("each String in words[] must be non-empty");

    List<Integer> resultIndices = new ArrayList<Integer>();
    
  	if (stringLength < words.length * wordLength)
  		return resultIndices;  	
  	
    HashMap<String, Integer> hashMap = createHashMapFromStringArray(words);
    
    Integer wordCount;
    String substring;
    int head = 0, tail = 0;
    A: for (;;) {
	    for (;; head++) {
	    	if (head + wordLength > stringLength)
	    		return resultIndices;
	    	substring = string.substring(head, head + wordLength);
	    	wordCount = hashMap.get(substring);
	    	if (wordCount != null)
	    		break;
	    }
	    tail = head;
	    
	    if (wordCount.intValue() == 1)
	    	hashMap.remove(substring);
	    else
	    	hashMap.put(substring, wordCount.intValue() - 1);

        if (hashMap.size() == 0)  { // We've matched!
            resultIndices.add(tail);
            hashMap = createHashMapFromStringArray(words);
            head = tail + 1;
            continue A;
        }
        
	    for(;;) {
	    	head += wordLength;
	      // head is at the start of next possible word
	    	if (head + wordLength > stringLength)
	    		return resultIndices;
	    	substring = string.substring(head, head + wordLength);
	    	wordCount = hashMap.get(substring);
	    	if (wordCount == null) {
	    		hashMap = createHashMapFromStringArray(words);
		    	head = tail + 1;
	    		continue A;
	    	}
	    	
		    if (wordCount.intValue() == 1)
		    	hashMap.remove(substring);
		    else
		    	hashMap.put(substring, wordCount.intValue() - 1);
		    
		    if (hashMap.size() == 0)  { // We've matched!
		    	resultIndices.add(tail);
	    		hashMap = createHashMapFromStringArray(words);
		    	head = tail + 1;
		    	break;
		    }
	    }
    }
 	}

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 1, 4 }), findSubstring("mississippi", new String[] { "is" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {0}), findSubstring(\"mississippi\", new String[] { \"is\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }), findSubstring("aaaaaaaaaaaaaa", new String[] { "aa","aa" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {0}), findSubstring(\"aaaaaaaaaaaaaa\", new String[] { \"aa\",\"aa\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 0 }), findSubstring("a", new String[] { "a" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {0}), findSubstring(\"a\", new String[] {\"a\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 8 }),
				findSubstring("wordgoodgoodgoodbestword", new String[] { "word", "good", "best", "good" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {8}), findSubstring(\"wordgoodgoodgoodbestword\", new String[] {\"word\",\"good\",\"best\",\"good\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 0, 3 }),
				findSubstring("catfoxcat", new String[] { "cat", "fox" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {0, 3}), findSubstring(\"catfoxcat\", new String[] { \"cat\", \"fox\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] { 3 }),
				findSubstring("catcatfoxfox", new String[] { "cat", "fox" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {3}), findSubstring(\"catcatfoxfox\", new String[] { \"cat\", \"fox\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}

}
