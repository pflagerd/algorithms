package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given an array containing 0s and 1s, 
 * if you are allowed to replace no more than ‘k’ 0s with 1s, 
 * find the length of the longest contiguous subarray having all 1s.
 */
public class ReplacingOnes {
	public static int findLength(int[] arr, int k) {
		if (arr == null)
			throw new IllegalArgumentException("arr must be non-null");

		if (k < 0)
			throw new IllegalArgumentException("k must be non-negative");

		int maxLength = -1;
		for (int head = 0, tail = 0, replacements = 0; ; head++) {
			if (head >= arr.length) {
				if (head - tail > maxLength)
					maxLength = head - tail;
				break;
			}
			if (arr[head] != 0) {
				continue;
			} else {
				if (replacements < k) {
					replacements++;
				} else {
					while (arr[tail] == 1 && tail != head)
						tail++;
					if (arr[tail] == 0) {
						tail++;
						if (head - tail + 1 > maxLength)
							maxLength = head - tail + 1;
					}
				}
			}
		}

		return maxLength;
	}

	@Test
	void test() {
		assertEquals(6, findLength(new int[] { 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 }, 2));
		assertEquals(9, findLength(new int[] { 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1 }, 3));
	}

}
