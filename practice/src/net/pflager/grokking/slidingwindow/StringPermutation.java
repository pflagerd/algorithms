package net.pflager.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/*
 * Given a string and a pattern, find out if the string contains any permutation of the pattern.
 * 
 * Permutation is defined as the re-arranging of the characters of the string. 
 * 
 * For example, “abc” has the following six permutations:
 * 
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * 
 * If a string has ‘n’ distinct characters, it will have n! permutations. 
 */
public class StringPermutation {
	public static boolean findPermutation(String str, String pattern) {
		if (str == null || pattern == null)
			throw new IllegalArgumentException("str and pattern must be non-null");

		int strLength = str.length();
		int patternLength = pattern.length();
		if (strLength == 0 || patternLength == 0)
			throw new IllegalArgumentException("str and pattern must be non-empty");
		
		if (strLength < patternLength)
			throw new IllegalArgumentException("pattern must be shorter or equal in length to str");
		
		// str must contain only lowerCase
		for (int i = 0; i < strLength; i++) {
			if (!Character.isLowerCase(str.charAt(i)))
				throw new IllegalArgumentException("str must contain only lowercase characters");
		}
		
		// pattern must contain only lowerCase
		for (int i = 0; i < patternLength; i++) {
			if (!Character.isLowerCase(pattern.charAt(i)))
				throw new IllegalArgumentException("pattern must contain only lowercase characters");
		}
		
		// compute "metric" for pattern
		int[] patternMetric = new int[26];
		Arrays.fill(patternMetric, 0);
		
		for (int i = 0; i < patternLength; i++) {
			int charIndex = pattern.charAt(i) - 'a';
			patternMetric[charIndex]++;
		}		
		
		int[] currentSubstringMetric = new int[26];
		Arrays.fill(currentSubstringMetric, 0);
		
		int head = 0;
		for (; head < patternLength; head++) {
			int charIndex = str.charAt(head) - 'a';
			currentSubstringMetric[charIndex]++;
		}
		if (Arrays.equals(currentSubstringMetric, patternMetric))
			return true;
		
		for (int tail = 0; head < strLength; head++, tail++) {
			int charIndex = str.charAt(head) - 'a';
			currentSubstringMetric[charIndex]++;
			charIndex = str.charAt(tail) - 'a';
			currentSubstringMetric[charIndex]--;			
			
			if (Arrays.equals(currentSubstringMetric, patternMetric))
				return true;			
		}
		
		return false;
	}
 
	@Test
	void test() {
		assertEquals(true, findPermutation("oidbcaf", "abc"));
		assertEquals(false, findPermutation("odicf", "dc"));
		assertEquals(true, findPermutation("bcdxabcdy", "bcdyabcdx"));
		assertEquals(true, findPermutation("aaacb", "abc"));
	}

}
