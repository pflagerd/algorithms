package net.pflager.grokking.twopointers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class RemoveDuplicates {
	public static int remove(int[] arr) {
		if (arr == null)
			return 0;
		
		if (arr.length == 0)
			return 0;
		
		if (arr.length == 1)
			return 1;

		int head = 0;
		int tail = 0;
		A:
		while (head + 1 < arr.length) {
			while (arr[head] == arr[head + 1]) {
				head++;
				if (head + 1 == arr.length)
					break A;
			}
			
			if (head != tail) {
				arr[++tail] = arr[++head];
				continue;
			}
		
			tail++;
			head++;
		}

		return tail + 1;
	}

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;

		exec = () -> assertEquals(4, remove(new int[] { 2, 3, 3, 3, 6, 9, 9 }));
		testName = "assertEquals(4, remove(new int[] { 2, 3, 3, 3, 6, 9, 9 })))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(2, remove(new int[] { 2, 2, 2, 11 }));
		testName = "assertEquals(2, remove(new int[] { 2, 2, 2, 11 }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
