package net.pflager.grokking.twopointers;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given an array of sorted numbers and a target sum, find a pair in the array whose sum is equal to the given target.
 * 
 * Write a function to return the indices of the two numbers (i.e. the pair) such that they add up to the given target.
 */
public class PairWithTargetSum {
	public static int[] search(int[] a, int sum) {
        int lo = 0;
        int hi = a.length - 1;
        
        // key is lo, value is high
        List<Integer> list = new ArrayList<>();
        
        for(; lo < hi;) {
            if (a[hi] >= sum || a[lo] + a[hi] > sum) {
                hi--;
            } else if (a[lo] + a[hi] < sum) {
                lo++;
            } else  {
                list.add(lo);
                list.add(hi);
                lo++;
            }
        }
        
        int[] returnArray = new int[list.size()];
        int i = 0;
        for (Integer elem : list) {
        	returnArray[i++] = elem;
        }
        
        return returnArray;
    }

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertArrayEquals(new int[] { 1, 3 }, PairWithTargetSum.search(new int[] { 1, 2, 3, 4, 6 }, 6));
		testName = "assertArrayEquals(new int[] { 1, 3 }, PairWithTargetSum.search(new int[] { 1, 2, 3, 4, 6 }, 6))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertArrayEquals(new int[] {0, 2}, PairWithTargetSum.search(new int[] { 2, 5, 9, 11 }, 11));
		testName = "assertArrayEquals(new int[] {0, 2}, PairWithTargetSum.search(new int[] { 2, 5, 9, 11 }, 11))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}

