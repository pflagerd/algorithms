package net.pflager;

public class BinarySearch {
    public int search(int[] array, int value) {
        if (array == null || array.length == 0)
            return -1;

        if (array.length == 1)
            return 0;

        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int middle = low + (high - low) / 2;
            if (value < array[middle])
                high = middle - 1;
            else if (value > array[middle])
                low = middle + 1;
            else
                return middle;
        }

        return -1;
    }

    public int search1(int[] array, int value) {
        if (array == null || array.length == 0)
            return -1;

        if (array.length == 1)
            return 0;

        return -1;
    }


}
