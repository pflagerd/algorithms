package net.pflager;

public class AllDigitsTheSame {
    public static boolean allDigitsTheSame(int a) {
        int x = a % 10;
        a /= 10;
        while (a != 0) {
            if (x != (a % 10)) {
                return false;
            }
            a /= 10;
        }
        return true;
    }
}
