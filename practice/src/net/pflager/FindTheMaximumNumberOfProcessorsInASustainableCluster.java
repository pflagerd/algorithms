package net.pflager;

/*
 * AWS has several data centers which have multiple processors that perform computations.
 * 
 * In one such data center, these processors are placed in a sequence with their IDs denoted by 1, 2, ..., n. 
 * 
 * Each processor consumes a certain amount of time to boot up, denoted by bootingPower[i].
 * 
 * After booding a process uses processingPower[i] (of power) to run the processes.
 * 
 * For optimum performance the data center wishes to group these processors into "sustainable" clusters.
 * 
 * Clusters may only be formed of processors located adjacent to each other.  For example, 
 * processor 2, 3, 4 and 5 could form a cluster, but 1, 3, 4 cannot.
 * 
 * The net power consumption (NPC) of a cluster of k processors (starting at index i) is defined as follows:
 * 
 * NPC = max(bootingPower[i], bootingPower[i + 1], bootingPower[i + 2] ... bootingPower[i + k - 1] + 
 *       k * sum(processingPower[i], processingPower[i + 1], processingPower[i + 2] ... procerssingPower[i + k - 1])
 *       
 * A sustainable cluster is a cluster which has a NPC not exceeding a threshold value powerMax.
 * 
 * Find the maximum number of processors which can be grouped together to form a sustainable cluster. 
 * 
 * If no such cluster(s) can be found, return 0.
 */
public class FindTheMaximumNumberOfProcessorsInASustainableCluster {

}
