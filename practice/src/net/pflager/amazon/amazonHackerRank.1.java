package net.pflager.amazon;

//class Result {
//
//    /*
//     * Complete the 'foo' function below.
//     *
//     * The function is expected to return an INTEGER.
//     * The function accepts following parameters:
//     *  1. STRING_ARRAY codeList
//     *  2. STRING_ARRAY shoppingCart
//     */
//
//    public static int foo(List<String> codeList, List<String> shoppingCart) {
//        System.err.println(codeList);
//        System.err.println(shoppingCart);
//        
//        int shoppingCartFruitIndex = 0;  // index into shoppingCart
//        for (int codeListIndex = 0; codeListIndex < codeList.size(); codeListIndex++) {
//
//            String[] group = codeList.get(codeListIndex).split("\\s+");
//            System.err.println(Arrays.toString(group));
//            
//            int shoppingCartFruitsToSkip = cartMatchesAllInGroup(group, shoppingCart, shoppingCartFruitIndex);
//            if (shoppingCartFruitsToSkip == -1) {
//                return 0;
//            } else {
//                shoppingCartFruitIndex += shoppingCartFruitsToSkip;
//            }
//            
//        }
//        
//        return 1; // we exactly ran out of both
//
//    }
//    
//    
//    public static int cartMatchesAllInGroup(String[] group, List<String> shoppingCart, int shoppingCartFruitIndex) {
//            int skipCount = 0;
//            for (int i = 0; i < group.length; ) {  // i indexes shoppingCart relative to shoppingCardFruitIndex AND index of fruit within group
//                    // index oob
//                    if (shoppingCartFruitIndex + i + skipCount >= shoppingCart.size())
//                        return -1; // ran out of shopping cart fruit
//            
//                    if (!group[i].contentEquals(shoppingCart.get(shoppingCartFruitIndex + i + skipCount)) && !group[i].contentEquals("anything")) { // demorgan?
//                        // skip a shoppingCartFruit and try to match again
//                        i = 0; skipCount++;
//                        continue;
//                    }
//                    i++;
//            }
//            return skipCount + group.length; // only if it matched everything.
//    }
//
//}
//
//class Solution {
//}
