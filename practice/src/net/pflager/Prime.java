package net.pflager;

public class Prime {
	private final static long _2__94_63 = 1 << 61;

	public static boolean isPrime(long n) {
		if (n < 0 || n > _2__94_63) {
			throw new IllegalArgumentException("n must be <= 2^62");
		}

		if (n < 2) {
			return false;
		}

		for (long i = 2; i * i <= n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		boolean first = true;
		for (int i = 2, j = 0; i < 10_000; i++, j %= 10) {
			if (isPrime(i)) {
				if (first) {
					first = false;
				} else {
					if (j++ == 9)
						System.out.println(",");
					else
						System.out.print(", ");
				}
				System.out.print(i);
			}
		}
		System.out.println();
	}
}
