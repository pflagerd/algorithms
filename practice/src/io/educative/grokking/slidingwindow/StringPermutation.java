package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string and a pattern, find out if the string contains any permutation of the pattern.
 * 
 * Permutation is defined as the re-arranging of the characters of the string. 
 * 
 * For example, “abc” has the following six permutations:
 * 
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * 
 * If a string has ‘n’ distinct characters, it will have n! permutations. 
 */
public class StringPermutation {
	public static boolean findPermutation(String str, String pattern) {
		return false;
	}
 
	@Test
	void test() {
		assertEquals(true, findPermutation("oidbcaf", "abc"));
		assertEquals(false, findPermutation("odicf", "dc"));
		assertEquals(true, findPermutation("bcdxabcdy", "bcdyabcdx"));
		assertEquals(true, findPermutation("aaacb", "abc"));
	}

}
