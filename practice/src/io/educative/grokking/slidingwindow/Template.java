package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class Template {
	public static int algorithm(int S, int[] arr) {
		return -1;
	}
 
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertEquals(2, algorithm(7, new int[] { 2, 1, 5, 2, 3, 2 }));
		testName = "assertEquals(2, algorithm(7, new int[] { 2, 1, 5, 2, 3, 2 }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(1, algorithm(7, new int[] { 2, 1, 5, 2, 8 }));
		testName = "assertEquals(1, algorithm(7, new int[] { 2, 1, 5, 2, 8 }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(3, algorithm(8, new int[] { 3, 4, 1, 1, 6 }));
		testName = "assertEquals(3, algorithm(8, new int[] { 3, 4, 1, 1, 6 }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}
