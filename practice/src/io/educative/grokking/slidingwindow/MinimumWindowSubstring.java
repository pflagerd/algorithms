package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string and a pattern, find the smallest substring in the 
 * given string which has all the character occurrences of the given pattern.
 */
public class MinimumWindowSubstring {
	public static String findString(String string, String pattern) {
		return "";
	}
 
	@Test
	void test() {
		assertEquals("abdec", findString("aabdec", "abc"));
		assertEquals("aabdec", findString("aabdec", "abac"));
		assertEquals("bca", findString("abdbca", "abc"));
		assertEquals("", findString("adcad", "abc"));
	}

}
