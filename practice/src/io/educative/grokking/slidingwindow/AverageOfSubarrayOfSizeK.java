package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;


/**
 * Given an array, find the averages of all subarrays of ‘K’ contiguous elements in it.
 * 
 * @author oy753c
 *
 */
class AverageOfSubarrayOfSizeK {
	public static double[] findAverages(int K, int[] arr) {
		return null;
	}

	@Test
	void test() {
		assertArrayEquals(new double[] { 2.2, 2.8, 2.4, 3.6, 2.8 }, AverageOfSubarrayOfSizeK.findAverages(5, new int[] { 1, 3, 2, 6, -1, 4, 1, 8, 2 }), Math.ulp(1));
	}
}