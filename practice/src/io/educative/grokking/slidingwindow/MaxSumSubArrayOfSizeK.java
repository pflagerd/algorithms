package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given an array of positive numbers and a positive number ‘k,’ 
 * find the maximum sum of any contiguous subarray of size ‘k’.
 */
public class MaxSumSubArrayOfSizeK {
	public static int findMaxSumSubArray(int k, int[] arr) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(9, findMaxSumSubArray(3, new int[] { 5, 1, 3 }));
		assertEquals(7, findMaxSumSubArray(2, new int[] { 2, 3, 4, 1, 5 }));
	}

}
