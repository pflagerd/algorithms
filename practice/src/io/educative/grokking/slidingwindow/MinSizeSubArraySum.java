package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given an array of positive integers and a number ‘S,’ 
 * find the length of the smallest contiguous subarray whose sum 
 * is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class MinSizeSubArraySum {
	public static int findMinSubArray(int S, int[] arr) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(2, findMinSubArray(7, new int[] { 2, 1, 5, 2, 3, 2 }));
		assertEquals(1, findMinSubArray(7, new int[] { 2, 1, 5, 2, 8 }));
		assertEquals(3, findMinSubArray(8, new int[] { 3, 4, 1, 1, 6 }));
	}

}
