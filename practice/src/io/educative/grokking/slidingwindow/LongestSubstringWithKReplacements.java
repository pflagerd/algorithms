package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string with lowercase letters only,
 * 
 * if you are allowed to replace no more than k letters with any letter, 
 * find the length of the longest substring having the same letters after replacement.
 */
public class LongestSubstringWithKReplacements {
	public static int findLength(String str, int k) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(2, findLength("yjnwf", 1));
		assertEquals(5, findLength("aabccbb", 2));
		assertEquals(4, findLength("abbcb", 1));
		assertEquals(3, findLength("abccde", 1));
	}

}
