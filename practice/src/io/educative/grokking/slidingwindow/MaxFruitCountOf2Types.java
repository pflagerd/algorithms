package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * You are visiting a farm to collect fruits. The farm has a single row of fruit trees. You will be given two baskets, and your 
 * goal is to pick as many fruits as possible to be placed in the given baskets.
 * 
 * You will be given an array of characters where each character represents a fruit tree. The farm has following restrictions:
 *
 * Each basket can have only one type of fruit. There is no limit to how many fruit a basket can hold.
 * 
 * You can start with any tree, but you can’t skip a tree once you have started.
 * 
 * You will pick exactly one fruit from every tree until you cannot, i.e., you will stop when you have to pick from a third fruit type.
 * 
 * Write a function to return the maximum number of fruits in both baskets.
 * 
 */
public class MaxFruitCountOf2Types {
	public static int findLength(char[] arr) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(3, findLength(new char[] {'A', 'B', 'C', 'A', 'C'}));
		assertEquals(5, findLength(new char[] {'A', 'B', 'C', 'B', 'B', 'C'}));
	}

}
