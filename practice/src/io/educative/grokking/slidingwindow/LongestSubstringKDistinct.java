package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string, find the length of the longest substring in it with no more than K distinct characters.
 */
public class LongestSubstringKDistinct {
	public static int findLength(String str, int k) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(4, findLength("araaci", 2));
		assertEquals(2, findLength("araaci", 1));
		assertEquals(5, findLength("cbbebi", 3));
		assertEquals(6, findLength("cbbebi", 10));
	}

}
