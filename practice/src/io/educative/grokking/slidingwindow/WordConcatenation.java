package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * You are given a string s and an array of strings words. All the strings of words are of the same length.
 * 
 * A concatenated substring in s is a substring that contains all the strings of any permutation of <code>words</code> concatenated.
 * 
 * For example, if words = ["ab","cd","ef"], then "abcdef", "abefcd", "cdabef", "cdefab", "efabcd", and "efcdab" are all concatenated strings. 
 * "acdbef" is not a concatenated substring because it is not the concatenation of any permutation of words.
 * 
 * Return the starting indices of all the concatenated substrings in s. You can return the answer in any order.
 * 
 */
public class WordConcatenation {
  public static List<Integer> findWordConcatenation(String string, String[] words) {
  	return null;
  }
  
	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertEquals(Arrays.asList(new Integer[] {0, 3}), findWordConcatenation("catfoxcat", new String[] { "cat", "fox" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {0, 3}), findWordConcatenation(\"catfoxcat\", new String[] { \"cat\", \"fox\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(Arrays.asList(new Integer[] {3}), findWordConcatenation("catcatfoxfox", new String[] { "cat", "fox" }));
		testName = "assertEquals(Arrays.asList(new Integer[] {3}), findWordConcatenation(\"catcatfoxfox\", new String[] { \"cat\", \"fox\" }))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}

}
