package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * Given a string, find the length of the longest substring which has all distinct characters.
 */
public class LongestSubstringAllDistinct {
	public static int findLength(String str) {
		return -1;
	}
 
	@Test
	void test() {
		assertEquals(3, findLength("aabccbb"));
		assertEquals(2, findLength("abbbb"));
		assertEquals(3, findLength("abccde"));
	}

}
