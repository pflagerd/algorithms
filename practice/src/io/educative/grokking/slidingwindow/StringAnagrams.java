package io.educative.grokking.slidingwindow;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

/*
 * Given a string and a pattern, find all anagrams of the pattern in the given string.
 */
public class StringAnagrams {
	public static List<Integer> findStringAnagrams(String str, String pattern) {
		return null;
	}
 
	@Test
	void test() {
		assertArrayEquals(new Integer[] {1, 2}, findStringAnagrams("ppqp", "pq").toArray(new Integer[0]));
		assertArrayEquals(new Integer[] {2, 3, 4}, findStringAnagrams("abbcabc", "abc").toArray(new Integer[0]));
		assertArrayEquals(new Integer[] {2, 3, 4}, findStringAnagrams("xyzxwxzyx", "xyz").toArray(new Integer[0]));
	}

}
