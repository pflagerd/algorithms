PairWithTargetSum.java (Pair with Target Sum (easy))
	https://practice.geeksforgeeks.org/problems/pair-with-given-sum-in-a-sorted-array4940/1
	
  Not Sorted input.	
	https://practice.geeksforgeeks.org/problems/count-pairs-with-given-sum5022/1
	https://leetcode.com/problems/two-sum/
	https://code-exercises.com/programming/medium/32/two-sum-pair-with-a-given-sum
	https://www.codingninjas.com/codestudio/problems/two-sum_839653
	https://algo.monster/problems/two_sum_unique_pairs
	
  Similar
  	https://codedrills.io/contests/interview-practice-round-6/problems/pair-sum-less-than-target
  	https://algodaily.com/challenges/two-sum
  
RemoveDuplicates.java (Remove Duplicates (easy))
  	https://practice.geeksforgeeks.org/problems/remove-duplicate-elements-from-sorted-array/1
  	https://leetcode.com/problems/remove-duplicates-from-sorted-array/
  	https://leetcode.com/problems/remove-duplicates-from-sorted-list/
  	https://workat.tech/problem-solving/practice/remove-duplicates-sorted-linked-list
  	https://techiedelight.com/practice/?problem=RemoveDuplicatesLinkedList
  	https://www.interviewbit.com/problems/remove-duplicates-from-sorted-array/
  	https://www.interviewbit.com/problems/remove-element-from-array/
  	https://www.codecademy.com/courses/technical-interview-practice-python/lessons/tip-python-lists/exercises/tip-python-lists-duplicates-no-space
  	https://www.codingninjas.com/codestudio/problem-details/remove-duplicates-from-unsorted-linked-list_1069331
  	
  Similar 	
  	https://practice.geeksforgeeks.org/problems/remove-duplicates3034/1
  	https://leetcode.com/problems/remove-duplicates-from-an-unsorted-linked-list/