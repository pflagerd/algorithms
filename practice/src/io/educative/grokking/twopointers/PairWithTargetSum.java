package io.educative.grokking.twopointers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

/*
 * Given an array of sorted numbers and a target sum, find a pair in the array whose sum is equal to the given target.
 * 
 * Write a function to return the indices of the two numbers (i.e. the pair) such that they add up to the given target.
 */
public class PairWithTargetSum {
	public static int[] search(int[] arr, int targetSum) {
		return null;
	}

	@TestFactory
	public Collection<DynamicTest> translateDynamicTests() {
		Collection<DynamicTest> dynamicTests = new ArrayList<>();

		Executable exec;
		String testName;
		DynamicTest dTest;
		
		exec = () -> assertEquals(new int[] { 1, 3 }, PairWithTargetSum.search(new int[] { 1, 2, 3, 4, 6 }, 6));
		testName = "assertEquals(new int[] { 1, 3 }, PairWithTargetSum.search(new int[] { 1, 2, 3, 4, 6 }, 6))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		exec = () -> assertEquals(new int[] {0, 2}, PairWithTargetSum.search(new int[] { 2, 5, 9, 11 }, 11));
		testName = "assertEquals(new int[] {0, 2}, PairWithTargetSum.search(new int[] { 2, 5, 9, 11 }, 11))";
		dTest = DynamicTest.dynamicTest(testName, exec);
		dynamicTests.add(dTest);

		return dynamicTests;
	}
}

