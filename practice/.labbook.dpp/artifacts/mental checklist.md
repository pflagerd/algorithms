Make happy self-talk.

Check ranges of variables and compute domains and ranges. Use correct variables.

Slow it down.

Check static-ness

Double-check expressions
​    Use temporary variables to help precise the mental processes.

Double-check types

Make sure all data structures are properly initialized.

Don't optimize early

Test scaffolding is okay.

Look for all occurrences of the pattern of a discovered bug in already written code.

Ask if TDD is a virtuous (ask for permission)

Initializing 2-dimensional int array with literal starts new int\[\]\[\]

How to encode test target (thing under test) into the test name

Did I get the return type right?

Does the type of the thing I'm calling match the type I'm assigning to?

Does the type of the thing I'm passing match the type I'm passing to?

Am I passing the right thing?

Check for arithmetic overflows.



1 LISTEN

2 GOOD EXAMPLE (Make larger, a)

3 BRUTE FORCE - state it, state runtime, optimize

4 OPTIMIZE

5 WALK THROUGH YOUR ALGORITHM (you need to know 95%)

6 CODE (Try to write straight, Use space wisely, Style matters)

​	Style matters: consistent braces, consistent variables, consistent spaces, descriptive variable names, camelCase

​	Modularize OFTEN. Write TOP-DOWN.

7 TEST 

​	a) look for common errors

​	b) start with small cases

​	c) edge cases

​	d) maybe another big test

​	e) test your code (not your algorithm)

​	f) when you find a bug Don't Panic. Think it through.



int std::stoi(const std::string& s, size_t* s = 0; int base = 0); 