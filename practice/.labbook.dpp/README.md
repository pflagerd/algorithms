### How to test a GeeksForGeeks exercise.

Let's say the exercise url is: https://practice.geeksforgeeks.org/problems/print-all-possible-strings/1`

* Create a directory (package) called `net/pflager/geeksforgeeks/print_all_possible_strings_1` 

  * Notice that `-` has been replaced with `_` so java won't freak.
  * Also notice that the `/1` is translated to `_1` because it's possible for there to  be more than one "print-all-possible-strings" problem.

* Create a file called `net/pflager/geeksforgeeks/print_all_possible_strings_1/instructions.md` and copy the "Problem" pane's content to `instructions.md`

* Edit `instructions.md` and resolve any grammatical and/or content errors.

* Copy the files from the `net/pflager/geeksforgeeks/template/` directory to the new directory. 

* Edit the newly copied `TestDriver.java`, and make sure the package matches the new parent directory name. e.g. `net.pflager.geeksforgeeks.print_all_possible_strings_1`

* Edit the newly copied `GFG.java` file, and copy the code from the GeeksForGeeks code editor over top of the corresponding code in the driver.  Make sure to keep the class name as GFG (same as the file basename).  You may have to tweak the code slightly to make it runnable.

* Edit the testdata.xml file, and add the test data corresponding to the examples in `instructions.md`.

* NOTE: Sometimes GeeksForGeeks passes the size of a string as an integer preceding the string, and other times they pass just the string.  This framework observes the former convention exclusively, which may require you to modify the code slightly on your local machine.




