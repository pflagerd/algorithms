Is there a simple function of $n$ to compute the following:

$f(n)=\sum\limits_{j=1}^n\sum\limits_{i = 1}^ji\tag 1$

To do this, I first define a function $g(n)$ as follows:

​	$g(n)=\sum\limits_{i=1}^ji\tag 2$

So (1) becomes

$f(n)=\sum\limits_{j=1}^ng(j)\tag 3$



Now, what are the first several values of $g(n)$?
$$g(1)=1$$
$$g(2)=3$$
$$g(3)=6$$
$$g(4)=10$$
$$g(5)=15$$
$$g(6)=21$$
$$g(7)=28$$

Let's hypothesize that this sequence might be represented as a polynomial.

Let's take a quick look at the average value of the first derivative of this polynomial, which may be computed by examining the difference between each pair of values:

$$g(2)-g(1)=2$$

$$g(3)-g(2)=3$$

$$g(4)-g(3)=4$$

$$g(5)-g(4)=5$$

$$g(6)-g(5)=6$$

$$g(7)-g(6)=7$$

That's cool! The function is monotonic and increasing as one might expect, but what's truly exciting is that the difference between each of these differences is a constant value: $1$

This gives us the second derivative: $g''(n) = 1$:

By integrating, we find the first derivative must be of the form $g'(n)=n + C$.

And by integrating again, the function itself must be of the form

 $g(n)=\frac{n^2}{2} + Cn + D \tag 3$

Since we have some values of $n$, we can treat this as solving a system of equations for $C$ and $D$.

When $n=1$, we know that $g(1)=1$.

Substituting $n=1$ into $(3)$.

$g(1)=1=\frac{1}{2}+C\cdot 1 + D \tag 4$

So $\frac{1}{2}=C+D$.

Substituting $C=\frac{1}{2}-D$ into $(3)$, we get

$g(n)=\frac{n^2}{2}+Dn+\frac{1}{2}-D \tag 5$.

Apply $g(2)=3$ to $(5)$ and we get

$3=\frac{4}{2}+2D + \frac{1}{2} - D = \frac{5}{2} + D.$



Solve for $C$, and we get $C=\frac{1}{2}$.

Substitute C into $(5)$, and we get

$g(n)=\frac{n^2}{2}+\frac{n}{2} \tag 6$

And simplifying a bit:

$g(n)=\frac{n(n+1)}{2} \tag 7$

Substituting $(6)$ into $(3)$, we get

$f(n)=\sum\limits_{j=0}^n\left(\frac{j^2}{2}+\frac{j\ +\ 1}{2}\right) \tag 8$

Which, by distributing the $\sum $, we can write as

$f(n)=\sum\limits_{j=0}^n\frac{j^2}{2}+\sum\limits_{j=0}^n\frac{j\ +\ 1}{2} \tag 9$

Factoring out the $\frac{1}{2}$

$f(n)=\frac{1}{2}\left(\sum\limits_{j=0}^n j^2 + \sum\limits_{j=0}^n (j + 1)\right) \tag {10}$

Let's work on the right side of the $+$ in $(10)$ as a separate function $h(n)$:

$h(n)=\sum\limits_{j=0}^n (j + 1) \tag {11}$

We can distribute the $\sum$ here, and  write $(11)$ as

$h(n)=\sum\limits_{j=0}^n j + \sum\limits_{j=0}^n 1 \tag {12}$

Taking the right side of the $+$ in $(12)$, and applying the $\sum$ to the constant $1$, we get

$\sum\limits_{j=0}^n 1= n+1$













