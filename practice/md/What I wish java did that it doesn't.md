```
int x = for (int i = 0; i < 10; i++);

// x is 10


int j = {
    int c = a + b;
    return c; // optional, the return value of the block is the
              // value of the last statement in it.
}
```

