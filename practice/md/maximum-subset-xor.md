https://practice.geeksforgeeks.org/problems/maximum-subset-xor/1



All possible combinations - n^2

0 <= N <= 10^5
0 <= arr[i] <= 10^6

For set of size 1, output is always the single array element.

For set of size 2, ?

Finding the maximum of the array is finding the lower bound of the possible solutions


1 0 1 0
1 0 0 1
1 0 0 0
=========
1 0 1 1


Odd number of occurrences of a bit results in that bit being 1.

5 5 is allowed.  Not really a set at all.  It's in fact a multi-set.



Maximum means if one had only disjoint bit-sets, one should sort and pick from the highest to the lowest.





From David:



Fun facts about XOR on the set of non-negative integers (I believe):

 

xor is commutative:

 

  a xor b = b xor a

 

xor is associative

 

  (a xor b) xor c = a xor (b xor c)

 

The element 0 is an identity element:

 

  0 xor a = a = a xor 0

 

Each non-negative integer is its own inverse under xor:

 

  a xor a = 0

 

This suggests that the non-negative integers under xor form a group, actually, an abelian group. I’m not sure what we can do with this… But here’s a page I just found that discusses it:

 https://www.themathcitadel.com/group-theory-xor-and-binary-codes-introducing-coding-theory/

 I always wanted to turn group theory into a useful tool on some real problem. In grad school, my fever dream was to create a group theory for feature extraction of patterns in neural networks. Ha. 

