The CodeMart Crazy Raffle (was: https://practice.geeksforgeeks.org/problems/first-come-first-serve/0)

CodeMart is giving away a free prize in a unique kind of raffle based on customer visits.  The customer who visits a given number of times, k, and whose first visit was earliest wins the prize.  Only CodeMart knows what k is.  Only one customer may win the prize, and that customer must visit exactly k times. Any customer visiting more or less than exactly k times will not win.  

You are given an array of non-negative integers whose elements are the IDs of visiting customers.  Each customer has a unique ID. Each occurrence of an ID in the array represents one visit by that customer.  The lower the array index of an ID occurrence in the array, the earlier that customer visited.  



**Output:**
Output the customer ID of the winner. Output "-1" if there is no winner.

**Constraints:**
1<=T<=10
1<=n<=1000
1<=a[i]<=100000
1<=k<=100

**Example:**

**Input:**

````
3
7 2
1 7 4 3 8 7 4
7 2
1 7 4 3 4 8 7
6 1
4 1 6 1 6 4
````

**Output:**

````
7
7
-1
````