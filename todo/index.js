// create an array with nodes
let nodes = [
  { id: 1, label: "Daniel", href: 'https://insite.web.boeing.com/culture/displayBluesInfo.do?bemsid=1593970' },
  { id: 2, label: "David", href: 'https://insite.web.boeing.com/culture/displayBluesInfo.do?bemsid=1663455' },
  { id: 3, label: "Richard", href: 'https://insite.web.boeing.com/culture/displayBluesInfo.do?bemsid=133494' },

  {
    id: 11,
    label: "Math",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 12,
    label: "Data Structures\nand Algorithms",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    mass: 4,
  },
  {
    id: 13,
    label: "System Design",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 14,
    label: "Fortran\n(Programming Language)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 15,
    label: "bash\n(Programming Language)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 16,
    label: "ksh\n(Programming Language)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 17,
    label: "java\n(Programming Language)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 18,
    label: "Flight Sciences",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 19,
    label: "Geometry",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 20,
    label: "ECFD",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 21,
    label: "Aero Performance\n(Org and Group)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 22,
    label: "Richard's New Group",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 23,
    label: "Boyarsky OCA\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Boyarsky-Jeanne-Selikoff-Scott-Java-Study-1.pdf',
  },
  {
    id: 24,
    label: "Jet Transport Performance Methods\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/JetTransportPerformanceMethods.pdf',
  },
  {
    id: 25,
    label: "System Design Interview An Insiders Guide by Alex Yu\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/System Design Interview An Insiders Guide by Alex Yu (z-lib.org).pdf',
  },
  {
    id: 26,
    label: "Number Theory Through Inquiry\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: "../doc/number-theory-through-inquiry.pdf"
  },
  {
    id: 27,
    label: "The moore method",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 28,
    label: "Artificial Intelligence\nA Modern Approach\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Artificial-Intelligence-A-Modern-Approach-4th-Edition-1.pdf',
  },
  {
    id: 29,
    label: "Introduction To\nMathematical Thinking\nKeith Devlin\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/IntroductionToMathematicalThinking.pdf',
  },
  {
    id: 30,
    label: "Knowledge graphs: Introduction, history, and perspectives\n(Article)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/AI Magazine - 2022 - Chaudhri - Knowledge graphs  Introduction  history  and perspectives.pdf',
  },
  {
    id: 31,
    label: "Python",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 32,
    label: "The Elements of Programming Style\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/TheElementsOfProgrammingStyle.pdf'
  },
  {
    id: 33,
    label: "Proofs",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 40,
    label: "Ohio State CIS 680 - Data Structures\n(Course Archive)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: 'https://web.archive.org/web/20061018204339/http://www.cse.ohio-state.edu/~gurari/course/cis680/cis680.html#cis680No1.html',
  },
  {
    id: 50,
    label: "Algorithms, Part 1\nPrinceton University\n(Coursera Course)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: 'https://www.coursera.org/learn/algorithms-part1',
  },
  {
    id: 60,
    label: "Book of Proof\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/book-of-proof.pdf',
  },
  {
    id: 70,
    label: "FORTRAN VERSION 5\nREFERENCE MANUAL\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/CDC 60481300E_Fortran_Version_5_Reference_Manual_Jan1981.pdf',
  },
  {
    id: 80,
    label: "Abstract Algebra - Herstein\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/abstract-algebra-herstein-i-1996.pdf',
  },
  {
    id: 90,
    label: "Geometric Tools for\nComputer Graphics\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Geometric Tools For Computer Graphics.pdf',
  },
  {
    id: 100,
    label: "Using GNU Fortran 95\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/gfortran.pdf',
  },
  {
    id: 110,
    label: "Introduction to Curves and Surfaces - Rockwell\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/IntroductionToCurvesAndSurfaces-Rockwell.pdf',
  },
  {
    id: 120,
    label: "Introduction to Programming using Fortran 95 2003 2008\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Introduction to Programming using Fortran 95 2003 2008.pdf',
  },
  {
    id: 130,
    label: "Microservices Patterns\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Microservices Patterns.pdf',
  },
  {
    id: 140,
    label: "Programming in D\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Programming_in_D.pdf',
  },
  {
    id: 150,
    label: "Sedgewick Algorithms\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Sedgewick Algorithms-4th.pdf',
  },
  {
    id: 160,
    label: "Stanford Fortran 77 Tutorial\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: 'https://web.stanford.edu/class/me200c/tutorial_77/',
  },
  {
    id: 170,
    label: "Subdivision Methods for Geometry Design\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Subdivision Methods for Geometric Design.pdf',
  },
  {
    id: 180,
    label: "Universal Principles of Design\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Universal_Principles_of_Design.pdf',
  },
  {
    id: 190,
    label: "Writing Mathematical Proofs\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: '../doc/Writing Mathematical Proofs.pdf',
  },
  {
    id: 200,
    label: "Sedgewick Algorithms Web Site\n(Book)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
    href: 'https://algs4.cs.princeton.edu/home/',
  },
  {
    id: 210,
    label: "Panda\n(Software)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 220,
    label: "Obsolete K&R C",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
  {
    id: 230,
    label: "C\n(Programming Language)",
    shape: "box",
    font: { face: "Monospace", align: "left" },
  },
];

// create an array with edges
let edges = [
  { from: 1, to: 11, label: "wants to learn" },
  { from: 1, to: 12, label: "wants to learn" },
  { from: 1, to: 13, label: "wants to learn" },
  { from: 1, to: 14, label: "has to learn some" },
  { from: 1, to: 15, label: "has to learn some" },
  { from: 1, to: 16, label: "has to learn some" },
  { from: 1, to: 17, label: "wants to master" },
  { from: 1, to: 18, label: "supports" },
  { from: 1, to: 19, label: "wants to master" },
  { from: 1, to: 20, label: "can support" },
  { from: 1, to: 21, label: "must support" },
  { from: 1, to: 28, label: "wants to learn" },
  { from: 1, to: 30, label: "wants to read" },
  { from: 1, to: 31, label: "has to learn some" },
  { from: 1, label: "is learning", to: 140 },
  { from: 1, label: "is learning", to: 180 },
  { from: 1, label: "is learning", to: 130 },
  { from: 1, label: "is expert in", to: 230 },

  { from: 2, to: 31, label: "has to learn some" },

  { from: 3, to: 22, label: "has to learn things for" },

  { from: 11, to: 19, label: "includes" },
  { from: 11, to: 33, label: "includes" },

  { from: 12, label: "has resource", to: 40},
  { from: 12, label: "has resource", to: 50},
  { from: 12, label: "has resource", to: 150},
  { from: 12, label: "has resource", to: 160},
  { from: 12, label: "has resource", to: 200},

  { from: 13, to: 25, label: "has resource" },

  { from: 14, to: 32, label: "has resource" },
  { from: 14, label: "has resource", to: 70 },
  { from: 14, label: "has resource", to: 100 },
  { from: 14, label: "has resource", to: 120 },
  { from: 14, label: "has resource", to: 160 },

  { from: 17, to: 23, label: "has resource" },

  { from: 19, label: "has resource", to: 90 },
  { from: 19, label: "has resource", to: 110 },
  { from: 19, label: "has resource", to: 170 },

  { from: 21, to: 24, label: "includes" },

  { from: 22, label: "supports", to: 210, },

  { from: 27, to: 26, label: "includes" },

  { from: 33, to: 27, label: "has resource" },
  { from: 33, to: 29, label: "has resource" },
  { from: 33, to: 60, label: "has resource" },
  { from: 33, to: 80, label: "has resource" },
  { from: 33, to: 190, label: "has resource" },

  { from: 210, label: "has resource", to: 220 },

  { from: 230, label: "can help with", to: 220 },

  // { from: 1, to: 2, label: "middle", font: { align: "middle" } },
  // { from: 1, to: 3, label: "top", font: { align: "top" } },
  // { from: 2, to: 4, label: "horizontal", font: { align: "horizontal" } },
  // { from: 2, to: 5, label: "bottom", font: { align: "bottom" } },
];

// create a network
let container = document.getElementById("mynetwork");
let data = {
  nodes: new vis.DataSet(nodes),
  edges: new vis.DataSet(edges),
};
let options = { };
let nodeMap = {};
nodes.forEach(node => {
  node.mass = node.label.length / 5;
  nodeMap[node.id] = node;
});

let network = new vis.Network(container, data, options);

network.on("click", function (params) {
  params.event = "[original event]";
  console.log(JSON.stringify(params));
  let href = nodeMap[params.nodes[0]].href;
  if (href) {
    window.open(href, '_blank');
  }
});
