There is a better solution to https://practice.geeksforgeeks.org/problems/message-spreading4258/0 than 2n-2.



$$2(\lceil{N \over 2}\rceil - 1) + 2(\lfloor{N \over 2}\rfloor - 1) + 2$$



$$2\lceil{N \over 2}\rceil - 2 + 2\lfloor{N \over 2}\rfloor - 2 + 2$$

$$2\lceil{N \over 2}\rceil + 2\lfloor{N \over 2}\rfloor - 2$$

$$2(\lceil{N \over 2}\rceil + \lfloor{N \over 2}\rfloor) - 2$$



if N is even, N - 2

if N is odd, N - 2







































