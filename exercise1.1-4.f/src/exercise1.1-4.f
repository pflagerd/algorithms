      program cnvbas

      character num1*20, num2*20, num3*20

      write (*,*) 'Chapter 1 exercise 1.1-4'
      num1 = '82'
      num2 = '109'
      num3 = '3433'

      call conv( num1, 10, 2 )
      call conv( num2, 10, 2 )
      call conv( num3, 10, 2 )

      call conv( num3, 10, 8 )
      call conv( num3, 5, 10 )
      call conv( num3, 5, 8 )
      call conv( num3, 5, 2 )
      

      write (*,*) 'Done.'
      end


      subroutine conv(num, betin, betout)
      character num*20
      integer betin
      integer betout
      
      character ch
      integer digit
      integer numin(20)
      integer nin
      integer numout(20)
      integer nout
      integer i, k
      
C     --- Load the input array
      k = 0
      do 10, i = 1, 20
          ch = num(i:i)
          digit = ichar( ch ) - ichar( '0' )
          if ( (0 .le. digit) .and. (digit .le. 9) ) then
              k = k + 1
              numin(k) = digit
          else
              goto 20
          end if
   10 continue

   20 continue

      call cb2b(k, numin, betin, nout, numout, betout)

C     Now display the results!
      call display(k, numin, nout, numout)

      end


      subroutine cb2b(nin, numin, betin, nout, numout, betout)
C     integer nin
      integer numin(nin)
      integer betin

      integer nout
      integer numout(nout)
      integer betout

      integer result(20)
      integer accum
      integer rem
      integer i, k

      nout = 0

      accum = 0
      do 50 i = 1, nin
          accum = accum * betin
          accum = accum + numin(i)
   50 continue

      nout = 0
      do 60 k = 1, 20
          if (accum .eq. 0) goto 65
          result(k) = mod( accum, betout )
          nout = k
          accum = accum / betout
   60 continue

   65 continue

C     --- Reverse the digits.
      do 70 i = nout, 1, -1
         numout( nout - i + 1 ) = result( i ) 
   70 continue
      end

      subroutine display(nin, numin, nout, numout)
      integer nin, nout
      integer numin(20)
      integer numout(20)
      integer i

      write(*, 100) (numin(i:i), i = 1, nin)
      write(*, 110) (numout(i:i), i = 1, nout)
  100 format('input: ', 20I1)
  110 format('output: ', 20I1)
      end
