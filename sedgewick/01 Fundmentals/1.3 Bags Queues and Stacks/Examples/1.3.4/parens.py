data = [
                [ "[()]{}{[()()]()}",
                  "[(])"
        

                [ 4, 6, 8, 7, 5, 3, 2, 9, 0, 1 ],
                [ 2, 5, 6, 7, 4, 8, 9, 3, 1, 0 ],
                [ 4, 3, 2, 1, 0, 5, 6, 7, 8, 9 ],
                [ 1, 2, 3, 4, 5, 6, 9, 8, 7, 0 ],
                [ 0, 4, 6, 5, 3, 8, 1, 7, 2, 9 ],
                [ 1, 4, 7, 9, 8, 6, 5, 3, 0, 2 ],
                [ 2, 1, 4, 3, 6, 5, 8, 7, 9, 0 ]
            ]

answers = [ True, False ]

class stackops:




    """Evaluate a sequence of numbers to determine whether it represents
    a permutation of the numbers 0..n that could be produced by stack
    operations.

    We use the numbers 0 through n to label the original items in the
    sequence.  We assume that our stack push operations will consume
    these numbers in their original order, 0 through n.  We produce
    an output sequence by means of stack pop operations.  

    If we simply perform a push and immediate pop of each input number,
    we would produce the output 0 1 2 3 ... n.  By changing the sequence
    of pushes and pops the output becomes a permutation of those numbers.
    Yet not all permutations can be produced in this way.  The following
    code determines whether a particular output sequence is a permutation
    which can be produced by such a sequence of stack push and pop
    operations from the original input sequence of 0 though n.
    """

    def is_valid(self, permutation):
        length = len( permutation )

        # Our inputs, to be rearranged, are the values 0 through n
        n = length - 1

        stack = []
        last_consumed = -1

        for value in permutation:
            print( '\nvalue:', value, 'and stack:', stack )
            while last_consumed < value:
                last_consumed += 1
                stack.append( last_consumed )
                print( '... pushing', last_consumed, ': ', stack )
            if stack[ -1 ] == value:
                stack.pop()
                print( '... popping', value, ': ', stack )
            else:
                print( '... unable to pop', value, '-- exiting' )
                return False

        return True


if __name__ == '__main__':

    checker = stackops()

    for i, perm in enumerate( data ):

        print( 'Evaluate possible permutation', perm )

        valid = answers[ i ]

        label = 'Test: Ok  '
        we_say = checker.is_valid( perm )
        if we_say != valid:
            label = 'Test: oops'

        print( '\n', label, " : ", perm, " : ", we_say, '\n\n' )





        
