package net.finton;
import edu.princeton.cs.algs4.Queue;

/**
 * 1.3.15 Write a Queue client that takes a command-line argument k and prints the kth
 *        from the last string found on standard input (assuming that standard input has k or
 *        more strings).
 * We need a Queue.
 */
public class Exercise1_3_15
{

    public static void main( String[] args )
    {
        System.out.println( "Parameters" );
        int i = 1;
        for (String s : args) {
            if (i == (args[0].charAt(0) - '0'))
                System.out.println("... arg " + i++ + " = " + s);
        }
    }




}
