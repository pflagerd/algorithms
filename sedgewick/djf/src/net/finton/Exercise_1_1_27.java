package net.finton;

public class Exercise_1_1_27 
{

    public static double binomial(int n, int k, double p)
    {
        System.out.printf("    in binomial(%d, %d, %4.2f) ...%n", n, k, p);
        if ((n == 0) && (k == 0)) return 1.0;
        if ((n < 0) || (k < 0)) return 0.0;
        return (1 - p) * binomial(n-1, k, p) + p * binomial(n-1, k-1, p);
    }

    public static void main(String[] args) 
    {
        System.out.println("Exercise_1_1_27");
        
        int n = 5; // 100;
        int k = 2;
        double p = 0.25;
        double mystery = binomial(n, k, p);

        System.out.printf("binomial(%d, %d, %4.2f) = %f%n", n, k, p, mystery);
    }

}
