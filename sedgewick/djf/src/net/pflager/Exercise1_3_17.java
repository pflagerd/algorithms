package net.pflager;

import edu.princeton.cs.algs4.Date;
import edu.princeton.cs.algs4.Transaction;
import edu.princeton.cs.algs4.Queue;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_17 {

    public static Transaction[] readTransactions(String string) {
        String[] stringArray = string.split("\\s+");
        Queue<Transaction> q = new Queue<>();
        for (int i = 0; i < stringArray.length; i += 3) {
            String[] sa = stringArray[i + 1].split("/");
            assert sa.length == 3;
            int[] ia = new int[3];
            ia[0] = Integer.parseInt(sa[0]);
            ia[1] = Integer.parseInt(sa[1]);
            ia[2] = Integer.parseInt(sa[2]);

            q.enqueue(new Transaction(stringArray[i], new Date(ia[0], ia[1], ia[2]), Double.parseDouble(stringArray[i + 2])));
        }

        int qSize = q.size();
        Transaction[] returnValue = new Transaction[qSize];

        for (int i = 0; i < qSize; i++) {
            returnValue[i] = q.dequeue();
        }

        return returnValue;
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void testTransaction5_22_1939() throws IOException {
            assertEquals(new Transaction("Turing", new Date(5, 22, 1939), 11.99), readTransactions("Turing 5/22/1939 11.99")[0]);
        }

        @org.junit.jupiter.api.Test
        void testTransaction5_22_1939_w_Arrays() throws IOException {
            assertArrayEquals(new Transaction[] { new Transaction("Turing", new Date(5, 22, 1939), 11.99) }, readTransactions("Turing 5/22/1939 11.99"));
        }

        @org.junit.jupiter.api.Test
        void testTwoDifferentTransactions() throws IOException {
            assertArrayEquals(new Transaction[] { new Transaction("Turing", new Date(5, 22, 1939), 11.99), new Transaction("Dijkstra", new Date(3, 25, 1943), 12.11) }, readTransactions("Turing 5/22/1939 11.99 Dijkstra 3/25/1943 12.11"));
        }

        @org.junit.jupiter.api.Test
        void testThreeDifferentTransactions() throws IOException {
            assertArrayEquals(new Transaction[] { new Transaction("Turing", new Date(5, 22, 1939), 11.99),
                                                  new Transaction("Dijkstra", new Date(3, 25, 1943), 12.11),
                                                  new Transaction("Euler", new Date(3, 25, 1742), 2.718) },
                              readTransactions("Turing 5/22/1939 11.99 Dijkstra 3/25/1943 12.11 Euler 3/25/1742 2.718"));
        }

    }

}
