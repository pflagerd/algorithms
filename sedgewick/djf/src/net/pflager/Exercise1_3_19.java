package net.pflager;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_19 {

    static class Node<Item> {
        private Item item;
        private Node next;

        public Node(Item item) {
            this.item = item;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void test() throws IOException {
            Node first = new Node<Integer>(10);
            Node node1 = new Node<Integer>(11);
            first.next = node1;
            Node node2 = new Node<Integer>(12);
            node1.next = node2;
            assertEquals("10 11 12", first.toString());

            for (Node x = first; x.next != null; x = x.next) {
                if (x.next.next == null) {
                    x.next = x.next.next;
                    break;
                }
            }

            assertEquals("10 11", first.toString());

        }

    }

}
