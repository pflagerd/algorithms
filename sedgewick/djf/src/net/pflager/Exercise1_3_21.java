package net.pflager;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_21 {

    static class Node<Item> {
        private Item item;
        private Node<Item> next;

        public Node(Item item) {
            this.item = item;
        }

        /**
         * Deletes the kth node from this Node (numbering from 0. 0 being this Node)
         */
        public Node delete(int k) {
            if (k == 0)
                return this.next;

            Node<Item> node = this;
            for (int i = 0; i < k - 1 && node != null && node.next != null; i++) {
                node = node.next;
            }
            node.next = node.next.next;
            return this;
        }

        public static Node<String> makeListFromString(@org.jetbrains.annotations.NotNull String listAsString) {
            String[] listAsStringArray = listAsString.split("\\s+");
            if (listAsStringArray.length == 0)
                throw new IllegalArgumentException("listAsString must be non-null and must contain at least one word");

            Node<String> head = null;
            Node<String> tail = null;
            for (int i = 0; i < listAsStringArray.length; i++) {
                if (head == null) {
                    head = tail = new Node<String>(listAsStringArray[i]);
                    continue;
                }
                tail.next = new Node<String>(listAsStringArray[i]);
                tail = tail.next;
            }
            return head;
        }

        public static <Item> Node<Item> makeListFromString(@org.jetbrains.annotations.NotNull Item[] items) {

            Node<Item> head = null;
            Node<Item> tail = null;
            for (int i = 0; i < items.length; i++) {
                if (head == null) {
                    head = tail = new Node<Item>(items[0]);
                    continue;
                }
                tail.next = new Node<Item>(items[i]);
                tail = tail.next;
            }
            return head;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node<Item> n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void testString() throws IOException {
            Node<String> first = Node.makeListFromString("10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }

        @org.junit.jupiter.api.Test
        void testIntegerArray() throws IOException {
            Integer[] integers = new Integer[22];
            for (int i = 10; i < 32; i++) {
                integers[i - 10] = i;
            }

            Node<Integer> first = Node.makeListFromString(integers);

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }

    }
}
