package net.pflager;

public class Bits {

	private static final int[] bitCountNybble = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
	private static final int[] bitCountByte = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};
	
	public static int bitCount(int i) {
		int bitCount = 0;
//		for (int j = Integer.BYTES * 2; j > 0 && i != 0; j--) {
//			bitCount += bitCountNybble[i & 0xF];
//			i >>= 4;
//		}
		
		for (int j = Integer.BYTES * 2; j > 0 && i != 0; j--) {
			bitCount += bitCountByte[i & 0xFF];
			i >>= 8;
		}

		return bitCount;
	}
	
	
	public static void main(String[] args) {
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			if (bitCount(i) != Integer.bitCount(i))
				throw new RuntimeException("Bad bit count for " + i);
			
//			if (i != 0)
//				System.out.print(", ");
//			System.out.print(bitCount(i));
		}
//		System.out.println();
	}

}
