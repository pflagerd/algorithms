package net.pflager;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_20 {

    static class Node<Item> {
        private Item item;
        private Node<Item> next;

        public Node(Item item) {
            this.item = item;
        }

        /**
         * Deletes the kth node from this Node (numbering from 0. 0 being this Node)
         */
        public Node delete(int k) {
            if (k == 0)
                return this.next;

            Node<Item> node = this;
            for (int i = 0; i < k - 1 && node != null && node.next != null; i++) {
                node = node.next;
            }
            node.next = node.next.next;
            return this;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void test() throws IOException {
            Node<Integer> first = new Node<>(10);
            Node<Integer> last = first;
            for (int i = 11; i < 32; i++) {
                Node<Integer> node = new Node<>(i);
                last.next = node;
                last = node;
            }
            System.err.println(first);

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }

    }

}
