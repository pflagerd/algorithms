package net.pflager;

import java.util.Arrays;

//
// Sedgewick p 56.
// 1.1.15 Write a static method histogram() that takes an array a[] of int values and
// an integer M as arguments and returns an array of length M whose ith entry is the number
// of times the integer i appeared in the argument array. If the values in a[] are all
// between 0 and M�1, the sum of the values in the returned array should be equal to
// a.length.
//
public class Exercise1_1_15 {
	public static int[] histogram(int[] a, int M) {
		if (a == null && a.length > 0)
			throw new IllegalArgumentException("a must be non-null, and it must contain at least 1 integer");
	
		if (M <= 0)
			throw new IllegalArgumentException("M must be greater than zero");
	
		int[] result = new int[M];
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] < 0 || a[i] >= M)
				continue;
			result[a[i]]++;
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(Arrays.toString(histogram(new int[] {0, 1, 2, 2, 3, 4, 5, 6}, 4)));
	}
}