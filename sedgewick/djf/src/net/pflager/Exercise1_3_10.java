package net.pflager;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_10 {
    public static String InfixToPostfix(String input) {
        Stack<String> stack = new Stack<>();

        String[] tokens = input.split(" ");

        for (String token : tokens) {
            if (token.contentEquals("(")) {
                ;
            } else if (token.contentEquals(")")) {
                String rightArg = stack.pop();
                String operator = stack.pop();
                String leftArg = stack.pop();
                stack.push(leftArg + " " + rightArg + " " + operator);
            } else if ("+-*/".contains(token)) {
                stack.push(token);
            } else { // it's a digit
                stack.push(token);
            }
        }

        return stack.pop();
    }

    public static void main(String[] args) {

    }

    @Test
    void test1() {
        assertEquals("1 2 + 3 4 - 5 6 - * *", InfixToPostfix("( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )"));
    }
}

