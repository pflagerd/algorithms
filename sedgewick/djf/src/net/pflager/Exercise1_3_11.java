package net.pflager;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_11 {
    public static String EvaluatePostfix(String input) {
        Stack<String> stack = new Stack<>();

        String[] tokens = input.split(" ");

        for (String token : tokens) {
            if ("+-*/".contains(token)) {
                int rightArg = Integer.parseInt(stack.pop());
                int leftArg = Integer.parseInt(stack.pop());
                switch (token) {
                    case "+":
                        stack.push(Integer.toString(leftArg + rightArg));
                        break;

                    case "-":
                        stack.push(Integer.toString(leftArg - rightArg));
                        break;

                    case "*":
                        stack.push(Integer.toString(leftArg * rightArg));
                        break;

                    case "/":
                        stack.push(Integer.toString(leftArg / rightArg));
                        break;
                }
            } else { // it's a digit
                stack.push(token);
            }
        }

        return stack.pop();
    }

    public static void main(String[] args) {

    }

    @Test
    void test1() {
        assertEquals("3", EvaluatePostfix("1 2 + 3 4 - 5 6 - * *"));
    }
}

