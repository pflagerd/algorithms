package net.pflager;

public class Exercise1_1_24 {
	
	public static int gcd(int p, int q) {
		if (p < q) {
			int tmp = p; p = q; q = tmp;
		}
		
		System.out.println("p == " + p + ", q == " + q);
		
		int r = p % q;
		if (r == 0) {
			return q;
		}
		return gcd(q, r);
	}
	

	public static void main(String[] args) {
		System.out.println(gcd(105, 24));
		System.out.println(gcd(1111111, 1234567));
	}

}
