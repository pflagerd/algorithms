package net.pflager;

import java.util.Arrays;
import java.util.Random;

public class Exercise1_1_22 {

	public static int rank(int key, int[] a) {
		return rank(key, a, 0, a.length - 1, 0);
	}
	
	private static String spaces(int depth) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			stringBuilder.append(" ");
		}
		return stringBuilder.toString();
	}

	public static int rank(int key, int[] a, int lo, int hi, int depth) {
		System.out.println(spaces(depth) + " lo == " + lo + " hi == " + hi);
		
		// Index of key in a[], if present, is not smaller than
		// lo and not larger than hi.
		if (lo > hi)
			return -1;
		int mid = lo + (hi - lo) / 2;
		if (key < a[mid])
			return rank(key, a, lo, mid - 1, depth + 1);
		else if (key > a[mid])
			return rank(key, a, mid + 1, hi, depth + 1);
		else
			return mid;
	}

	public static void main(String[] args) {
		Random random = new Random();
		int arrayLength = random.nextInt(1000);
		int[] array = new int[arrayLength];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(Integer.MAX_VALUE);
		}
		Arrays.sort(array);
		
		int i = random.nextInt(array.length);
		System.out.println(rank(array[i], array));
	}

}
