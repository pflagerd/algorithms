package net.pflager;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercise1_1_21 {
	
	static private ArrayList<Record> records = new ArrayList<>();
	
	static private class Record {
		public Record(String name) {
			this.name = name;
		}
		
		String name;
		int a;
		int b;
		
		@Override
		public String toString() {
			StringBuilder stringBuilder = new StringBuilder();
			
			stringBuilder.append("{ name=" + "\"" + name + "\", a=" + a + ", b=" + b + " }");
			
			return stringBuilder.toString();
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
//		for (;;) {
//			System.out.print("Please enter a name (or Q/q to quit): ");
//			String s = scanner.nextLine();
//			if (s.contains("q") || s.contains("Q"))
//				break;
//			
//			Record record = new Record(s);
//			System.out.print("Please enter two integers, separated by a space (or Q/q to quit): ");
//			
//			s = scanner.nextLine();
//			if (s.contains("q") || s.contains("Q"))
//				break;
//			
//			s = s.trim();
//			String[] ints = s.split("\\s+");
//			record.a = Integer.parseInt(ints[0]);
//			record.b = Integer.parseInt(ints[1]);
//			
//			records.add(record);
//			System.out.println("Added record: " + record + "\n");
//		}
//		
		{
			Record record = new Record("Arthur Puty");
			record.a = 3;
			record.b = 23;
			records.add(record);

			record = new Record("Edgar Allan Poe");
			record.a = 33;
			record.b = 2322334;
			records.add(record);

			record = new Record("Franklin Delanor Roosevelt");
			record.a = 33;
			record.b = 2322334;
			records.add(record);

			record = new Record("Franklin Delanor Roosevelt and Eleanor Roosevelt");
			record.a = 33;
			record.b = 2322334;
			records.add(record);
		}
		
		System.out.println("Name                                        a         b       a/b");
		for (Record record : records) {
			System.out.printf("%-35.35s%10d%10d%10.3f\n", record.name, record.a, record.b, (float)record.a / record.b);
		}
		
	}
}
