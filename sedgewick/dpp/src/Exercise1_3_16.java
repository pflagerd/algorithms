package net.pflager;

import edu.princeton.cs.algs4.Date;
import edu.princeton.cs.algs4.Queue;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_16 {

    public static Date[] readDates(String string) {
        String[] stringArray = string.split("\\s+");
        Queue<Date> q = new Queue<Date>();
        for (String s : stringArray) {
            String[] sa = s.split("/");
            assert sa.length == 3;
            int[] ia = new int[3];
            ia[0] = Integer.parseInt(sa[0]);
            ia[1] = Integer.parseInt(sa[1]);
            ia[2] = Integer.parseInt(sa[2]);
            q.enqueue(new Date(ia[0], ia[1], ia[2]));
        }

        int qSize = q.size();
        Date[] returnValue = new Date[qSize];

        for (int i = 0; i < qSize; i++) {
            returnValue[i] = q.dequeue();
        }

        return returnValue;
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void test5_22_1939() throws IOException {
            assertEquals(new Date(5, 22, 1939), readDates("5/22/1939")[0]);
        }


        @org.junit.jupiter.api.Test
        void test5_22_1939_w_Arrays() throws IOException {
            assertArrayEquals(new Date[] { new Date(5, 22, 1939) }, readDates("5/22/1939"));
        }


        @org.junit.jupiter.api.Test
        void testTwoDifferentDates() throws IOException {
            assertArrayEquals(new Date[] { new Date(5, 22, 1939), new Date(12, 29, 1959) }, readDates("5/22/1939 12/29/1959"));
        }

        @org.junit.jupiter.api.Test
        void testThreeDifferentDates() throws IOException {
            assertArrayEquals(new Date[] { new Date(5, 22, 1939), new Date(12, 29, 1959),  new Date(7, 30, 1987)}, readDates("5/22/1939 12/29/1959 7/30/1987"));
        }


    }

}
