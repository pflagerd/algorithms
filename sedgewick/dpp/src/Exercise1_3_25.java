package net.pflager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Exercise1_3_25 {

    static class Node<Item> {
        private Item item;
        private Node<Item> next;

        public Node(Item item) {
            this.item = item;
        }

        /**
         * Deletes the kth node from this Node (numbering from 0. 0 being this Node)
         */
        public Node<Item> delete(int k) {
            if (k == 0)
                return this.next;

            Node<Item> node = this;
            for (int i = 0; i < k - 1 && node != null && node.next != null; i++) {
                node = node.next;
            }
            node.next = node.next.next;
            return this;
        }

        public static Node<String> makeListFromString(@org.jetbrains.annotations.NotNull String listAsString) {
            String[] listAsStringArray = listAsString.split("\\s+");
            if (listAsStringArray.length == 0)
                throw new IllegalArgumentException("listAsString must be non-null and must contain at least one word");

            Node<String> head = null;
            Node<String> tail = null;
            for (int i = 0; i < listAsStringArray.length; i++) {
                if (head == null) {
                    head = tail = new Node<String>(listAsStringArray[i]);
                    continue;
                }
                tail.next = new Node<String>(listAsStringArray[i]);
                tail = tail.next;
            }
            return head;
        }

        public static <Item> Node<Item> makeListFromString(@org.jetbrains.annotations.NotNull Item[] items) {

            Node<Item> head = null;
            Node<Item> tail = null;
            for (int i = 0; i < items.length; i++) {
                if (head == null) {
                    head = tail = new Node<Item>(items[0]);
                    continue;
                }
                tail.next = new Node<Item>(items[i]);
                tail = tail.next;
            }
            return head;
        }

        /**
         * Removes the node following the given one (and does nothing if the argument or the next
         * field in the argument node is null)
         */
        public void removeAfter(@org.jetbrains.annotations.NotNull Node<Item> node) {
            if (node.next == null) {
                return;
            }
            node.next = node.next.next;
        }

        /**
         * inserts the given node after <code>this</code> node (and does nothing if the given node is null).
         */
        public void insertAfter(@org.jetbrains.annotations.NotNull Node<Item> node) {
            Node<Item> oldNodeNext = this.next;
            this.next = node;
            node.next = oldNodeNext;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node<Item> n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void testString() {
            Node<String> first = Node.makeListFromString("10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }

        @org.junit.jupiter.api.Test
        void testIntegerArray() {
            Integer[] integers = new Integer[22];
            for (int i = 10; i < 32; i++) {
                integers[i - 10] = i;
            }

            Node<Integer> first = Node.makeListFromString(integers);

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }


        @org.junit.jupiter.api.Test
        void testRemoveAfterWithNullArgument() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");

            Throwable exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> {
                        singlyLinkedList.removeAfter(null);
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Argument for @NotNull parameter 'node' of net/pflager/Exercise1_3_25$Node.removeAfter must not be null", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10", singlyLinkedList.toString());
        }


        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNextNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNextNodeHavingNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNextNodeHavingNoNextNodePassNull() {
            Node<Integer> singlyLinkedList = new Node<>(10);

            Throwable exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> {
                        singlyLinkedList.removeAfter(null);
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Argument for @NotNull parameter 'node' of net/pflager/Exercise1_3_25$Node.removeAfter must not be null", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingHavingNoNextNodePassFirstNode() {
            Node<Integer> singlyLinkedList = new Node<>(10);

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            singlyLinkedList.insertAfter(new Node<String>("11"));

            assertEquals("10 11", singlyLinkedList.toString());
        }


        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.insertAfter(new Node<String>("10.5"));

            assertEquals("10 10.5 11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.insertAfter(new Node<String>("10.5"));

            assertEquals("10 10.5 11 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNextNodeStartingWithNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            Node<String> nextNode = singlyLinkedList.next;

            nextNode.insertAfter(new Node<String>("11.5"));

            assertEquals("10 11 11.5 12", singlyLinkedList.toString());
        }
    }
}
