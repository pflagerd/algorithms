package net.pflager;

import edu.princeton.cs.algs4.Queue;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_15 {

    /**
     * Takes a single argument k and uses it to find the kth element of a bunch of strings which are read from stdin.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("You must supply a single non-negative value k as an argument");
            System.exit(1);
        }

        int k = Integer.parseInt(args[0]);

        Queue<String> queue = new Queue();

        Scanner input = new Scanner(System.in);
        while (input.hasNextLine()) {
            queue.enqueue(input.nextLine());
        }

        System.err.println(queue);

        if (queue.size() < k) {
            System.err.println("You only entered " + queue.size() + " strings.  Cannot find the " + k + "th last element. Please enter a number of strings greater than or equal to " + k);
            System.exit(2);
        }

        String theString = null;
        int queueSize = queue.size();
        for (int i = 0; i <= queueSize - k; i++) {
            theString = queue.dequeue();
        }

        System.out.println(theString);
    }

    static class Test {
        private static class OutputToStringStream extends OutputStream {

            private StringBuilder stringBuilder = new StringBuilder();

            @Override
            public void write(int b) throws IOException {
                this.stringBuilder.append((char) b);
            }

            @Override
            public String toString() {
                return this.stringBuilder.toString();
            }
        }

        @org.junit.jupiter.api.Test
        void test2_9() throws IOException {
            // Capture and redirect System.in and System.out
            String str = "The\nQuick\nBrown\nFox\nJumps\nOver\nThe\nLazy\nDog.";
            InputStream inputStream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
            System.setIn(inputStream);
            inputStream.close();

            OutputToStringStream outputToStringStream = new OutputToStringStream();
            PrintStream printStream = new PrintStream(outputToStringStream);
            System.setOut(printStream);
            main(new String[] { "2" });

            System.err.println(outputToStringStream.toString());
            assertEquals("Lazy", outputToStringStream.toString().trim());
        }

        @org.junit.jupiter.api.Test
        void test6_9() throws IOException {
            // Capture and redirect System.in and System.out
            String str = "The\nQuick\nBrown\nFox\nJumps\nOver\nThe\nLazy\nDog.";
            InputStream inputStream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
            System.setIn(inputStream);

            OutputToStringStream outputToStringStream = new OutputToStringStream();
            PrintStream printStream = new PrintStream(outputToStringStream);
            System.setOut(printStream);
            main(new String[] { "6" });

            System.err.println(outputToStringStream.toString());
            assertEquals("Fox", outputToStringStream.toString().trim());

            inputStream.close();
            outputToStringStream.close();
        }
    }

}
