package net.pflager;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_18 {

    static class Node<Item> {
        private Item item;
        private Node next;

        public Node(Item item) {
            this.item = item;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void test() throws IOException {
            Node list = new Node<Integer>(10);
            Node node1 = new Node<Integer>(11);
            list.next = node1;
            Node node2 = new Node<Integer>(12);
            node1.next = node2;

            System.err.println(list.toString());

            assertEquals("10 11 12", list.toString());

            Node x = list;
            x.next = x.next.next;

            assertEquals("10 12", list.toString());

        }

    }

}
