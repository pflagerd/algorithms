package net.pflager;

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;

import static edu.princeton.cs.algs4.StdOut.println;

public class Exercise1_3_06 {
    public static void main(String[] args) {
        Queue<String> q = new Queue<>();
        q.enqueue("A");
        q.enqueue("B");
        q.enqueue("C");
        q.enqueue("D");
        q.enqueue("E");

        Stack<String> stack = new Stack<String>();
        while (!q.isEmpty())
            stack.push(q.dequeue());
        while (!stack.isEmpty())
            q.enqueue(stack.pop());

        println(q);
    }
}
