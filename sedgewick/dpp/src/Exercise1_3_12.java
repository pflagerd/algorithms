package net.pflager;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class Exercise1_3_12 {

    static Stack<String> copy(Stack<String> stack) {
        Stack<String> stackOut = new Stack<>();

        for (String s : stack) {
            stackOut.push(s);
        }

        return stackOut;
    }

    @Test
    void test1() {
        Stack<String> stack = new Stack<>();
        stack.push("The");
        stack.push("quick");
        stack.push("brown");
        stack.push("fox");
        stack.push("jumps");
        stack.push("over");
        stack.push("the");
        stack.push("lazy");
        stack.push("dog");
        stack.forEach(item -> System.err.println(item));
        System.err.println();

        Stack<String> anotherStack = copy(copy(stack));

        assertIterableEquals(stack, anotherStack);
    }
}

