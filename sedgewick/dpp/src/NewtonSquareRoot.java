package net.pflager;

import static edu.princeton.cs.algs4.StdOut.println;

public class NewtonSquareRoot {
	
	public static double sqrt(double c) {
		if (c < 0)
			return Double.NaN;
		
		double t = c;
		double previous_t = -1;
		while (t != previous_t) {
			previous_t = t;
			t = (c / t + t) / 2.0;
		}
		return t;
	}

	public static void main(String[] args) {
		println("sqrt(100) == " + sqrt(100.0));
		println("sqrt(10) == " + sqrt(10.0));
		println("sqrt(10) == " + Math.sqrt(10.0));
		
	}

}
