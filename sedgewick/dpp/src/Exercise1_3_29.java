import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class Exercise1_3_29 {

    static class DoubleNode<Item> {

        public DoubleNode<Item> insertAfter(DoubleNode<Item> node, DoubleNode<Item> afterMe) {
            return null;
        }

        public DoubleNode<Item> insertBefore(DoubleNode<Item> node, DoubleNode<Item> beforeMe) {
            return null;
        }

        public DoubleNode<Item> insertAtBeginning(DoubleNode<Item> node) {
            return null;
        }

        public DoubleNode<Item> insertAtEnd(DoubleNode<Item> node) {
            return null;
        }

        public DoubleNode<Item> remove(DoubleNode<Item> node) {
            return null;
        }

        public DoubleNode<Item> removeFromBeginning(DoubleNode<Item> node) {
            return null;
        }

        public DoubleNode<Item> removeFromEnd(DoubleNode<Item> node) {
            return null;
        }
    }

    static class Queue<Item> extends Node<Item> {
        public Node<Item> last = null;

        public Queue() {
            super();
            this.last = null;
        }

        public Queue(Item item) {
            super(item);
            this.next = this;
            this.last = this;
        }

        public void enqueue(Item item) {
            Node<Item> node = new Node<Item>(item);
            if (last == null) {
                last = node;
                node.next = node;
                return;
            }

            node.next = last.next;
            last.next = node;
            last = node;
        }

        public Item dequeue() {
            if (this.last == null)
                throw new NoSuchElementException("queue is empty");

            if (this.last == this.last.next) {
                Item returnValue = this.last.item;
                this.last = null;
                return returnValue;
            }

            // get and remove the successor of <code>this.last</code>
            Node<Item> first = this.last.next;

            this.last.next = first.next;

            return first.item;
        }

        public boolean isEmpty() {
            return this.last == null;
        }

        public int size() {
            if (this.last == null)
                return 0;

            if (this.last.next == this.last)
                return 1;

            int size = 1;
            Node<Item> node = this.last.next;
            while (node != last) {
                size++;
                node = node.next;
            }
            return size;
        }

    }

    static class Node<Item> {
        private Item item;
        protected Node<Item> next;

        private Node() {}

        public Node(Item item) {
            this.item = item;
        }

        /**
         * Deletes the kth node from this Node (numbering from 0. 0 being this Node)
         */
        public Node<Item> delete(int k) {
            if (k == 0)
                return this.next;

            Node<Item> node = this;
            for (int i = 0; i < k - 1 && node != null && node.next != null; i++) {
                node = node.next;
            }
            node.next = node.next.next;
            return this;
        }

        public static Node<String> makeListFromString(@org.jetbrains.annotations.NotNull String listAsString) {
            String[] listAsStringArray = listAsString.split("\\s+");
            if (listAsStringArray.length == 0)
                throw new IllegalArgumentException("listAsString must be non-null and must contain at least one word");

            Node<String> head = null;
            Node<String> tail = null;
            for (int i = 0; i < listAsStringArray.length; i++) {
                if (head == null) {
                    head = tail = new Node<String>(listAsStringArray[i]);
                    continue;
                }
                tail.next = new Node<String>(listAsStringArray[i]);
                tail = tail.next;
            }
            return head;
        }

        public static <Item> Node<Item> makeListFromArray(@org.jetbrains.annotations.NotNull Item[] items) {

            Node<Item> head = null;
            Node<Item> tail = null;
            for (int i = 0; i < items.length; i++) {
                if (head == null) {
                    head = tail = new Node<Item>(items[0]);
                    continue;
                }
                tail.next = new Node<Item>(items[i]);
                tail = tail.next;
            }
            return head;
        }

        /**
         * returns the value of the maximum key in the list.
         *
         * if list is empty returns 0
         */
        public Integer max() {
            Integer max = Integer.MIN_VALUE;

            for (Node<Integer> node = (Node<Integer>)this; node != null; node = node.next) {
                if (node.item > max)
                    max = node.item;
            }

            return max;
        }

        /**
         * returns the value of the maximum key in the list.
         *
         * if list is empty returns 0
         */
        public Integer maxRecursive() {
            return maxRecursiveHelper((Node<Integer>)this, Integer.MIN_VALUE);
        }

        private Integer maxRecursiveHelper(Node<Integer> node, Integer currentMax) {
            if (node == null)
                return currentMax;

            return maxRecursiveHelper(node.next, node.item > currentMax ? node.item : currentMax);
        }

        /**
         * Removes all of the nodes in the list that have key as its item field
         */
        public void remove(@org.jetbrains.annotations.NotNull Item key) {
            Node<Item> behind = null;
            for (Node<Item> node = this; node != null; node = node.next) {
                if (node.item.equals(key)) {
                    if (node == this && node.next == null) {
                        throw new IllegalArgumentException("If you remove the only node in the list, you have no list.");
                    }

                    if (node.next != null) {
                        node.item = node.next.item;
                        node.next = node.next.next;
                    } else {
                        behind.next = null;
                    }
                    return;
                }
                behind = node;
            }
        }

        /**
         * Removes the node following the given one (and does nothing if the argument or the next
         * field in the argument node is null)
         */
        public void removeAfter(@org.jetbrains.annotations.NotNull Node<Item> node) {
            if (node.next == null) {
                return;
            }
            node.next = node.next.next;
        }

        /**
         * inserts the given node after <code>this</code> node (and does nothing if the given node is null).
         */
        public void insertAfter(@org.jetbrains.annotations.NotNull Node<Item> node) {
            Node<Item> oldNodeNext = this.next;
            this.next = node;
            node.next = oldNodeNext;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Node<Item> n = this; n != null; n = n.next) {
                if (n != this) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(n.item.toString());
            }
            return stringBuilder.toString();
        }
    }

    static class Test {

        @org.junit.jupiter.api.Test
        void testString() {
            Node<String> first = Node.makeListFromString("10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }

        @org.junit.jupiter.api.Test
        void testIntegerArray() {
            Integer[] integers = new Integer[22];
            for (int i = 10; i < 32; i++) {
                integers[i - 10] = i;
            }

            Node<Integer> first = Node.makeListFromArray(integers);

            first.delete(4);

            System.err.println(first);

            assertEquals("10 11 12 13 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", first.toString());
        }


        @org.junit.jupiter.api.Test
        void testRemoveAfterWithNullArgument() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");

            Throwable exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> {
                        singlyLinkedList.removeAfter(null);
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Argument for @NotNull parameter 'node' of Exercise1_3_29$Node.removeAfter must not be null", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10", singlyLinkedList.toString());
        }


        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNextNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNextNodeHavingNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.removeAfter(singlyLinkedList);

            assertEquals("10 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingNoNextNodePassNull() {
            Node<Integer> singlyLinkedList = new Node<>(10);

            Throwable exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> {
                        singlyLinkedList.removeAfter(null);
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Argument for @NotNull parameter 'node' of Exercise1_3_29$Node.removeAfter must not be null", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testRemoveAfterWithFirstNodeHavingHavingNoNextNodePassFirstNode() {
            Node<Integer> singlyLinkedList = new Node<>(10);

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            singlyLinkedList.insertAfter(new Node<String>("11"));

            assertEquals("10 11", singlyLinkedList.toString());
        }


        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNoNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.insertAfter(new Node<String>("10.5"));

            assertEquals("10 10.5 11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.insertAfter(new Node<String>("10.5"));

            assertEquals("10 10.5 11 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testInsertAfterWithFirstNodeHavingNextNodeHavingNextNodeStartingWithNextNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            Node<String> nextNode = singlyLinkedList.next;

            nextNode.insertAfter(new Node<String>("11.5"));

            assertEquals("10 11 11.5 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveSingleNodeKeyNotMatches() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            singlyLinkedList.remove("11");

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveSingleNodeKeyMatches() {
            Node<String> singlyLinkedList = Node.makeListFromString("10");

            Throwable exception = assertThrows(
                    IllegalArgumentException.class,
                    () -> {
                        singlyLinkedList.remove("10");
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("If you remove the only node in the list, you have no list.", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testRemoveTwoNodesKeyNotMatches() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.remove("12");

            assertEquals("10 11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveTwoNodesNodeKeyMatchesFirstNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.remove("10");

            assertEquals("11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveTwoNodesNodeKeyMatchesSecondNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11");

            singlyLinkedList.remove("11");

            assertEquals("10", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveThreeNodesNodeKeyMatchesFirstNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.remove("10");

            assertEquals("11 12", singlyLinkedList.toString());
        }


        @org.junit.jupiter.api.Test
        void testRemoveThreeNodesNodeKeyMatchesSecondNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.remove("11");

            assertEquals("10 12", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveThreeNodesNodeKeyMatchesThirdNode() {
            Node<String> singlyLinkedList = Node.makeListFromString("10 11 12");

            singlyLinkedList.remove("12");

            assertEquals("10 11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testRemoveThreeIntegerNodesNodeKeyMatchesThirdNode() {
            Node<Integer> singlyLinkedList = Node.makeListFromArray(new Integer[] { 10, 11, 12 });

            singlyLinkedList.remove(12);

            assertEquals("10 11", singlyLinkedList.toString());
        }

        @org.junit.jupiter.api.Test
        void testMaxThreeElementArray() {
            Node<Integer> singlyLinkedList = Node.makeListFromArray(new Integer[] { 10, 11, 12 });

            assertEquals(12, singlyLinkedList.max());
        }

        @org.junit.jupiter.api.Test
        void testMaxEmptyArray() {

            Node<Integer> singlyLinkedList = Node.makeListFromArray(new Integer[] { });

            Throwable exception = assertThrows(
                    NullPointerException.class,
                    () -> {
                        singlyLinkedList.max();
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Cannot invoke \"Exercise1_3_29$Node.max()\" because \"singlyLinkedList\" is null", exception.getMessage());
        }


        @org.junit.jupiter.api.Test
        void testMaxRecursiveThreeElementArray() {
            Node<Integer> singlyLinkedList = Node.makeListFromArray(new Integer[] { 10, 11, 12 });

            assertEquals(12, singlyLinkedList.maxRecursive());
        }

        @org.junit.jupiter.api.Test
        void testMaxRecursiveEmptyArray() {

            Node<Integer> singlyLinkedList = Node.makeListFromArray(new Integer[] { });

            Throwable exception = assertThrows(
                    NullPointerException.class,
                    () -> {
                        singlyLinkedList.maxRecursive();
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("Cannot invoke \"Exercise1_3_29$Node.maxRecursive()\" because \"singlyLinkedList\" is null", exception.getMessage());
        }

        @org.junit.jupiter.api.Test
        void testQueueEnqueueAndDequeueOneInteger() {

            Queue<Integer> queue = new Queue<>();
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());

            queue.enqueue(121212121);
            assertEquals(1, queue.size());
            assertFalse(queue.isEmpty());

            assertEquals(121212121, queue.dequeue());
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());

        }

        @org.junit.jupiter.api.Test
        void testQueueEnqueueAndDequeueTwoIntegers() {

            Queue<Integer> queue = new Queue<>();
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());

            queue.enqueue(121212121);
            assertEquals(1, queue.size());
            assertFalse(queue.isEmpty());

            queue.enqueue(212121212);
            assertEquals(2, queue.size());
            assertFalse(queue.isEmpty());

            assertEquals(121212121, queue.dequeue());
            assertEquals(1, queue.size());
            assertFalse(queue.isEmpty());

            assertEquals(212121212, queue.dequeue());
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());
        }

        @org.junit.jupiter.api.Test
        void testQueueEnqueueAndDequeueTwoIntegersDequeueThree() {

            Queue<Integer> queue = new Queue<>();
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());

            queue.enqueue(121212121);
            assertEquals(1, queue.size());
            assertFalse(queue.isEmpty());

            queue.enqueue(212121212);
            assertEquals(2, queue.size());
            assertFalse(queue.isEmpty());

            assertEquals(121212121, queue.dequeue());
            assertEquals(1, queue.size());
            assertFalse(queue.isEmpty());

            assertEquals(212121212, queue.dequeue());
            assertEquals(0, queue.size());
            assertTrue(queue.isEmpty());

            Throwable exception = assertThrows(
                    NoSuchElementException.class,
                    () -> {
                        queue.dequeue();
                    }
            );

            // Optionally, further assertions can be made about the exception
            assertEquals("queue is empty", exception.getMessage());
        }

//      public DoubleNode<Item> insertAfter(DoubleNode<Item> node, DoubleNode<Item> afterMe)


         @org.junit.jupiter.api.Test
         void testDoubleNode_insertAfter_InsertNonNullAfterNull() {}

//      public DoubleNode<Item> insertBefore(DoubleNode<Item> node, DoubleNode<Item> beforeMe)

//      public DoubleNode<Item> insertAtBeginning(DoubleNode<Item> node)
        @org.junit.jupiter.api.Test
        void testDoubleNode_insertAtBeginning_InsertNull() {}

        @org.junit.jupiter.api.Test
        void testDoubleNode_insertAtBeginning_InsertNonNull() {}

        @org.junit.jupiter.api.Test
        void testDoubleNode_insertAtBeginning_Insert2NonNulls() {}

        @org.junit.jupiter.api.Test
        void testDoubleNode_insertAtBeginning_Insert3NonNulls() {}

        @org.junit.jupiter.api.Test
        void testDoubleNode_insertAtBeginning_InsertSomeNonNulls() {}

        void testDoubleNode_insertAtBeginning_InsertMaximumNumberOfNonNulls() {}

        void testDoubleNode_insertAtBeginning_InsertMoreThanMaximumNumberOfNonNulls() {}

//      public DoubleNode<Item> insertAtEnd(DoubleNode<Item> node)

//      public DoubleNode<Item> remove(DoubleNode<Item> node)

//      public DoubleNode<Item> removeFromBeginning(DoubleNode<Item> node)

//      public DoubleNode<Item> removeFromEnd(DoubleNode<Item> node)

    }
}
