package net.pflager;

import java.util.Stack;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExpressionEvaluation {
	
	private static double evaluateExpression(String[] expression) {
		Stack<String> ops = new Stack<String>();
		Stack<Double> vals = new Stack<Double>();
		for (int i = 0; i < expression.length; i++) {
			// Read token, push if operator.
			String s = expression[i];
			if (s.equals("(")) ;
			else if (s.equals("+")) ops.push(s);
			else if (s.equals("-")) ops.push(s);
			else if (s.equals("*")) ops.push(s);
			else if (s.equals("/")) ops.push(s);
			else if (s.equals("sqrt")) ops.push(s);
			else if (s.equals(")"))
			{ // Pop, evaluate, and push result if token is ")".
				String op = ops.pop();
				double v = vals.pop();
				if (op.equals("+")) v = vals.pop() + v;
				else if (op.equals("-")) v = vals.pop() - v;
				else if (op.equals("*")) v = vals.pop() * v;
				else if (op.equals("/")) v = vals.pop() / v;
				else if (op.equals("sqrt")) v = Math.sqrt(v);
				vals.push(v);
			} // Token not operator or paren: push double value.
			else vals.push(Double.parseDouble(s));
			}
			return vals.pop();	
		}

	public static void main(String[] args) {
	}

    @org.junit.jupiter.api.Test
	void test() {
		Assertions.assertEquals(51, evaluateExpression("( 1 + ( ( 2 + 3 ) * ( ( sqrt 4 ) * 5 ) ) )".split("\\s+")));
	}

    @org.junit.jupiter.api.Test
	void test1() {
		String[] tokens = new String[] { "(", ")", "+", "-", "*", "/"};
		
	}
}
