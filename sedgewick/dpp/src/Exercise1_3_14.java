package net.pflager;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class Exercise1_3_14 {

    public static class ResizingArrayQueue<Item> implements Iterable<Item> {

        private static final int defaultCapacity = 16;
        private Item[] a = (Item[]) new Object[defaultCapacity]; // queue items

        private int capacity = defaultCapacity;
        private int N = 0; // number of items

        public ResizingArrayQueue() {
        }

        public ResizingArrayQueue(int capacity) {
            if (capacity <= 0)
                throw new IllegalArgumentException("capacity " + capacity + " is not a non-negative number.");

            this.capacity = capacity;
            this.a = (Item[]) new Object[capacity];
        }

        public boolean isEmpty() {
            return N == 0;
        }

        public int size() {
            return N;
        }

        private void resize(int max) { // Move stack to a new array of size max.
            Item[] temp = (Item[]) new Object[max];
            for (int i = 0; i < N; i++)
                temp[i] = a[i];
            a = temp;
        }

        public void pushBack(Item item) { // Add item to top of queue.
            if (N == a.length) // grow it
                resize(2 * a.length); // TODO: DPP: 230905161742Z: Doubling seems excessive.
            a[N++] = item;
        }

        public Item popFront() { // Remove item from top of queue.
            if (N == 0)
                return null;

            Item item = a[--N];
            a[N] = null; // Avoid loitering (see text).
            if (N > 0 && N == a.length / 4) // shrink it
                resize(a.length / 2);
            return item;
        }

        public Iterator<Item> iterator() {
            return new ReverseArrayIterator();
        }

        private class ReverseArrayIterator implements Iterator<Item> { // Support LIFO iteration.
            private int i = N;

            public boolean hasNext() {
                return i > 0;
            }

            public Item next() {
                return a[--i];
            }

            public void remove() {
            }
        }

        public static class Test {
            @org.junit.jupiter.api.Test
            public void popFrontEmptyQueue() {
                ResizingArrayQueue<String> resizingArrayQueueUnderTest = new ResizingArrayQueue<>();
                assertEquals(resizingArrayQueueUnderTest.popFront(), null);
            }

            @org.junit.jupiter.api.Test
            public void pushBackAndPopFrontEmptyQueue() {
                ResizingArrayQueue<String> resizingArrayQueueUnderTest = new ResizingArrayQueue<>();
                resizingArrayQueueUnderTest.pushBack("The quick brown fox jumps over the lazy dog.");
                assertEquals("The quick brown fox jumps over the lazy dog.", resizingArrayQueueUnderTest.popFront());
            }

            @org.junit.jupiter.api.Test
            void defineNonPositiveCapacity() {
                assertThrows(IllegalArgumentException.class, () -> {
                    new Exercise1_3_14.ResizingArrayQueue<String>(-2);
                });
            }


            @org.junit.jupiter.api.Test
            void test1() {
                ResizingArrayQueue<String> resizingArrayQueue = new ResizingArrayQueue<>();

                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.forEach(item -> System.err.println(item));
                System.err.println();

                ResizingArrayQueue<String> resizingArrayQueueUnderTest = new ResizingArrayQueue<>();
                resizingArrayQueueUnderTest.pushBack("The");
                resizingArrayQueueUnderTest.pushBack("quick");
                resizingArrayQueueUnderTest.pushBack("brown");
                resizingArrayQueueUnderTest.pushBack("fox");
                resizingArrayQueueUnderTest.pushBack("jumps");
                resizingArrayQueueUnderTest.pushBack("over");
                resizingArrayQueueUnderTest.pushBack("the");
                resizingArrayQueueUnderTest.pushBack("lazy");
                resizingArrayQueueUnderTest.pushBack("dog");
                resizingArrayQueueUnderTest.forEach(item -> System.err.println(item));
                System.err.println();

                assertIterableEquals(resizingArrayQueue, resizingArrayQueueUnderTest);
            }

            @org.junit.jupiter.api.Test
            public void testResizingA() {

                ResizingArrayQueue<String> resizingArrayQueue = new ResizingArrayQueue<>(18);
                assertEquals(18, resizingArrayQueue.capacity);
                //assertEquals(18, resizingArrayQueue.a.length);

                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                assertEquals(18, resizingArrayQueue.capacity);
                //assertEquals(18, resizingArrayQueue.a.length);
                resizingArrayQueue.forEach(item -> System.err.println(item));
                System.err.println();
            }


            @org.junit.jupiter.api.Test
            void testResizingB() {
                ResizingArrayQueue<String> resizingArrayQueue = new ResizingArrayQueue<>(18);

                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.forEach(item -> System.err.println(item));
                System.err.println();

                ResizingArrayQueue<String> resizingArrayQueueUnderTest = new ResizingArrayQueue<>();
                resizingArrayQueueUnderTest.pushBack("The");
                resizingArrayQueueUnderTest.pushBack("quick");
                resizingArrayQueueUnderTest.pushBack("brown");
                resizingArrayQueueUnderTest.pushBack("fox");
                resizingArrayQueueUnderTest.pushBack("jumps");
                resizingArrayQueueUnderTest.pushBack("over");
                resizingArrayQueueUnderTest.pushBack("the");
                resizingArrayQueueUnderTest.pushBack("lazy");
                resizingArrayQueueUnderTest.pushBack("dog");
                resizingArrayQueueUnderTest.pushBack("The");
                resizingArrayQueueUnderTest.pushBack("quick");
                resizingArrayQueueUnderTest.pushBack("brown");
                resizingArrayQueueUnderTest.pushBack("fox");
                resizingArrayQueueUnderTest.pushBack("jumps");
                resizingArrayQueueUnderTest.pushBack("over");
                resizingArrayQueueUnderTest.pushBack("the");
                resizingArrayQueueUnderTest.pushBack("lazy");
                resizingArrayQueueUnderTest.pushBack("dog");
                resizingArrayQueueUnderTest.forEach(item -> System.err.println(item));
                System.err.println();

                assertIterableEquals(resizingArrayQueue, resizingArrayQueueUnderTest);
            }

            @org.junit.jupiter.api.Test
            void testResizingC() {
                ResizingArrayQueue<String> resizingArrayQueue = new ResizingArrayQueue<>(18);

                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.pushBack("The");
                resizingArrayQueue.pushBack("quick");
                resizingArrayQueue.pushBack("brown");
                resizingArrayQueue.pushBack("fox");
                resizingArrayQueue.pushBack("jumps");
                resizingArrayQueue.pushBack("over");
                resizingArrayQueue.pushBack("the");
                resizingArrayQueue.pushBack("lazy");
                resizingArrayQueue.pushBack("dog");
                resizingArrayQueue.forEach(item -> System.err.println(item));
                System.err.println();

                int n = resizingArrayQueue.N;
                for (int i = 0; i < n - 3; i++) {
                    resizingArrayQueue.popFront();
                }

                assertEquals(9, ((Object[])resizingArrayQueue.a).length);
                assertEquals(3, resizingArrayQueue.N);
            }
        }
    }
}

