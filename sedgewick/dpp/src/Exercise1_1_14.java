package net.pflager;

//
// Sedgewick p 56.
// 1.1.14 Write a static method lg() that takes an int value N as argument and returns
// the largest int not larger than the base-2 logarithm of N. Do not use Math.
//
public class Exercise1_1_14 {
	public static int lg(int N) {
		if (N <= 0)
			throw new IllegalArgumentException("N must be greater than zero");
	
		int lg = 0;
		for (int i = N >> 1; i != 0; i >>>= 1) {
			lg++;
		}
		return lg;
	}
	
	public static void main(String[] args) {
		System.out.println(lg(123));
	}
}