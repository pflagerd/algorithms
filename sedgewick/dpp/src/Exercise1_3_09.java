package net.pflager;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercise1_3_09 {
    public static String parenthesize(String input) {
        Stack<String> stack = new Stack<>();

        String[] tokens = input.split("\\s+");
        for (String token : tokens) {
            switch(token) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case "*":
                case "/":
                case "-":
                case "+":
                    stack.push(token);
                    break;

                case ")":
                    String s = " )";
                    s = " " + stack.pop() + s; // " 2 )"
                    s = " " + stack.pop() + s; // " + 2 )"
                    s = " " + stack.pop() + s; // " 1 + 2 )"
                    s = "(" + s;
                    stack.push(s);
                    break;

                default:
                    throw new RuntimeException("Invalid Input");
            }
        }

        return stack.pop();
    }

    public static void main(String[] args) {
    }

    @Test
    void test1() {
        assertEquals("( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )", parenthesize("1 + 2 ) * 3 - 4 ) * 5 - 6 ) ) )"));
    }
}

