package net.pflager;

import static edu.princeton.cs.algs4.StdOut.print;
import static edu.princeton.cs.algs4.StdOut.printf;
import static edu.princeton.cs.algs4.StdOut.println;

import edu.princeton.cs.algs4.StdDraw;

public class LearnStdDraw {
    public static void main(String[] args) {
        int N = 100;
        StdDraw.setXscale(0, N);
        StdDraw.setYscale(0, N);
        StdDraw.setPenRadius(.01);
        double err = Math.ulp(0.5);
        println(err);

        int x = 0;
        double n = N;
        while (Math.abs(N / n - n) > err * n) {
            n = (N / n + n) / 2;
            StdDraw.point(x, n);
            print(x + ", ");
            printf("%.17g\n", n);
            x++;
        }
    }
}
