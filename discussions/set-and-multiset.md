here we are

Generic set:    {1, 2}

Axiom:   {1, 2} = {2, 1}  # no ordering preserved in a set : generalization (model, category)

{1, 2, 2} - what?   "registry of things seen or included"

{ (0, 1), (1, 2), (2, 2) }   - "multiset"

"bag" - "bag of marbles" - no ordering

candidates for bag properties: (opaque bag)

- add-a-thing

- grab-a-thing (iterator?)

- NO "contains"

- NO "empty" -- try grab-a-thing -- possible exception

convenience functions for opaque bag




