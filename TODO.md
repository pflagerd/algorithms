**M-T**

**$1 \over 2$ hr CAT (M(Daniel)W(David)) [p46 "preassessment test 3"](doc/Arun Sharma Quantitative Aptitude 8th Edition (www.sarkaripost.in).pdf) problem 9/[Elementary Numerical Analysis](doc/elementary_numerical_analysis.pdf)(T(David)T(Daniel)) (p23, §1.3, Floating-Point Arithmetic, "The Exponent $e$" working on finishing design and starting implementing round() and chop() in FloatingPoint class in elementary_numerical_analysis/, but we do not understand $\beta ^{m-n}$ yet**

​	Add cards for even/odd

​	Add card to remind and test exponential rules.

​	Adding "What every computer scientist ..." to our reading of §1.3.

​	Cards on terminating and non-terminating.

​    Add cards for "Properties of Prime Numbers on p59.

	gcd(a, b):
	if a % b == 0:
	    return b
	return gcd(b, a % b)

​	Check this against f729e155-7c86-4982-a3b8-4980d757689f

​	Re-write Elementary Numerical Analysis by semantic maps. Start with Section 1.3.

​		Meticulously define the notation used. With examples.

https://utils.kde.org/projects/ark		Create a Semantic map.  A new kind of book.

Function to create "standard form" prime factorization.

Create cards for Sharma p64 "Some Rules for Co-primes"

In the use of a representation of Real Numbers having a hard limit on significant digits (after normalization), is it preferrable to accumulate additional significant digits when operating on the representation and then re-normalizing [the answer] to the hard limit or is it preferrable to normalize to the hard limit and then perform the operations so that the significant digits are always kept at the hard limit?

E.g.  For a hard limit of 2 significant digits

$$\frac{2}{3} + \frac{4}{7}$$

Option 1: $$normalize(\frac{2}{3} + \frac{4}{7})$$

Option 2: $$normalize(normalize(\frac{2}{3}) + normalize(\frac{4}{7}))$$

https://math.stackexchange.com/questions/519832/proving-by-induction-that-sum-k-0nn-choose-k-2n





**$1\over 2$ hr maxrecall.net (Daniel: MW, David: TT)**

​	66782a21-496c-4f79-9265-1b47fed8ffdd - split into numerous cards.

​	74a335d3-cbec-4e8a-90ca-597b59e326f8 - hyperlink it to package recipe comment definition.

​    20d0818f-61ba-4d13-9d37-a95ded449f61 = add succinct description/animation.

​	badf1a31-a57d-42ae-8ba8-4abe0ff23bef

​	db51e053-98e5-4dcb-a999-3a56dfb69dfc: rewrite the text using the same form as what ChatGPT4 wrote for "Describe Merge Sort" and replace divide and conquer by subtract and conquer.

78c34856-5b4e-4d56-a02e-73c320fa725a: Prerequisite knowledge for this is https://math.stackexchange.com/questions/519832/proving-by-induction-that-sum-k-0nn-choose-k-2n

8e2670eb-5a56-41e9-a429-7d07d0a1b2b2.  See practice problem 8e2670eb-5a56-41e9-a429-7d07d0a1b2b2.md and work on that.

If I have no card defining stable sort, make one. https://www.freecodecamp.org/news/stability-in-sorting-algorithms-a-treatment-of-equality-fa3140a5a539/#:~:text=A%20stable%20sorting%20algorithm%20maintains,after%20the%20collection%20is%20sorted.



**$1\over 2$ hr (MT) Sedgewick p161  "1.3.3" [this]() | (W) [Scheinerman](doc/Mathematics_A Discrete Introduction (2012).pdf), p3 Exercise 2.1 (T) [Epp](doc/Discrete Mathematics with Applications [3ed]-Epp (lnw Adam).pdf ) We're still in the first paragraph of 1.1 (p1)(David: MW, Daniel: TT)**

​	What is the relationship between a set and multiset?

​    What algorithms require a queue?

​		Collect some GeeksForGeeks problems on ChatGPT's answer.

​    What algorithms require a stack?

​		Collect some GeeksForGeeks problems on ChatGPT's answer.

​		Is there a GeeksForGeeks which has us implement a two-stack expression interpreter as on p129.

​		How about implementing a stack from scratch.

​			As an array and As as list and compare speed.

​    What does a queue look like in D, C, C++, clisp, javascript, python, and java.

​	What does a stack look like in D, C, C++, clisp, javascript, pythonDeepMind, and java.

​	p129 Why two stacks, but not one.

​	Implement an RPN calculator

   Implement a stack using array without resizing (Basic or Easy), array with resizing (grow and shrink - loitering) (Medium)

   Add programming questions that test the three types of counting (and differences).

   Add questions about java LinkedList class (which I don't), python and d equivalents.

   Add questions about interator in the three languages.

   Add questions about collections in the three languages.

   Add questions about queues in the three languages.

   Add questions about deques in the three languages

   Add questions about stacks in the three languages.

   https://xlinux.nist.gov/dads/HTML/abstractDataType.html

   Order of ${12\ \ln 2 \over \pi^2}\ \ln n$for Euclid's alg expressed with %.

​	flash card for "computational method" (Q, I, Omega, f) 

​	computational sequence

Opaque Bags vs Transparent Bags

The most general thing is the thing with "less code".

How does generalization relate to specification in terms of properties.  Does specialization imply it has more properties.

0-262-18120-7

0-262-18123-1 1966 MIT Press



**$1 \over 2$ hr GeeksForGeeks (David: MW [this](https://practice.geeksforgeeks.org/problems/sum-the-common-elements/1), Daniel: TT [this](https://practice.geeksforgeeks.org/problems/search-for-traitor5950/0)**)

​	Check khan academy India curriculum: e.g. https://www.khanacademy.org/math/in-in-class-1st-math-cbse

​		Add to max recall https://practice.geeksforgeeks.org/problems/delete-middle-element-of-a-stack/1

​	   Need a better way to describe algorithms such as Merge Sort 20d0818f-61ba-4d13-9d37-a95ded449f61 - include gotchas and sources of error

​	Correct this card with proper algorithm = new FloatingPoint(): 3f043680-5303-4641-af21-c8aae88afd09

https://practice.geeksforgeeks.org/problems/sort-a-stack/1?page=1&status[]=unsolved&category[]=Linked%20List&category[]=Stack&category[]=Queue&sortBy=submissions

https://practice.geeksforgeeks.org/problems/longest-valid-parentheses5657/1

https://practice.geeksforgeeks.org/problems/maximum-sum-lcm3025/1

​	How to compute ceiling square root using only integer operations

​		Using integer logarithms?

https://practice.geeksforgeeks.org/problems/hands-of-straights/1



**Fridays**

1 hr ML (Coursera) [next](https://www.coursera.org/learn/machine-learning-course/lecture/Lt2Mx/working-on-and-submitting-programming-assignments).

​	



1 hr Linear Algebra (Khan)  [next](Defining a plane in R3 with a point and normal vector.webm).

​	We started attempting Add vectors: magnitude & direction to component.  Create card.

​	We did Unit Vectors and Parametric representations of lines.

​		Create a card to convert from 2-D line forms to parametric form. And vice-versa.

​		Create a card to convert from 3-D line forms to parametric form. And vice-versa.

​		Convert from vectors to unit vector notation. And vice-versa.

​		Make the point that you can use either $\vec a$ or $\vec b$ to define a line involving $\vec b - \vec a$.



$$\left\{\begin{bmatrix}1 \\ 2\end{bmatrix},\begin{bmatrix}2 \\ 3\end{bmatrix},\begin{bmatrix}3 \\ 4\end{bmatrix}\right\}$$







# FUTURE

Spline Math / Interactive Curves and Surfaces

