
public class FeedingSchedule2 {
	public static void main(String[] args) {
		int x = 5, j = 0;
		OUTER: for (int i = 0; i < 7; i++) {
			System.out.println(i);
			INNER: do {
				i++;
				x++;
				if (x > 11) {
					continue OUTER;
				}
				
				if (x > 10) {
					break INNER;
				}
				x += 4;
				j++;
				System.out.println(j);
			} while (j <= 2);
			System.out.println(x);
		}
		System.out.println(j);
	}
}
