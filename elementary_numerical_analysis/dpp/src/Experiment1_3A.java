import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Experiment1_3A {

    public static class FloatingPoint {
        public static final FloatingPoint UNDERFLOW = new FloatingPoint();
        public static final FloatingPoint OVERFLOW = new FloatingPoint();

        public static final int maximumMantissaLength = 256;

        public class ExponentLimits {
            int m; // lower limit (exclusive)
            int M; // upper limit (exclusive)
        }

        private ExponentLimits exponentLimits;
        private int n;
        private final int β = 2;
        private byte[] mantissa;                    // Each byte stands for a decimal digit (although 1/2 of it is wasted, since a decimal digit requires only 4 bits, not 8)
        private int exponent;
        private boolean negativeSign;            // true if this is a negative number

        private FloatingPoint() {
        }

        public FloatingPoint(String string) {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        // TODO: DPP: What about supplying dividend and divisor as (very long) strings.

        public FloatingPoint(int dividend, int divisor) {
            if (divisor == 0) {
                throw new IllegalArgumentException("divisor must not be zero");
            }
            negativeSign = (dividend * divisor < 0);
            dividend = Math.abs(dividend);
            if (dividend == 0) {
                mantissa = new byte[1];
                mantissa[0] = 0;
                return;
            }

            divisor = Math.abs(divisor);

            mantissa = new byte[maximumMantissaLength]; // this is our quotient
            //byte[] remainder = new byte[maximumMantissaLength];

            int exponent = 0;
            boolean b = false;
            for (int i = 0; i < maximumMantissaLength; i++) {
                int q = dividend / divisor;
                int r = dividend % divisor;
                while (q == 0) {
                    dividend *= 10;
                    if (!b)
                        exponent--;
                    q = dividend / divisor;
                    r = dividend % divisor;
                }
                b = true;
                mantissa[i] = (byte)q;
                if (r == 0)
                    break;

                dividend = r;
            }
            this.exponent = exponent + 1;

            int i = mantissa.length - 1;
            for (; i >= 0; i--) {
                if (mantissa[i] != 0)
                    break;
            }
            int newMantissaLength = i + 1;
            byte[] newMantissa = new byte[newMantissaLength];
            i = 0;
            for (int j = newMantissaLength - 1; i < newMantissaLength; i++, j--) {
                newMantissa[i] = mantissa[j];
            }
            mantissa = newMantissa;
        }

        public FloatingPoint(double d) {
            if (d == 0.0) {
                mantissa = new byte[1];
                mantissa[0] = 0;
                return;
            }

            if (d < 0) {
                negativeSign = true;
                d = -d;
            }

            // TODO: DPP: What next? Get exponent, get mantissa?  How?
            // David Goldberg article?


        }

        // TODO: DPP: What about supplying an integer as a (very long) string.

        public FloatingPoint(int integer) {
            if (integer == 0) {
                mantissa = new byte[1];
                mantissa[0] = 0;
                return;
            }

            if (integer < 0) {
                negativeSign = true;
                integer = -integer;
            }
            int numberOfDigits = floorLog(integer) + 1;
            exponent = numberOfDigits;
            mantissa = new byte[numberOfDigits];
            for (int i = 0; integer != 0; i++, integer /= 10) {
                mantissa[i] = (byte) (integer % 10);
            }
        }

        /**
         *
         * @param integer is passed a non-negative value to compute the floorLog of.
         *
         * @return the value of (int)Math.floor(Math.log10((double)integer))
         */
        private static int floorLog(int integer) {
            if (integer <= 0) {
                throw new IllegalArgumentException("There is no such thing as a log of 0, and this function will return a valid answer only if `integer` is positive.");
            }

            int floorLog = 0;
            while ((integer /= 10) != 0) {
                floorLog++;
            }
            return floorLog;
        }

        public FloatingPoint normalize() {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        static public FloatingPoint normalize(FloatingPoint floatingPoint) {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        public boolean isNormalized() {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        /* 17 being the default precision of a IEEE 754 64-bit float */
        /* Truncate decimal portion of the number. */

        /** This overload of chop() will truncate the mantissa only if the mantissa will not fit in a double.
         *
         * e.g. If the mantissa of this FloatingPoint contains more than 17 decimal digits, the mantissa of the
         *      FloatingPoint representation will not fit into the 53 available mantissa bits of the `double`,
         *      so we'll have to truncate the FloatingPoint mantissa to fit before we can convert it to a `double`.
         *
         *      This is roughly equivalent to chop(17).
         *
         * @return the chopped double representation of the FloatingPoint()
         */
        public double chop() {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        /* Here digits is ANY digits, not just digits after the decimal point */
        public FloatingPoint chop(int digits) {
            if (digits > mantissa.length) {
                throw new IllegalArgumentException("digits " + digits +
                        " must not exceed mantissa length " + mantissa.length);
            }

            if (digits == mantissa.length) {
                return this;
            }

            if (digits <= 0) {
                throw new IllegalArgumentException("digits " + digits + " must be positive");
            }
            byte[] newMantissa = new byte[digits];
            for (int i = 0, j = mantissa.length - 1; i < digits; i++, j--) {
                newMantissa[newMantissa.length - 1 - i] = mantissa[j];
            }
            mantissa = newMantissa;
            return this;
        }

        /* 17 being the default precision of a IEEE 754 64-bit float */
        public double round() {
            throw new UnsupportedOperationException("FloatingPoint(String string) not implemented yet.");
        }

        /* Here digits is ANY digits, not just digits after the decimal point */
        public FloatingPoint round(int digits) {
            byte[] newMantissa = new byte[digits];
            int j = mantissa.length - 1;
            for (int i = 0; i < digits; i++, j--) {
                newMantissa[newMantissa.length - 1 - i] = mantissa[j];
            }
            if (j >= 0 && mantissa[j] >= 5) // TODO: Not quite right.  What about 8.4488888?
                newMantissa[0] += 1;

            mantissa = newMantissa;
            return this;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();

            if (negativeSign)
                stringBuilder.append("-");

            stringBuilder.append("0.");

            for (int i = mantissa.length - 1; i >= 0; i--) {
                stringBuilder.append(mantissa[i]);
            }

            stringBuilder.append("e");
            stringBuilder.append(exponent);
            return stringBuilder.toString();
        }

        static class Tests {
            @org.junit.jupiter.api.Test
            public void testFloatingPoint0Chop() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(0);
                // Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop();
                // assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.0e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPoint0Chop1() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(0);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(1);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.0e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg835Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-838);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.83e3"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg838Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-838);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.84e3"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg1234Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-1234);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.12e4"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg1234Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-1234);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.12e4"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg1267Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-1267);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.12e4"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg1267Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-1267);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.13e4"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg448Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-448);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.44e3"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg448Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-448);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.45e3"));
            }


            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg458Chop1() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-458);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(1);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.4e3"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointNeg458Round1() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-458);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(1);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.5e3"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational2_3Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(2, 3);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("0.67e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational2_3Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(2, 3);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.66e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRationalNeg2_3Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-2, 3);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.67e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational2_Neg3Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(2, -3);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.66e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational2_Neg3Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(2, -3);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("-0.67e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRationalNeg2_Neg3Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-2, -3);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.66e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRationalNeg2_Neg3Round2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-2, -3);
                Experiment1_3A.FloatingPoint roundedFloatingPointUnderTest = floatingPointUnderTest.round(2);
                assertTrue(roundedFloatingPointUnderTest.toString().contentEquals("0.67e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRationalNeg2_3Chop2() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(-2, 3);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(2);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("-0.66e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational2_0Constructor() {
                Assertions.assertThrows( IllegalArgumentException.class, () -> {new Experiment1_3A.FloatingPoint(2, 0);} );
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational0_5Chop1() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(0, 5);
                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(1);
                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.0e0"));
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational1_4Chop3() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(1, 4);

                Assertions.assertThrows( IllegalArgumentException.class, () -> {floatingPointUnderTest.chop( 3);} );
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational1_4Chop0() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(1, 4);
                Assertions.assertThrows( IllegalArgumentException.class, () -> { floatingPointUnderTest.chop( 0 ); } );
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRational1_4ChopNeg1() {
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(1, 4);
                Assertions.assertThrows( IllegalArgumentException.class, () -> { floatingPointUnderTest.chop(-1); } );
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointRationalInteger_MAX_VALUE_1Chop0() { // Biggest dividend, smallest divisor
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(Integer.MAX_VALUE, 1);
                Assertions.assertThrows( IllegalArgumentException.class, () -> { floatingPointUnderTest.chop( 0 ); } );
            }

            @org.junit.jupiter.api.Test
            public void testFloatingPointConstructor() {
                Experiment1_3A.FloatingPoint floatingPoint = new Experiment1_3A.FloatingPoint(84_488_888, 10_000_000);
                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(8.4488888);
                Assertions.assertEquals(floatingPoint, floatingPointUnderTest);
            }

//            @org.junit.jupiter.api.Test
//            public void testFloatingPointDoubleChop0() {
//                Experiment1_3A.FloatingPoint floatingPointUnderTest = new Experiment1_3A.FloatingPoint(8.4488888);
//                Experiment1_3A.FloatingPoint choppedFloatingPointUnderTest = floatingPointUnderTest.chop(0);
//                assertTrue(choppedFloatingPointUnderTest.toString().contentEquals("0.0e0"));
//            }

            @org.junit.jupiter.api.Test
            public void testFloorLog1() { // Biggest dividend, smallest divisor
                Assertions.assertEquals(0 , floorLog(1));
            }

            @org.junit.jupiter.api.Test
            public void testFloorLog10() { // Biggest dividend, smallest divisor
                Assertions.assertEquals(1 , floorLog(10));
            }

        }
}

}
