package ex1_2_3;

public class NumericalIntegration {

	static double f(double x) {
		return x * x * x;
	}
	
	static double integrate(double low, double hi, int iterations) {
		double sum = 0;
		double h = (hi - low) / iterations;
		for (double i = low; i <= hi - h; i += h) {
			sum += f(i) + f(i + h);
		}
		
		return sum / 2.0 * h;
	}
	
	static double dynamicIntegrate(double low, double hi) {
		
		int iterations = 1;
		double lastValue = integrate(low, hi, iterations);
		double nextValue = 0;
		while (true) {
			nextValue = integrate(low, hi, ++iterations);
			System.err.println(nextValue);
			if (lastValue == nextValue)
				return nextValue;
			lastValue = nextValue;
		}
	}
	
	public static void main(String[] args) {
		System.err.println(dynamicIntegrate(10, 12));
		
		System.err.println(Math.pow(12, 4) / 4 - Math.pow(10,  4) / 4);
	}
	
}
