#!/usr/bin/python
import requests

# Set up the API request parameters
api_url = 'https://api.openai.com/v1/answers'
headers = {'Authorization': 'Bearer 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'}
data = {
    'model': 'davinci',
    'question': 'What is the capital of France?',
    'documents': ['Paris is the capital of France.'],
    'max_tokens': 5,
    'stop': '.'
}

# Send the API request
response = requests.post(api_url, headers=headers, json=data)

# Get the HTML-formatted answer and save it to a file
answer = response.json()['answers'][0]['html_answer']
with open('answer.html', 'w') as file:
    file.write(answer)
