#!/usr/bin/env rdmd
import std.stdio;
import std.conv;

string usage = "factors <integer>";

int main(string[] args) {
    if (args.length != 2) {
        stderr.writeln(usage);
        return 1;
    }
    
    long theLong = to!long(args[1]);
    writeln(theLong);
    
    for (long l = 1; l <= theLong; l++) {    
        if (theLong % l == 0) {
            write(l, " ");
        }
    }
    writeln();

    return 0;
}
