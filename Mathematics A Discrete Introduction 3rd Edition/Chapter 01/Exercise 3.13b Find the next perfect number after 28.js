/* boolean */ function isPerfectNumber(/* number */ number) {
  if (number === 1)
    return false;

  let sum = 1;
  for (let i = 2; i * i < number; i++) {
    if (number % i === 0) {
       sum += i;
       sum += Math.floor(number / i);
    }
  }
  return sum === number;
}


/* number */ function getNextPerfectNumber(/* number */ number) {
  console.log(number);
  for (let n = number + 1; n < 1_000_000; n++) {
    if (isPerfectNumber(n)) {
      console.log(`Found ${n}`);
      return n;
    }
  }
  return -1;
}