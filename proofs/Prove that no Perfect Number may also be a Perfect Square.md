Prove that no Perfect Number may also be a Perfect Square.



A Perfect Number is defined to be a number for which the sum of its factors, excluding itself, equals itself.



$$ \pi = 3 $$
