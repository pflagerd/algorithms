Prove by induction

$\left(\sum\limits_{i=0}^n i \right)^2 = \sum\limits_{i=0}^n i^3$												(1)

We know from [here](https://en.wikipedia.org/wiki/Triangular_number) that:

$\sum\limits_{i=0}^n i = \frac{n(n+1)}{2}$													(2)

Substituting (2) into (1):

$\left(\frac{n(n+1)}{2}\right)^2 = \sum\limits_{i=0}^n i^3 \tag{1.0}$											(3)



Base case, $n = 0$.

$0 = \left(\frac{0(0+1)}{2}\right)^2 = 0$



Induction case, assume (3).

$\left(\frac{n(n+1)}{2}\right)^2 = \sum\limits_{i=0}^n i^3 \tag{1.0}$



Add $(n+1)^3$ to both sides:

$\left(\frac{n(n+1)}{2}\right)^2 + (n+1)^3 = \sum\limits_{i=0}^n i^3 + (n+1)^3$



Rewrite the right-hand side, changing the upper bound $n$ to $n + 1$ so that $(n+1)^3$ may be collapsed into the summation.

$\left(\frac{n(n+1)}{2}\right)^2 + (n+1)^3 =  \sum\limits_{i=0}^{n+1} i^3$          (4)



Focusing on the left hand side exclusively now.

$\left(\frac{n(n+1)}{2}\right)^2  + (n+1)^3$



Factor out $(n+1)$ from $(n+1)^3$.

$\left(\frac{n(n+1)}{2}\right)^2 + (n+1)(n+1)^2$



Split $\left(\frac{n(n+1)}{2}\right)^2$ into $\left(\frac{n(n+1)}{2}\right)\left(\frac{n(n+1)} {2}\right)$ 

$\left(\frac{n(n+1)}{2}\right)\left(\frac{n(n+1)}{2}\right) + (n+1)(n+1)^2$





$\left(\frac{n(n+1)}{2}\right)\left(\frac{n(n+1)}{2}\right) + n(n+1)^2+(n+1)^2$



$(n+1)^2\left(\frac{n}{2}\right)\left(\frac{n}{2}\right) + n(n+1)^2+(n+1)^2$

$\frac{1}{4}(n+1)^2n^2 + n(n+1)^2+(n+1)^2$



$\frac{1}{4}((n+1)^2n^2 + 4n(n+1)^2+4(n+1)^2)$



$\frac{1}{4}(n+1)^2(n^2 + 4n+4)$

$\frac{1}{4}(n+1)^2(n + 2)^2$

$\frac{1}{4}((n+1)(n + 2))^2$

$\left(\frac{(n+1)(n + 2)}{2}\right)^2$

therefore $\left(\frac{(n+1)(n + 2)}{2}\right)^2$  =  $\sum\limits_{i=0}^{n+1} i^3$ 

Let $m = n + 1$, and substitute $m$ for $(m + 1)$ above.         

$\left(\frac{m(m + 1)}{2}\right)^2=\sum\limits_{i=0}^{m} i^3$

QED.









































































































